function confirmDialogSend(frm)
{
    swal({
        title:"Confirmar operaci\u00f3n",
        text:"\u00bfRealmente desea proceder?",
        icon:"warning",
        buttons:["No, cancelar.","Si, proceder."]
        }).then(
            function(b)
            {
                b&&(ignoreRestrictedClose=!0,
                $("#"+frm).submit())
            }
    )
}