<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use DB;

class TUbigeo extends Model
{
	protected $table='ubigeo';
	protected $primaryKey='idubigeo';
	public $incrementing=true;
    public $timestamps=false;

    // public function getProvincia()
    // {
    //     $provincia = DB::select('select * from ubigeo where id_region='.$this->id_region.' AND id_provincia='.$this->id_provincia.' AND id_distrito=0');
    //     // echo '-----------';
    //     // echo $this;
    //     // echo '-----------';
    //     // echo $provincia[0]->ubigeo_nombre;exit();
    //     return $provincia[0]->ubigeo_nombre;
    //     $this->hasOne('App\Price')
    // }

    public function tIe_()
    {
        return $this->hasOne('App\Model\TIIEE','ubigeoid');
    }
}
?>
