<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TUnidad extends Model
{
	protected $table='tunidad';
	protected $primaryKey='idunidad';
	public $incrementing=true;
	public $timestamps=false;

	public function tCursoxdocente()
    {
        return $this->belongsTo('App\Model\TCursoxdocente','idcd');
	}
	public function tSesion()
    {
        return $this->hasMany('App\Model\TSesion','idunidad');
	}
}
?>