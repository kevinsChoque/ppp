<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TRecursosaprendizaje extends Model
{
	protected $table='trecursosaprendizaje';
	protected $primaryKey='idra';
	public $incrementing=true;
	public $timestamps=false;
}
?>