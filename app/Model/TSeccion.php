<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TSeccion extends Model
{
	protected $table='seccion';
	protected $primaryKey='idseccion';
	public $incrementing=true;
	public $timestamps=false;

	public function tDetalleCursoporDocente()
	{
		return $this->hasMany('App\Model\tDetalleCursoporDocente','idseccion');
	}
}
?>