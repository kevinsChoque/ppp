<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TRasecundaria extends Model
{
	protected $table='trasecundaria';
	protected $primaryKey='idras';
	public $incrementing=true;
	public $timestamps=false;
}
?>