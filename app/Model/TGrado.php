<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TGrado extends Model
{
	protected $table='grado';
	protected $primaryKey='idgrado';
	public $incrementing=true;
	public $timestamps=false;

	public function tDetalleCursoporDocente()
	{
		return $this->hasMany('App\Model\TDetallecursopordocente','idgrado');
	}
}
?>