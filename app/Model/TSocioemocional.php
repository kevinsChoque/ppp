<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TSocioemocional extends Model
{
	protected $table='tsocioemocional';
	protected $primaryKey='idse';
	public $incrementing=true;
	public $timestamps=false;
}
?>