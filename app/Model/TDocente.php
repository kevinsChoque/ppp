<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TDocente extends Model
{
	protected $table='docente';
	protected $primaryKey='dni';
	public $incrementing=true;
	public $timestamps=false;

	public function tPersona()
	{
	return $this->belongsTo('App\Model\TPersona');
	}

	public function tIIEE()
	{
	return $this->belongsTo('App\Model\TIIEE','ie');
	} 


	public function tCursoxdocente()
    {
        return $this->hasMany('App\Model\TCursoxdocente','dni');
	}


	////////////////////
	public function tPersona_()
	{
		return $this->hasOne('App\Model\TPersona','dni');
	}

}
?>