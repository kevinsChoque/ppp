<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TArchivospecialist extends Model
{
	protected $table='tarchivospecialist';
	protected $primaryKey='idarchivospecialist';
	public $incrementing=true;
	public $timestamps=false;
}
?>