<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TArchivoie extends Model
{
	protected $table='tarchivoie';
	protected $primaryKey='idarchivoie';
	public $incrementing=true;
	public $timestamps=false;

	public function tIIEE()
    {
        return $this->belongsTo('App\Model\TIIEE','idie');
    }
}
?>