<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TFormulario2 extends Model
{
	protected $table='tformulario2';
	protected $primaryKey=false;
	public $incrementing=false;
	public $timestamps=false;
}
?>