<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TCurso extends Model
{
	protected $table='tcurso';
	protected $primaryKey='idcurso';
	public $incrementing=true;
	public $timestamps=false;

	public function tCursoxDtDetalleCursoporDocenteocente()
	{
		return $this->hasMany('App\Model\TDetallecursopordocente','idcurso');
	}
}
?>