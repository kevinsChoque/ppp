<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TPersona extends Model
{
	protected $table='persona';
	protected $primaryKey='dni';
	public $incrementing=true;
	public $timestamps=false;

	public function tFuncionario()
	{
		return $this->hasOne('App\Model\TFuncionario','dni');
	}
	public function tEspecialista()
	{
		return $this->hasOne('App\Model\TEspecialista','dni');
	}
	public function tDocente()
	{
		return $this->hasOne('App\Model\TDocente','dni');
	}
	
	///////////
	public function tDocente_()
	{
	return $this->belongsTo('App\Model\TDocente','dni');
	} 
}
?>