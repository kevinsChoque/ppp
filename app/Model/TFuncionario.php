<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TFuncionario extends Model
{
	protected $table='funcionario';
	protected $primaryKey='dni';
	public $incrementing=true;
	public $timestamps=false;

	public function tPersona()
	{
	return $this->belongsTo('App\Model\TPersona');
	}
}
?>