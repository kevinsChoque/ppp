<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Tugel extends Model
{
	protected $table='ugel';
	protected $primaryKey='idugel';
	public $incrementing=true;
	public $timestamps=false;

	public function tEspecialista()
    {
        return $this->hasMany('App\Model\TEspecialista','idugel');
    }
    public function tIIEE_()
    {
        return $this->hasOne('App\Model\TIIEE','idugel');
    }
    public function tIIEE()
    {
        return $this->hasMany('App\Model\TIIEE','ugelid');
    }
}
?>