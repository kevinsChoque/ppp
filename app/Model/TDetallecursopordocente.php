<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class TDetallecursopordocente extends Model
{
	protected $table='tdetallecursopordocente';
	protected $primaryKey='iddcd';
	public $incrementing=true;
	public $timestamps=false;

	public function tCurso()
    {
        return $this->belongsTo('App\Model\TCurso','idcurso');
	}
	public function tGrado()
    {
        return $this->belongsTo('App\Model\TGrado','idgrado');
	}
	public function tSeccion()
    {
        return $this->belongsTo('App\Model\TSeccion','idseccion');
    }	
    
    public function tCursoxDocente()
    {
        return $this->belongsTo('App\Model\TCursoxdocente','idcd');
	}
}
?>