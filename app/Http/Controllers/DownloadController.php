<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TArchivoie;
use App\Model\TCursoxdocente;

class DownloadController extends BaseController
{
    public function actionDownload(Request $request,SessionManager $sessionManager,$file=null)
    {
    	// $tArchivoie=TArchivoie::find($idarchivoie);
    	// $pathtoFile = public_path().'/fileie/';
    	// File::move($pathtoFile.$tArchivoie->nombrereal.'.'.$tArchivoie->formato,$pathtoFile.$tArchivoie->idie.'-'.);

        $pathtoFile = public_path().'/fileie/'.$file;
        return response()->download($pathtoFile);
    }
}
