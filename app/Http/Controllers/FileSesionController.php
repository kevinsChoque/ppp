<?php
namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Response;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TDocente;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TSesion;
use App\Model\TUnidad;
use App\Model\TDetallecursopordocente;
use App\Model\TCursoxdocente;

class FileSesionController extends Controller
{
    protected $documentos = array("pdf", "docx", "xlsx");

    public function actionInsertSesion(Request $request,SessionManager $sessionManager)
    {
        if($_POST)
        { 
            try
            {
                if($request->hasFile('file'))
                {
                    DB::beginTransaction();

                    $tSesion= new TSesion();
                    $tSesion->idunidad=$request->selectUnidad;
                    $tSesion->nombre=$request->nombre;
                    $tSesion->comentario=$request->comentario;
                    $tSesion->formato=strtolower($request->file('file')->getClientOriginalExtension());
                    $tSesion->peso=round($request->file('file')->getSize()/1024).'-kb';
                    $tSesion->createddate=date('Y-m-d H:m:s');
                    $tSesion->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';

                    $tSesion->save();

                    $request->file('file')->move(public_path().'/filesesion'.'/',session()->get('Person')->dni.'-'.$tSesion->idsesion.'.'.$tSesion->formato);
                
                    DB::commit();
                    
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSesion/insertSesion');
                }
                else 
                {
                    return $this->helperdrea->redirectError('No se cargo ningun archivo.', 'fileSesion/insertSesion');
                }
            }
            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal. Comuniquese con el administrador.', 'fileSesion/insertSesion');
            }
        }

        $nivel = session()->get('Person')->tDocente->tIIEE->nivelm;
        $tCursoxDocente=TCursoxDocente::whereRaw('dni=? and nombreArchivo=?',[session()->get('Person')->dni,'Plan anual'])->get();

        //esto cambiaremos por la lista de sesiones
        // $listTunidad = TUnidad::select('TUnidad.*')
        //     ->join('TCursoxdocente', 'TCursoxdocente.idcd', '=', 'TUnidad.idcd')
        //     ->where('TCursoxdocente.dni',session()->get('Person')->dni)
        //     ->get();

        $listTsesion = TSesion::select('tsesion.*')
            ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
            ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
            ->where('tcursoxdocente.dni',session()->get('Person')->dni)
            ->get();

            // echo($listTunidad);exit();

        return view('fileDcte/insertSesion',['nivel'=>$nivel,'tCursoxDocente'=>$tCursoxDocente,'listTsesion'=>$listTsesion]);
    }
    public function actionDeleteSesion($idsesion=null)
    {
        $tSesion=TSesion::find($idsesion);
        // echo($idunidad);exit();
        if($tSesion!=null)
        {
            if($tSesion->delete())
            {
                $rutaArchivo = public_path().'/filesesion/'.session()->get('Person')->dni.'-'.$tSesion->idsesion.'.'.$tSesion->formato;

                if(File::delete($rutaArchivo))
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSesion/insertSesion');
                }
                else
                {
                    return $this->helperdrea->redirectError('No se pudo eliminar el archivo.', 'fileSesion/insertSesion');
                }
            }
        }

        return $this->helperdrea->redirectError('Ocurrió un error. Contactese con el administrador.', 'fileSesion/insertSesion');

    }
    
    public function actionEditSesion(Request $request,SessionManager $sessionManager)
    {
        $tSesion=TSesion::find($request->idsesion);
        //--
        if($_POST)
        {
            $tSesion->idunidad=$request->selectUnidad;
            $tSesion->nombre=$request->nombre;
            $tSesion->comentario=$request->comentario;
            $tSesion->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';
            // echo($tSesion);exit();

            $file = $request->file('file');
            
            if($file!='')
            {
                // echo $file;exit();
                $rutaArchivo = public_path().'/filesesion/'.session()->get('Person')->dni.'-'.$tSesion->idsesion.'.'.$tSesion->formato;
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';

                if(File::delete($rutaArchivo))
                {
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/filesesion';
                        $fileName = session()->get('Person')->dni.'-'.$tSesion->idsesion.'.'.$formato;
                        
                        $file->move($ruta,$fileName);

                        $tSesion->formato = $formato;
                        $tSesion->peso = $peso;

                        if($tSesion->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSesion/insertSesion');
                        }
                        else
                        {
                            return $this->helperdrea->redirectError('Ocurrió un error. Contactese con el administrador', 'fileSesion/insertSesion');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error.', 'fileSesion/insertSesion');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectError('Ocurrió un error.', 'fileSesion/insertSesion');
                }
            }
            else
            {
                if($tSesion->save())
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSesion/insertSesion');

                }
            }
        }
        //--
        return response()->json(['data'=>$tSesion]);
        // echo $request->idarchivoie;
    }
    public function actionInsertUS(Request $request,SessionManager $sessionManager)
    {
        return view('fileDcte/insertUS');
    }
    public function actionShowFormInsertS(Request $request,SessionManager $sessionManager)
    {
        // $idgrado=TCursoxdocente::find($request->idcd)->tgrado->idgrado;
        
        $listTsesion = TSesion::select('tsesion.*')
                ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->join('grado', 'grado.idgrado', '=', 'tcursoxdocente.idgrado')
                ->where('tcursoxdocente.dni',session()->get('Person')->dni)
                ->where('tcursoxdocente.idcd',$request->idcd)
                ->get();

        return view('fileDcte/showFormInsertS',['idcd'=>$request->idcd,'listTsesion'=>$listTsesion]);
    }
    public function actionLoadUnid(Request $request,SessionManager $sessionManager)
    {
        if($request->ajax())
        {
            $listTunidad = TUnidad::where('idcd',$request->idcd)->get();
            return response::json($listTunidad);
        }
        else{
            // return $this->response->redirect('index');
        }

        // return response()->json(['data'=>$request->idcd]);   
    }
}