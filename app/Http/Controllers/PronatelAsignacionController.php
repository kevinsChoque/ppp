<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;

use App\Model\TRasecundaria;
use App\Model\TUsuario;
use App\Model\TExperiencias;
use App\Model\TIIEE;

class PronatelAsignacionController extends Controller
{
    public function actionAsignacion(Request $request)
    {
        if($_POST)
        {
            $tExperiencias=new TExperiencias();
            $tExperiencias->dni=session()->get('Person')->dni;
            $tExperiencias->ugel=$request->ugel;
            $tExperiencias->mes=$request->mes;
            $tExperiencias->grado=$request->grado;
            $tExperiencias->nivel='Secundaria';
            $tExperiencias->nombre=$request->nombre;
            $tExperiencias->descripcion=$request->descripcion;
            $tExperiencias->earchivo=$request->earchivo;
            $tExperiencias->evideo=$request->evideo;
            $tExperiencias->eaudio=$request->eaudio;

            if($tExperiencias->save())
            {
                if($request->hasFile('archivoPdf'))
                {
                    $file = $request->file('archivoPdf');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    if($formato=='pdf')
                    {
                        $tExperiencias=TExperiencias::all()->last();
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivoPdf->getClientOriginalName()));
                        $request->file('archivoPdf')->move(public_path().'/fileea/sec/pdf'.'/',$tExperiencias->ide.'_'.$nombreArchivo);
                        $tExperiencias->archivoPdf=$tExperiencias->ide.'_'.$nombreArchivo;
                        $tExperiencias->save();
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Solo se acepta archivos en formato PDF.', 'expAprSec/registrar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar el registro.', 'expAprSec/registrar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
        }
        // dd(session()->get('Person')->cm);
        $tie=TIIEE::whereRaw('codigomodular=?',session()->get('Person')->cm)->first();
        // dd($tie);
        return view('gestionPronatel/asignacion',['tie'=>$tie]);
    }
    public function actionListar()
    {
        $list=TRecursosaprendizaje::whereRaw('dni=?',session()->get('Person')->dni)->get();
        return view('recursosAprendizaje/listar',['list'=>$list]);
    }
    public function actionEditar(Request $request)
    {
        $tSocioemocional = TSocioemocional::find($request->idse);
        if($_POST)
        {
            $tSocioemocional->ugel=$request->ugel;
            $tSocioemocional->categoria=$request->categoria;
            $tSocioemocional->nombre=$request->nombre;
            $tSocioemocional->descripcion=$request->descripcion;
            $tSocioemocional->earchivo=$request->earchivo;
            $tSocioemocional->evideo=$request->evideo;
            $tSocioemocional->eaudio=$request->eaudio;
            if($tSocioemocional->save())
            {
                if($request->hasFile('archivo'))
                {
                    $archivoEliminado=true;
                    if($tSocioemocional->archivo!='')
                    {
                        // dd('Se eliminon el archivo anterior');
                        $rutaArchivo = public_path().'/filesocioemocional/'.$tSocioemocional->archivo;
                        if(!File::delete($rutaArchivo))
                        {
                            $archivoEliminado=false;
                        }
                    }
                    if($archivoEliminado)
                    {
                        // dd('entro ya q no tiene archivo');
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                        $request->file('archivo')->move(public_path().'/filesocioemocional'.'/',$tSocioemocional->idse.'_'.$nombreArchivo);
                        $tSocioemocional->archivo=$tSocioemocional->idse.'_'.$nombreArchivo;
                        // $tSocioemocional->save();
                        if($tSocioemocional->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Error al guardar el nuevo archivo.', 'socioEmocional/listar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Error al eliminar el archivo anterior.', 'socioEmocional/listar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar cambios.', 'socioEmocional/listar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
        }
        
        return response()->json(['data'=>$tSocioemocional]);
    }
    public function actionDelete($ide=null)
    {
        $tExperiencias=TExperiencias::find($ide);
        
        if($tExperiencias!=null)
        {
            if($tExperiencias->delete())
            {
                if($tExperiencias->archivoPdf!='')
                {
                    $rutaArchivo = public_path().'/fileea/sec/pdf/'.$tExperiencias->archivoPdf;
                    if(File::delete($rutaArchivo))
                    {
                        if($tExperiencias->archivoDoc!='')
                        {
                            $rutaArchivo = public_path().'/fileea/sec/doc/'.$tExperiencias->archivoDoc;
                            if(File::delete($rutaArchivo))
                            {
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                            }
                            else
                            {
                                return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo en word.', 'expAprSec/registrar');
                            }
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo.', 'expAprSec/registrar');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'expAprSec/registrar');
                }
            }
        }

        return $this->helperdrea->redirectError('No se encontro el registro.', 'expAprSec/registrar');

    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
    // portal
    public function actionListarPortal(Request $request)
    {
        $grado=$request->grado+1;
        $list=TRecursosaprendizaje::whereRaw('nivel=? and grado=? and tipoMaterial=? and mes=?',[$request->nivel,$grado,$request->tipo,$request->mes])->get();
        return view('portal/recursosAprendizaje/listarPortal',['list'=>$list]);
    }
}
