<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;
use Mail;

use App\Model\TSocioemocional;
use App\Model\TUsuario;
use App\Model\TFormulario2;

class TabletasPronatelController extends Controller
{
    public function actionPortal()
    {
        return view('index');
    }
    public function actionInicio()
    {
        return view('gestionPronatel/inicio');
    }
    public function actionLogin(Request $request,SessionManager $sessionManager,Encrypter $encrypter)
    {
        if($_POST)
        {
            $sessionManager->flush();
            // $tUsuario=TFormulario2::find($request->input('dni'));
            $tUsuario=TFormulario2::whereRaw('cam23=?',[$request->input('dni')])->first();
            // dd($tUsuario);

            if($tUsuario==null)
            {
                return $this->helperdrea->redirectError('El usuario no tiene acceso a la plataforma.', 'user/loginTabletasPronatel');
            }
            //------------------primero crear su usuario en la tabla de usuarios
            if($tUsuario!=null && '@repo2020'==$request->input('password'))
            {
                if(true)
                // if(strpos($tUsuario->modulos, "gestionPronatel") == true)
                {
                    $sessionManager->put('Person',$tUsuario);
                    $sessionManager->put('rol','Gestor de tabletas');
                    return redirect('gestionPronatel/inicio');
                }
                else
                {
                    return $this->helperdrea->redirectError('No tiene acceso al modulo.', 'user/loginTabletasPronatel');
                }
                
            }

            return $this->helperdrea->redirectError('La contraseña es incorrecta.', 'user/loginTabletasPronatel');
        }
        return view('gestionPronatel/login');
    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
}
