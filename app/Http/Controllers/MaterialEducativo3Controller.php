<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

use App\Model\TArchivospecialist;

class MaterialEducativo3Controller extends Controller
{
    public function actionMostrar3(Request $request)
    {
        return view('portal/materialEducativo3/mostrar3');
    }
    public function actionBuscarSemana3(Request $request)
    {
        // dd($request->data);
        $array = explode("-", $request->data);


        if($array[1]!='sec')
        {
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana',$array[2])
                ->where('contenido','otros')
                ->get();
            $data = $array[0].'-'.$array[1];
            // dd($listTae[0]->nombre);
            return view('portal/materialEducativo3/buscarSemana3',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$data,'semana'=>$array[2]]);
        }
        else
        {
            // dd($request->data);
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana',$array[3])
                ->where('contenido','otros')
                ->get();
            $data = $array[0].'-'.$array[1].'-'.$array[2];
            return view('portal/materialEducativo3/buscarSemana3',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$data,'semana'=>$array[3]]);
        }
    }
    public function actionBuscar3(Request $request)
    {
    	$array = explode("-", $request->data);

    	if($array[1]!='sec')
    	{
	    	$listGuias = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana9')
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana9')
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[0].'%')
                ->where('grado',$array[1])
                ->where('semana','semana9')
                ->where('contenido','otros')
                ->get();
            
	    	// dd($listTae[0]->nombre);
	    	return view('portal/materialEducativo3/buscar3',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$request->data]);
    	}
    	else
    	{
            // dd($request->all());
            // echo 'es secundaria';
            $listGuias = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana9')
                ->where('contenido','guias')
                ->get();
            $listRecursos = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana9')
                ->where('contenido','recursos')
                ->get();
            $listOtros = TArchivospecialist::where('cnivel','like','%'.$array[1].'%')
                ->where('grado',$array[0])
                ->where('curso',$array[2])
                ->where('semana','semana9')
                ->where('contenido','otros')
                ->get();
            return view('portal/materialEducativo3/buscarCurso3',['listGuias'=>$listGuias,'listRecursos'=>$listRecursos,'listOtros'=>$listOtros,'data'=>$request->data]);
    	}
    }
}
