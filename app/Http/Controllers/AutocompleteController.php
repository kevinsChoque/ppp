<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;

use App\Model\TFuncionario;
use App\Model\TDocente;
use App\Model\TCurso;

use DB;

class AutocompleteController extends BaseController
{
    public function actionListUnidad(Request $request, SessionManager $sessionManager, Encrypter $encrypter)
    {
        if($request->get('query'))
        {
            $data = DB::table('tcurso')
                ->where('nombre','like','%'.$request->get('query').'%')
                ->get();
            $output = '<ul class="dropdown-menu px-2" style="display:block;position:relative;">';
            foreach ($data as $row) 
            {   $output.='<li><a>'.$row->nombre.'</a></li>';}
            $output .= '</ul>';
            echo $output;
        }
    }
}
