<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\File;
use Session;
use DB;
use Mail;
use App\Model\TCurso;
use App\Model\TDocente;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TUnidad;
use App\Model\TDetallecursopordocente;
use App\Model\TCursoxdocente;

class FileUnidadController extends Controller
{
    protected $documentos = array("pdf", "docx", "xlsx");

    public function actionInsertUnidad(Request $request,SessionManager $sessionManager)
    {
    	if($_POST)
        { 
            try
            {
                if($request->hasFile('file'))
                {
                    DB::beginTransaction();

                    $tUnidad= new TUnidad();
                    $tUnidad->idcd=$request->idcd;
                    $tUnidad->tipoUnidad=$request->selectTUnidad;
                    $tUnidad->nombre=$request->nombre;
                    
                    // $f = date_create($request->fechaPertenece);
                    // $f = date_format($f,'Y-m-d');
                    // // date("m", $f);
                    // echo(date("F", strtotime($request->fechaPertenece)));exit();
                    $tUnidad->fechaPertenece=$request->fechaPertenece;
                    $tUnidad->comentario=$request->comentario;
                    $tUnidad->formato=strtolower($request->file('file')->getClientOriginalExtension());
                    $tUnidad->peso=round($request->file('file')->getSize()/1024).'-kb';
                    $tUnidad->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';
                    $tUnidad->createddate=date('Y-m-d H:m:s');

                    $tUnidad->save();

                    $request->file('file')->move(public_path().'/fileunid'.'/',session()->get('Person')->dni.'-'.$tUnidad->idunidad.'.'.$tUnidad->formato);
                
                    DB::commit();
                    
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileUnidad/insertUnidad');
                }
                else 
                {
                	return $this->helperdrea->redirectError('Ocurrió un error.', 'fileUnidad/insertUnidad');
                }
            }

            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal.', 'fileDcte/insertUnidad');
            }
        }

    	$nivel = session()->get('Person')->tDocente->tIIEE->nivelm;
    	$tCursoxDocente=TCursoxDocente::whereRaw('dni=? and nombreArchivo=?',[session()->get('Person')->dni,'Plan anual'])->get();
    	// echo($tCursoxDocente[1]->tunidad);exit();
    	$listTunidad = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->where('tcursoxdocente.dni',session()->get('Person')->dni)
                ->get();
// echo($listTunidad);exit();
        return view('fileDcte/insertUnidad',['nivel'=>$nivel,'tCursoxDocente'=>$tCursoxDocente,'listTunidad'=>$listTunidad]);
    }
    public function actionDeleteUnidad($idunidad=null)
    {
        $tUnidad=TUnidad::find($idunidad);
        // echo($idunidad);exit();
        if($tUnidad!=null)
        {
            if($tUnidad->delete())
            {
                $rutaArchivo = public_path().'/fileunid/'.session()->get('Person')->dni.'-'.$tUnidad->idunidad.'.'.$tUnidad->formato;

                if(File::delete($rutaArchivo))
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileUnidad/insertUnidad');
                }
                else
                {
                    return $this->helperdrea->redirectError('No se pudo eliminar el archivo.', 'fileUnidad/insertUnidad');
                }
            }
        }

        return $this->helperdrea->redirectError('Ocurrió un error.', 'insert/file');

    }
    
    public function actionEditUnidad(Request $request,SessionManager $sessionManager)
    {
        $tUnidad=TUnidad::find($request->idunidad);
        //--
        if($_POST)
        {
            $tUnidad->tipoUnidad=$request->selectTUnidad;
            $tUnidad->nombre=$request->nombre;
            $tUnidad->fechaPertenece=$request->fechaPertenece;
            $tUnidad->comentario=$request->comentario;
            $tUnidad->estadoArchivo=$request->estadoArchivo=='publico'?'1':'0';
            // echo($tUnidad);exit();

            $file = $request->file('file');
            
            if($file!='')
            {
                // echo $file;exit();
                $rutaArchivo = public_path().'/fileunid/'.session()->get('Person')->dni.'-'.$tUnidad->idunidad.'.'.$tUnidad->formato;
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';

                if(File::delete($rutaArchivo))
                {
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/fileunid';
                        $fileName = session()->get('Person')->dni.'-'.$tUnidad->idunidad.'.'.$formato;
                        
                        $file->move($ruta,$fileName);

                        $tUnidad->formato = $formato;
                        $tUnidad->peso = $peso;

                        if($tUnidad->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileUnidad/insertUnidad');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error.', 'fileUnidad/insertUnidad');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectError('Ocurrió un error.', 'fileUnidad/insertUnidad');
                }
            }
            else
            {
                if($tUnidad->save())
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileUnidad/insertUnidad');

                }
            }
        }
        //--
        return response()->json(['data'=>$tUnidad]);
        // echo $request->idarchivoie;
    }
    public function actionInsertUS(Request $request,SessionManager $sessionManager)
    {
        return view('fileDcte/insertUS');
    }
    public function actionShowFormInsertU(Request $request,SessionManager $sessionManager)
    {
    	// $idgrado=TCursoxdocente::find($request->idcd)->tGrado->idgrado;
    	
    	// $listTunidad = TUnidad::select('TUnidad.*')
     //            ->join('TCursoxdocente', 'TCursoxdocente.idcd', '=', 'TUnidad.idcd')
     //            ->join('grado', 'grado.idgrado', '=', 'TCursoxdocente.idgrado')
     //            ->where('TCursoxdocente.dni',session()->get('Person')->dni)
     //            ->where('grado.idgrado',$idgrado)
     //            ->get();

        $listTunidad = TUnidad::select('tunidad.*')
            ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
            ->join('grado', 'grado.idgrado', '=', 'tcursoxdocente.idgrado')
            ->where('tcursoxdocente.idcd',$request->idcd)
            ->get();

        return view('fileDcte/showFormInsertU',['idcd'=>$request->idcd,'listTunidad'=>$listTunidad]);
    }
    
    public function actionListFileForCurso(Request $request,SessionManager $sessionManager)
    {
        return view('fileDcte/listFileForCurso');   
    }
}