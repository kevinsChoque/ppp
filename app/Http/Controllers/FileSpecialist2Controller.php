<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TGrado;
use App\Model\TUnidad;
use App\Model\TSesion;
use App\Model\TDocente;
use App\Model\TArchivoie;
use App\Model\TCursoxdocente;
use App\Model\TIIEE;
use App\Model\TArchivospecialist;

class FileSpecialist2Controller extends Controller
{
	protected $documentos = array("pdf", "docx", "xlsx");

    public function actionSearchDocGes(Request $request,SessionManager $sessionManager)
    {
        return view('fileSpecialist2/searchDocGes');
    }
    public function actionListDocGes(Request $request,SessionManager $sessionManager)
    {
        $list=TArchivoie::whereRaw('idie=?',$request->cm)->get();
        // echo $list[0]->tIIEE->ie_nombre;//exit();
        return view('fileSpecialist2/listDocGes',['list'=>$list]);
    }
    public function actionSearchPcDcte(Request $request,SessionManager $sessionManager)
    {
return view('fileSpecialist2/searchPcDcte');
    }
    public function actionListListPcDcte(Request $request,SessionManager $sessionManager)
    {
        if($request->tipoList=='plana')
        {
            $list = TCursoxdocente::select('tcursoxdocente.*')
                ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                ->where('iiee.codigomodular',$request->cm)
                ->get();
            return view('fileSpecialist2/listPcDcte/listPa',['list'=>$list]);
        }
        if($request->tipoList=='unidad')
        {
            if($request->mes!='' && $request->fi=='' && $request->ff=='')
            {
                $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'TUnidad.idcd')
                ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                ->where('iiee.codigomodular',$request->cm)
                ->where('tunidad.fechaPertenece',$request->mes)
                ->get();
                return view('fileSpecialist2/listPcDcte/listUnidad',['list'=>$list]);
            }
            $fi = date_create($request->fi);
            $fi = date_format($fi,'y-m-d h:i:s');
            $ff = date_create($request->ff);
            $ff = date_format($ff,'y-m-d h:i:s');

            if($request->fi!='' && $request->ff!='' && strtotime($ff)>=strtotime($fi) && $request->mes=='')
            {
                $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                ->where('iiee.codigomodular',$request->cm)
                ->whereBetween('tunidad.createddate', [$fi, $ff])
                ->get();
                return view('fileSpecialist2/listPcDcte/listUnidad',['list'=>$list]);
            }
            $list = TUnidad::select('tunidad.*')
                ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                ->where('iiee.codigomodular',$request->cm)
                ->get();
            
            return view('fileSpecialist2/listPcDcte/listUnidad',['list'=>$list]);
        }
        if($request->tipoList=='sesion')
        {
            if($request->fi=='' && $request->ff=='')
            {
                $list = TSesion::select('tsesion.*')
                    ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                    ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                    ->where('iiee.codigomodular',$request->cm)
                    ->get();
                return view('fileSpecialist2/listPcDcte/listSesion',['list'=>$list]);
            }

            $fi = date_create($request->fi);
            $fi = date_format($fi,'y-m-d h:i:s');
            $ff = date_create($request->ff);
            $ff = date_format($ff,'y-m-d h:i:s');

            if($request->fi!='' && $request->ff!='' && strtotime($ff)>=strtotime($fi))
            {
                $list = TSesion::select('tsesion.*')
                    ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                    ->join('iiee', 'iiee.codigomodular', '=', 'docente.ie')
                    ->where('iiee.codigomodular',$request->cm)
                    ->whereBetween('tsesion.createddate', [$fi, $ff])
                    ->get();
                return view('fileSpecialist2/listPcDcte/listSesion',['list'=>$list]);
            }
            return view('fileSpecialist2/listPcDcte/listSesion',['list'=>[]]);
        }
        if($request->tipoList=='resumen')
        {
            $list = TDocente::select()->where('ie',$request->cm)->get();
                // echo $list;
            return view('fileSpecialist2/listPcDcte/summary',['list'=>$list]);
        }
    }
}
