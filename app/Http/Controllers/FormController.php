<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\File;
 

use App\Model\TIe;
use App\Model\TFormulario;


class FormController extends Controller
{
    public function actionFormulario(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return view('formulario');
    }
    public function actionBuscar(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        $tTie = TIe::whereRaw('COD_MOD=?',$request->cm)->first();
    	$tTformulario = TFormulario::whereRaw('cm=?',$request->cm)->first();
        $mensaje=null;
        $exist='0';
        if($tTie!=null)
        {
            $exist='1';
        }
        if($tTformulario!=null)
        {
            $mensaje='LA IE YA SE REGISTRO, LOS DATOS INGRESADOS SE SOBREESCRIBIRA UNA VEZ REALIZE CLIC EN EL BOTON DE -REGISTRO-';
        }
        return response()->json(['data'=>$tTie,'mensaje'=>$mensaje,'exist'=>$exist,'tformulario'=>$tTformulario]);
    }
    public function actionRegistrar(Request $request,SessionManager $sessionManager)
    {
        $tTie=TFormulario::whereRaw('cm=?',$request->hcm)->first();
        if($tTie==null)
        {
            $tTie = new TFormulario();
        }
        $tTie->cm=$request->hcm;

        $tTie->cam0=$request->cam0;
// 2
        $tTie->cam21=$request->cam21;
        $tTie->cam22=$request->cam22;
        $tTie->cam23=$request->cam23;
        $tTie->cam24=$request->cam24;
        $tTie->cam25=$request->cam25;
        $tTie->cam26=$request->cam26;
// 3c
        $tTie->camc31=$request->camc31;
        $tTie->camc32=$request->camc32;
        $tTie->camc33=$request->camc33;
        $tTie->camc34=$request->camc34;
        $tTie->camc35=$request->camc35;
        $tTie->camc36=$request->camc36;
// 3e
        $tTie->came31=$request->came31;
        $tTie->came32=$request->came32;
        $tTie->came33=$request->came33;
        $tTie->came34=$request->came34;
        $tTie->came35=$request->came35;
        $tTie->came36=$request->came36;

        $tTie->came37=$request->came37;
        $tTie->came38=$request->came38;
        $tTie->came39=$request->came39;
        $tTie->came310=$request->came310;
        $tTie->came311=$request->came311;
        $tTie->came312=$request->came312;
        $tTie->came313=$request->came313;
        $tTie->came314=$request->came314;
        $tTie->came315=$request->came315;
        $tTie->came316=$request->came316;
        $tTie->came317=$request->came317;
        $tTie->came318=$request->came318;
        $tTie->came319=$request->came319;
        $tTie->came320=$request->came320;
        $tTie->came321=$request->came321;

        $tTie->came322=$request->came322;
        $tTie->came323=$request->came323;
        $tTie->came324=$request->came324;
        $tTie->came325=$request->came325;
        $tTie->came326=$request->came326;
        $tTie->came327=$request->came327;
        $tTie->came328=$request->came328;
        $tTie->came329=$request->came329;
        $tTie->came330=$request->came330;
        $tTie->came331=$request->came331;
        $tTie->came332=$request->came332;
        $tTie->came333=$request->came333;
        $tTie->came334=$request->came334;

        $tTie->came335=$request->came335;
        $tTie->came336=$request->came336;
// 4c
        $tTie->camc41=$request->camc41;
        $tTie->camc42=$request->camc42;
        $tTie->camc43=$request->camc43;
        $tTie->camc44=$request->camc44;
        $tTie->camc45=$request->camc45;
        $tTie->camc46=$request->camc46;
        $tTie->camc47=$request->camc47;
        $tTie->camc48=$request->camc48;
        $tTie->camc49=$request->camc49;
        $tTie->camc410=$request->camc410;
        $tTie->camc411=$request->camc411;
        $tTie->camc412=$request->camc412;
// 4e
        $tTie->came41=$request->came41;
        $tTie->came42=$request->came42;
        $tTie->came43=$request->came43;
        $tTie->came44=$request->came44;
        $tTie->came45=$request->came45;
        $tTie->came46=$request->came46;

        $tTie->came47=$request->came47;
        $tTie->came48=$request->came48;
        $tTie->came49=$request->came49;
        $tTie->came410=$request->came410;
        $tTie->came411=$request->came411;
        $tTie->came412=$request->came412;
        $tTie->came413=$request->came413;
        $tTie->came414=$request->came414;
        $tTie->came415=$request->came415;
        $tTie->came416=$request->came416;
        $tTie->came417=$request->came417;
        $tTie->came418=$request->came418;
        $tTie->came419=$request->came419;
        $tTie->came420=$request->came420;
        $tTie->came421=$request->came421;

        $tTie->came422=$request->came422;
        $tTie->came423=$request->came423;
        $tTie->came424=$request->came424;
        $tTie->came425=$request->came425;
        $tTie->came426=$request->came426;
        $tTie->came427=$request->came427;
        $tTie->came428=$request->came428;
        $tTie->came429=$request->came429;
        $tTie->came430=$request->came430;
        $tTie->came431=$request->came431;
        $tTie->came432=$request->came432;
        $tTie->came433=$request->came433;
        $tTie->came434=$request->came434;

        $tTie->came435=$request->came435;
        $tTie->came436=$request->came436;
// 5
$tTie->cam51=$request->cam51;
$tTie->cam52=$request->cam52;
$tTie->cam53=$request->cam53;
$tTie->cam54=$request->cam54;
$tTie->cam55=$request->cam55;
$tTie->cam56=$request->cam56;
$tTie->cam57=$request->cam57;
// 6
$tTie->cam61=$request->cam61;
$tTie->cam62=$request->cam62;
$tTie->cam63=$request->cam63;
$tTie->cam64=$request->cam64;
$tTie->cam65=$request->cam65;
$tTie->cam66=$request->cam66;
$tTie->cam67=$request->cam67;
$tTie->cam68=$request->cam68;
$tTie->cam69=$request->cam69;
$tTie->cam610=$request->cam610;

$tTie->cam611=$request->cam611;
$tTie->cam612=$request->cam612;
$tTie->cam613=$request->cam613;
$tTie->cam614=$request->cam614;
$tTie->cam615=$request->cam615;
$tTie->cam616=$request->cam616;
$tTie->cam617=$request->cam617;
$tTie->cam618=$request->cam618;
$tTie->cam619=$request->cam619;
$tTie->cam620=$request->cam620;

$tTie->cam621=$request->cam621;
// 7
$tTie->cam71=$request->cam71;
$tTie->cam72=$request->cam72;
$tTie->cam73=$request->cam73;
$tTie->cam74=$request->cam74;
$tTie->cam75=$request->cam75;
$tTie->cam76=$request->cam76;

$tTie->cam77=$request->cam77;
$tTie->cam78=$request->cam78;
$tTie->cam79=$request->cam79;
$tTie->cam710=$request->cam710;
$tTie->cam711=$request->cam711;
$tTie->cam712=$request->cam712;
$tTie->cam713=$request->cam713;


        $tTie->serEnU=$request->serEnU;
        $tTie->serFun=$request->serFun;
        $tTie->serDan=$request->serDan;
        $tTie->serGua=$request->serGua;
        $tTie->serMan=$request->serMan;
        $tTie->serCom=$request->serCom;

        $tTie->rouEnU=$request->rouEnU;
        $tTie->rouFun=$request->rouFun;
        $tTie->rouDan=$request->rouDan;
        $tTie->rouGua=$request->rouGua;
        $tTie->rouMan=$request->rouMan;
        $tTie->rouCom=$request->rouCom;

        $tTie->swiEnU=$request->swiEnU;
        $tTie->swiFun=$request->swiFun;
        $tTie->swiDan=$request->swiDan;
        $tTie->swiGua=$request->swiGua;
        $tTie->swiMan=$request->swiMan;
        $tTie->swiCom=$request->swiCom;

        $tTie->apaEnU=$request->apaEnU;
        $tTie->apaFun=$request->apaFun;
        $tTie->apaDan=$request->apaDan;
        $tTie->apaGua=$request->apaGua;
        $tTie->apaMan=$request->apaMan;
        $tTie->apaCom=$request->apaCom;

        $tTie->apeEnU=$request->apeEnU;
        $tTie->apeFun=$request->apeFun;
        $tTie->apeDan=$request->apeDan;
        $tTie->apeGua=$request->apeGua;
        $tTie->apeMan=$request->apeMan;
        $tTie->apeCom=$request->apeCom;

        $tTie->lapEnU=$request->lapEnU;
        $tTie->lapFun=$request->lapFun;
        $tTie->lapDan=$request->lapDan;
        $tTie->lapGua=$request->lapGua;
        $tTie->lapMan=$request->lapMan;
        $tTie->lapCom=$request->lapCom;

        $tTie->comEnU=$request->comEnU;
        $tTie->comFun=$request->comFun;
        $tTie->comDan=$request->comDan;
        $tTie->comGua=$request->comGua;
        $tTie->comMan=$request->comMan;
        $tTie->comCom=$request->comCom;

        $tTie->proEnU=$request->proEnU;
        $tTie->proFun=$request->proFun;
        $tTie->proDan=$request->proDan;
        $tTie->proGua=$request->proGua;
        $tTie->proMan=$request->proMan;
        $tTie->proCom=$request->proCom;

        $tTie->smaEnU=$request->smaEnU;
        $tTie->smaFun=$request->smaFun;
        $tTie->smaDan=$request->smaDan;
        $tTie->smaGua=$request->smaGua;
        $tTie->smaMan=$request->smaMan;
        $tTie->smaCom=$request->smaCom;

        $tTie->piiEnU=$request->piiEnU;
        $tTie->piiFun=$request->piiFun;
        $tTie->piiDan=$request->piiDan;
        $tTie->piiGua=$request->piiGua;
        $tTie->piiMan=$request->piiMan;
        $tTie->piiCom=$request->piiCom;

        $tTie->parEnU=$request->parEnU;
        $tTie->parFun=$request->parFun;
        $tTie->parDan=$request->parDan;
        $tTie->parGua=$request->parGua;
        $tTie->parMan=$request->parMan;
        $tTie->parCom=$request->parCom;

        $tTie->potEnU=$request->potEnU;
        $tTie->potFun=$request->potFun;
        $tTie->potDan=$request->potDan;
        $tTie->potGua=$request->potGua;
        $tTie->potMan=$request->potMan;
        $tTie->potCom=$request->potCom;


        // $tTie->cantAecWeb=$request->cantAecWeb;
        // $tTie->cantAecTv=$request->cantAecTv;
        // $tTie->cantAecR=$request->cantAecR;
        // $tTie->cantFc=$request->cantFc;
        // $tTie->cantNoA=$request->cantNoA;
        // $tTie->cantVideoL=$request->cantVideoL;
        // $tTie->cantOtrosM=$request->cantOtrosM;

        // $tTie->comDiario=$request->comDiario;
        // $tTie->comUnaS=$request->comUnaS;
        // $tTie->comCadaQui=$request->comCadaQui;
        // $tTie->comUnaMes=$request->comUnaMes;
        // $tTie->noCom=$request->noCom;

        // $tTie->eviImpSob=$request->eviImpSob;
        // $tTie->eviFotWas=$request->eviFotWas;
        // $tTie->eviPorCon=$request->eviPorCon;
        // $tTie->eviEmail=$request->eviEmail;
        // $tTie->eviGooDri=$request->eviGooDri;
        // $tTie->eviOtro=$request->eviOtro;


        $tTie->save();
        $sessionManager->put('mensaje','LA INFORMACION SE REGISTRO EXITOSAMENTE!');
        return view('formulario');
    }

}
