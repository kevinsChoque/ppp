<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;
use Mail;

use App\Model\TSocioemocional;
use App\Model\TUsuario;

class SocioEmocionalController extends Controller
{
    public function actionPortal()
    {
        return view('index');
    }
    public function actionInicio()
    {
        return view('socioEmocional/inicio');
    }
    public function actionRegistrar(Request $request)
    {
        if($_POST)
        {
            
            $tSocioemocional=new TSocioemocional();
            // $tSocioemocional->idse=TSocioemocional::count()+1;
            $tSocioemocional->dni=session()->get('Person')->dni;
            $tSocioemocional->ugel=$request->ugel;
            $tSocioemocional->categoria=$request->categoria;
            $tSocioemocional->nombre=$request->nombre;
            $tSocioemocional->descripcion=$request->descripcion;
            
            $tSocioemocional->earchivo=$request->earchivo;
            $tSocioemocional->evideo=$request->evideo;
            $tSocioemocional->eaudio=$request->eaudio;
            if($tSocioemocional->save())
            {
                if($request->hasFile('archivo'))
                {
                    $tSocioemocional=TSocioemocional::all()->last();
                    $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                    $request->file('archivo')->move(public_path().'/filesocioemocional'.'/',$tSocioemocional->idse.'_'.$nombreArchivo);
                    $tSocioemocional->archivo=$tSocioemocional->idse.'_'.$nombreArchivo;
                    $tSocioemocional->save();
                }
            }
            
            
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/registrar');
        }
        return view('socioEmocional/registrar');
    }
    public function actionListar()
    {
        $list=TSocioemocional::whereRaw('dni=?',session()->get('Person')->dni)->get();
        return view('socioEmocional/listar',['list'=>$list]);
    }
    public function actionEditar(Request $request)
    {
        $tSocioemocional = TSocioemocional::find($request->idse);
        if($_POST)
        {
            $tSocioemocional->ugel=$request->ugel;
            $tSocioemocional->categoria=$request->categoria;
            $tSocioemocional->nombre=$request->nombre;
            $tSocioemocional->descripcion=$request->descripcion;
            $tSocioemocional->earchivo=$request->earchivo;
            $tSocioemocional->evideo=$request->evideo;
            $tSocioemocional->eaudio=$request->eaudio;
            if($tSocioemocional->save())
            {
                if($request->hasFile('archivo'))
                {
                    $archivoEliminado=true;
                    if($tSocioemocional->archivo!='')
                    {
                        // dd('Se eliminon el archivo anterior');
                        $rutaArchivo = public_path().'/filesocioemocional/'.$tSocioemocional->archivo;
                        if(!File::delete($rutaArchivo))
                        {
                            $archivoEliminado=false;
                        }
                    }
                    if($archivoEliminado)
                    {
                        // dd('entro ya q no tiene archivo');
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                        $request->file('archivo')->move(public_path().'/filesocioemocional'.'/',$tSocioemocional->idse.'_'.$nombreArchivo);
                        $tSocioemocional->archivo=$tSocioemocional->idse.'_'.$nombreArchivo;
                        // $tSocioemocional->save();
                        if($tSocioemocional->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Error al guardar el nuevo archivo.', 'socioEmocional/listar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Error al eliminar el archivo anterior.', 'socioEmocional/listar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar cambios.', 'socioEmocional/listar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
        }
        
        return response()->json(['data'=>$tSocioemocional]);
    }
    public function actionDelete($idse=null)
    {
        $tSocioemocional=TSocioemocional::find($idse);
        
        if($tSocioemocional!=null)
        {
            if($tSocioemocional->delete())
            {
                if($tSocioemocional->archivo!='')
                {
                    $rutaArchivo = public_path().'/filesocioemocional/'.$tSocioemocional->archivo;
                    if(File::delete($rutaArchivo))
                    {
                        return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo.', 'socioEmocional/listar');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                }
            }
        }

        return $this->helperdrea->redirectError('No se encontro el registro.', 'socioEmocional/listar');

    }
    public function actionLogin(Request $request,SessionManager $sessionManager,Encrypter $encrypter)
    {

        if($_POST)
        {
            // dd('ingreso a socioemocional');
            $sessionManager->flush();
            $tUsuario=TUsuario::find($request->input('dni'));

            if($tUsuario==null)
            {
                return $this->helperdrea->redirectError('El usuario no tiene acceso a la plataforma.', 'user/loginSocioEmocional');
                // dd('El usuario no tiene acceso a la plataforma.');
            }
            
            if($tUsuario!=null && $encrypter->decrypt($tUsuario->password)==$request->input('password'))
            {
                $sessionManager->put('Person',$tUsuario);
                $sessionManager->put('rol','Gestor de materiales');
                return redirect('socioEmocional/inicio');
            }

            return $this->helperdrea->redirectError('La contraseña es incorrecta.', 'socioEmocional/persona/login');
        }
        return view('socioEmocional/persona/login');
    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
    // portal
    public function actionMaterial($categoria=null)
    {
        if($categoria=='1categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','La persona')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'LA PERSONA']);
        }
        if($categoria=='2categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','Trabajo en equipo')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'TRABAJO EN EQUIPO']);
        }
        if($categoria=='3categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','Las emociones')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'LAS EMOCIONES']);
        }
        if($categoria=='4categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','Alertas')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'ALERTAS']);
        }
        if($categoria=='5categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','La familia')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'LA FAMILIA']);
        }
        if($categoria=='6categoria')
        {
            $list=TSocioemocional::whereRaw('categoria=?','COVID')->get();
            return view('portal/socioEmocional/material',['list'=>$list,'nombre'=>'COVID']);
        }
    }
    
}
