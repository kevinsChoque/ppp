<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;

use Session;
use DB;
use Mail;

use App\Model\TPersona;
use App\Model\TUnidad;
use App\Model\TSesion;
use App\Model\TDocente;
use App\Model\TUgel;
use App\Model\TFuncionario;
use App\Model\TCursoxdocente;
use App\Model\TUbigeo;
use App\Model\TArchivoie;
use App\Model\TArchivospecialist;
use App\Model\TIIEE;

class PersonController extends Controller
{
    public function actionPortal()
    {
        return view('index');
    }
    public function actionIndex(Request $request,SessionManager $sessionManager)
    {
        $person = $request->session()->get('Person');
        $rol = $request->session()->get('rol');
        // dd($person);

        if($rol=='Director' || $rol=='Docente')//entra si es director o docente
        {
            $ie = $person->tDocente->tIIEE;
            $tUbigeo = $person->tDocente->tIIEE->tUbigeo_;
            $cantFile = '';
            if($rol=='Director')
            {
                $listAshared = TArchivospecialist::select()
                    ->where('cie','LIKE','%'.$ie->codigomodular.'%')
                    ->orderBy('createddate', 'desc')
                    ->get();
                // SELECT ta.subidopor,count(ta.dni) FROM tcursoxdocente ta inner join docente d on ta.dni=d.dni where d.ie=0404285 group by ta.subidoPor
                // $listSubidoPor = TCursoxdocente::select('tcursoxdocente.subidopor',DB::raw('count(tcursoxdocente.dni) as total'))
                //     ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                //     ->where('docente.ie',$ie->codigomodular)
                //     ->groupBy('tcursoxdocente.subidopor')
                //     ->get();

                $cantPa = TCursoxdocente::select()
                    ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                    ->where('docente.ie',$ie->codigomodular)
                    ->get()->count();
                $cantUnid = TUnidad::select()
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                    ->where('docente.ie',$ie->codigomodular)
                    ->get()->count();
                $cantSesi = TSesion::select()
                    ->join('tunidad', 'tunidad.idunidad', '=', 'tsesion.idunidad')
                    ->join('tcursoxdocente', 'tcursoxdocente.idcd', '=', 'tunidad.idcd')
                    ->join('docente', 'docente.dni', '=', 'tcursoxdocente.dni')
                    ->where('docente.ie',$ie->codigomodular)
                    ->get()->count();
                    $cantFile=array();
                    array_push($cantFile,$cantPa,$cantUnid,$cantSesi);
            }
            else
            {
                $listAshared = TArchivoie::select()
                ->where('idie',$ie->codigomodular)
                ->where('compartido','!=','no')
                ->where('compartido','LIKE','%'.$person->dni.'%')
                ->orWhere('compartido','=','todos')
                ->orderBy('createddate', 'desc')
                ->get();
                // SELECT * FROM `tarchivoie` where idie=0404285 and compartido!='no' and (compartido like '%21486317%' or compartido='todos')
            }
            
            $sessionManager->put('listAshared',$listAshared);

            $provincia=DB::table('ubigeo')
           ->select('ubigeo_nombre')
           ->where('id_region', $tUbigeo->id_region)
           ->where('id_provincia',$tUbigeo->id_provincia)
           ->where('id_distrito',0)
           ->first();

           $tArchivos=TCursoxdocente::whereRaw('dni=?',session()->get('Person')->dni)->get();

            return view('person/index',['ie'=>$ie,'provincia'=>$provincia->ubigeo_nombre,'tArchivo'=>$tArchivos,'cantFile'=>$cantFile]);
        }
        if($rol=='Especialista')//entra si es especialista
        {
            // $listAshared = TArchivoie::select()
            //     ->where('idie','0404285')
            //     ->where('compartido','!=','no')
            //     ->orderBy('createddate', 'desc')
            //     ->get();

            $listAshared = TArchivospecialist::select()
            ->where('otherspecialist','1')
            ->orderBy('createddate', 'desc')
            ->get();
            
            $sessionManager->put('listAshared',$listAshared);
            
            $listPa = TIIEE::select('ugel.idugel',DB::raw('count(docente.dni) as np'))
                    ->join('ugel', 'ugel.idugel', '=', 'iiee.ugelid')
                    ->join('docente', 'docente.ie', '=', 'iiee.codigomodular')
                    ->join('tcursoxdocente', 'tcursoxdocente.dni', '=', 'docente.dni')
                    ->groupBy('ugel.idugel')
                    ->get();
            $listU = TIIEE::select('ugel.idugel',DB::raw('count(tunidad.idunidad) as nunidad'))
                    ->join('ugel', 'ugel.idugel', '=', 'iiee.ugelid')
                    ->join('docente', 'docente.ie', '=', 'iiee.codigomodular')
                    ->join('tcursoxdocente', 'tcursoxdocente.dni', '=', 'docente.dni')
                    ->join('tunidad', 'tunidad.idcd', '=', 'tcursoxdocente.idcd')
                    ->groupBy('ugel.idugel')
                    ->get();
            $listS = TIIEE::select('ugel.idugel',DB::raw('count(tsesion.idsesion) as nsesion'))
                    ->join('ugel', 'ugel.idugel', '=', 'iiee.ugelid')
                    ->join('docente', 'docente.ie', '=', 'iiee.codigomodular')
                    ->join('tcursoxdocente', 'tcursoxdocente.dni', '=', 'docente.dni')
                    ->join('tunidad', 'tunidad.idcd', '=', 'tcursoxdocente.idcd')
                    ->join('tsesion', 'tsesion.idunidad', '=', 'tunidad.idunidad')
                    ->groupBy('ugel.idugel')
                    ->get();

            $listNie = TIIEE::select('ugel.idugel','ugel.ugel_nombre',DB::raw('count(iiee.codigomodular) as nie'))
                    ->join('ugel', 'ugel.idugel', '=', 'iiee.ugelid')
                    ->groupBy('ugel.idugel','ugel.ugel_nombre')
                    ->get();
            $listDcte = TIIEE::select('ugel.idugel','ugel.ugel_nombre',DB::raw('count(docente.dni) as ndcte'))
                    ->join('ugel', 'ugel.idugel', '=', 'iiee.ugelid')
                    ->join('docente', 'docente.ie', '=', 'iiee.codigomodular')
                    ->groupBy('ugel.idugel','ugel.ugel_nombre')
                    ->get();

            $ugel=30001;$valp=0;$valu=0;$vals=0;

            for($i=0;$i<=7;$i++)
            {
                if(isset($listPa[$valp]->idugel))
                {
                    if($listPa[$valp]->idugel==$ugel)
                    {
                        $rowp['idugel']=$ugel;
                        $rowp['np']=$listPa[$valp]->np;
                        $valp=$valp+1;
                    }
                    else
                    {
                        $rowp['idugel']=$ugel; $rowp['np']=0;
                    }
                }
                else
                {
                    $rowp['idugel']=$ugel; $rowp['np']=0;
                }
                // ------------
                if(isset($listU[$valu]->idugel))
                {
                    if($listU[$valu]->idugel==$ugel)
                    {
                        $rowu['idugel']=$ugel;
                        $rowu['nu']=$listU[$valu]->nunidad;
                        $valu=$valu+1;
                    }
                    else
                    {
                        $rowu['idugel']=$ugel; $rowu['nu']=0;
                    }
                }
                else
                {
                    $rowu['idugel']=$ugel; $rowu['nu']=0;
                }
                // -----------------
                if(isset($listS[$vals]->idugel))
                {
                    if($listS[$vals]->idugel==$ugel)
                    {
                        $rows['idugel']=$ugel;
                        $rows['ns']=$listS[$vals]->nsesion;
                        $vals=$vals+1;
                    }
                    else
                    {
                        $rows['idugel']=$ugel; $rows['ns']=0;
                    }
                }
                else
                {
                    $rows['idugel']=$ugel; $rows['ns']=0;
                }

                $row1[] = $rowp;$row2[] = $rowu;$row3[] = $rows;
                $ugel=$ugel+1;
            }
            // echo $row1[0]['idugel'];exit();
            return view('person/index',['listPa'=>$row1,'listU'=>$row2,'listS'=>$row3,'listNie'=>$listNie,'listDcte'=>$listDcte]);
        }
    }

    public function actionLogin(Request $request,SessionManager $sessionManager,Encrypter $encrypter)
    {

        if($_POST)
        {
            $sessionManager->flush();
            $tPersona=TPersona::find($request->input('dni'));

            if($tPersona==null)
            {
                return $this->helperdrea->redirectError('El usuario no tiene acceso a la plataforma.', 'user/login');
            }
            
            $tFuncionario=$tPersona->tEspecialista;
            
            if($tFuncionario!=null && $encrypter->decrypt($tFuncionario->password)==$request->input('password'))
            {
                // echo($tFuncionario->password);exit();
                $sessionManager->put('Person',$tPersona);
                $sessionManager->put('rol','Especialista');
                return redirect('/');
            }

            $tDocente=$tPersona->tDocente;
            if($tDocente!=null && $encrypter->decrypt($tDocente->password)==$request->input('password'))
            {
                // echo($tDocente->password);exit();
                $sessionManager->put('Person',$tPersona);
                $sessionManager->put('rol',$tDocente->director==0 || $tDocente->director==null?'Docente':'Director');
                return $this->helperdrea->redirectCorrect('Bienvenido al sistema.', '/');
            }
            return $this->helperdrea->redirectError('l contrseñ es incorrect.', 'user/login');
        }
        return view('person/login');
    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();

        return redirect('/');
    }
}
