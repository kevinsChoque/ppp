<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TPersona;
use App\Model\TDocente;
use App\Model\TFuncionario;
use App\Model\TArchivoie;
use App\Model\TIIEE;
use App\Model\TDetallecursopordocente;

class ShareIeController extends Controller
{
	protected $documentos = array("pdf", "docx", "xlsx");

    public function actionShareFileEdit(Request $request,SessionManager $sessionManager)
    {
        $tArchivoie=TArchivoie::find($request->idarchivoie);
        if($_POST)
        {
            $tArchivoie->nombre = $request->nombre;
            $tArchivoie->comentario = $request->comentario;
            $tArchivoie->video = $request->video;
            $tArchivoie->compartido = $request->listShared;
            // echo $tArchivoie;exit();
            $file = $request->file('file');
            if($file!='')
            {
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';
                if(strpbrk($tArchivoie->nombrereal, '-')!='')
                {
                    $rutaArchivo = public_path().'/fileie/'.$tArchivoie->nombrereal.'.'.$tArchivoie->formato;
                    if(File::delete($rutaArchivo))
                    {
                        if(in_array($formato, $this->documentos))
                        {
                            $ruta = public_path().'/fileIe';
                            $fileName = $tArchivoie->nombrereal.'.'.$tArchivoie->formato;
                            $file->move($ruta,$fileName);

                            $tArchivoie->formato = $formato;
                            $tArchivoie->peso = $peso;

                            if($tArchivoie->save())
                            {
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/shareFile');
                            }
                            else
                            {
                                $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                            }
                        }
                        else
                        {
                            $sessionManager->flash('estado','no tiene el formato correcto');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no se pudo eliminar el archivo');
                    }
                }
                else
                {
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/fileIe';
                        $fileName = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tArchivoie->nombrereal = $tArchivoie->nombrereal.'-'.$tArchivoie->idarchivoie;
                        $tArchivoie->formato = $formato;
                        $tArchivoie->peso = $peso;

                        if($tArchivoie->save())
                        {
                            $sessionManager->flash('estado','se guardo exitosamente');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
            }
            else
            {
                if($tArchivoie->save())
                {
                    // $sessionManager->flash('estado','se guardo exitosamente');
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileIe/shareFile');
                }
            }
            // echo 'llego a redireccdct';exit();
            return redirect('fileIe/shareFile');
        }
        return response()->json(['data'=>$tArchivoie]);
    }
}
