<?php
namespace App\Http\Controllers;

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\File;

use Session;
use DB;
use Mail;

use App\Model\TSocioemocional;
use App\Model\TRecursosaprendizaje;
use App\Model\TUsuario;

class RecursosAprendizajeController extends Controller
{
    public function actionPortal()
    {
        return view('index');
    }
    public function actionInicio()
    {
        return view('recursosAprendizaje/inicio');
    }
    public function actionRegistrar(Request $request)
    {
        if($_POST)
        {
            // dd($request->all());
            $tRecursosaprendizaje=new TRecursosaprendizaje();
            $tRecursosaprendizaje->dni=session()->get('Person')->dni;
            $tRecursosaprendizaje->tipoMaterial=$request->tipoMaterial;
            $tRecursosaprendizaje->ugel=$request->ugel;
            $tRecursosaprendizaje->mes=$request->mes;
            $tRecursosaprendizaje->nivel=$request->nivel;
            $tRecursosaprendizaje->grado=$request->grado;
            $tRecursosaprendizaje->curso=$request->curso;
            $tRecursosaprendizaje->nombre=$request->nombre;
            $tRecursosaprendizaje->descripcion=$request->descripcion;
            $tRecursosaprendizaje->earchivo=$request->earchivo;
            $tRecursosaprendizaje->evideo=$request->evideo;
            $tRecursosaprendizaje->eaudio=$request->eaudio;
            if($tRecursosaprendizaje->save())
            {
                if($request->hasFile('archivo'))
                {
                    $tRecursosaprendizaje=TRecursosaprendizaje::all()->last();
                    $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                    $request->file('archivo')->move(public_path().'/filerecursosaprendizaje'.'/',$tRecursosaprendizaje->idra.'_'.$nombreArchivo);
                    $tRecursosaprendizaje->archivo=$tRecursosaprendizaje->idra.'_'.$nombreArchivo;
                    $tRecursosaprendizaje->save();
                }
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recursosAprendizaje/registrar');
        }
        return view('recursosAprendizaje/registrar');
    }
    public function actionListar()
    {
        $list=TRecursosaprendizaje::whereRaw('dni=?',session()->get('Person')->dni)->get();
        return view('recursosAprendizaje/listar',['list'=>$list]);
    }
    public function actionEditar(Request $request)
    {
        $tSocioemocional = TSocioemocional::find($request->idse);
        if($_POST)
        {
            $tSocioemocional->ugel=$request->ugel;
            $tSocioemocional->categoria=$request->categoria;
            $tSocioemocional->nombre=$request->nombre;
            $tSocioemocional->descripcion=$request->descripcion;
            $tSocioemocional->earchivo=$request->earchivo;
            $tSocioemocional->evideo=$request->evideo;
            $tSocioemocional->eaudio=$request->eaudio;
            if($tSocioemocional->save())
            {
                if($request->hasFile('archivo'))
                {
                    $archivoEliminado=true;
                    if($tSocioemocional->archivo!='')
                    {
                        // dd('Se eliminon el archivo anterior');
                        $rutaArchivo = public_path().'/filesocioemocional/'.$tSocioemocional->archivo;
                        if(!File::delete($rutaArchivo))
                        {
                            $archivoEliminado=false;
                        }
                    }
                    if($archivoEliminado)
                    {
                        // dd('entro ya q no tiene archivo');
                        $nombreArchivo =strtolower(str_replace(' ', '_', $request->archivo->getClientOriginalName()));
                        $request->file('archivo')->move(public_path().'/filesocioemocional'.'/',$tSocioemocional->idse.'_'.$nombreArchivo);
                        $tSocioemocional->archivo=$tSocioemocional->idse.'_'.$nombreArchivo;
                        // $tSocioemocional->save();
                        if($tSocioemocional->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
                        }
                        else
                        {
                            return $this->helperdrea->redirectCorrect('Error al guardar el nuevo archivo.', 'socioEmocional/listar');
                        }
                    }
                    else
                    {
                        return $this->helperdrea->redirectCorrect('Error al eliminar el archivo anterior.', 'socioEmocional/listar');
                    }
                }
            }
            else
            {
                return $this->helperdrea->redirectCorrect('Ocurrio un problema al guardar cambios.', 'socioEmocional/listar');
            }
            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'socioEmocional/listar');
        }
        
        return response()->json(['data'=>$tSocioemocional]);
    }
    public function actionDelete($idra=null)
    {
        $tRecursosaprendizaje=TRecursosaprendizaje::find($idra);
        
        if($tRecursosaprendizaje!=null)
        {
            if($tRecursosaprendizaje->delete())
            {
                if($tRecursosaprendizaje->archivo!='')
                {
                    $rutaArchivo = public_path().'/filerecursosaprendizaje/'.$tRecursosaprendizaje->archivo;
                    if(File::delete($rutaArchivo))
                    {
                        return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recursosAprendizaje/listar');
                    }
                    else
                    {
                        return $this->helperdrea->redirectError('Ocurrió un error al eliminar el archivo.', 'recursosAprendizaje/listar');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'recursosAprendizaje/listar');
                }
            }
        }

        return $this->helperdrea->redirectError('No se encontro el registro.', 'recursosAprendizaje/listar');

    }
    public function actionLogin(Request $request,SessionManager $sessionManager,Encrypter $encrypter)
    {

        if($_POST)
        {
            $sessionManager->flush();
            $tUsuario=TUsuario::find($request->input('dni'));

            if($tUsuario==null)
            {
                return $this->helperdrea->redirectError('El usuario no tiene acceso a la plataforma.', 'user/loginRecursosAprendizaje');
            }
            
            if($tUsuario!=null && $encrypter->decrypt($tUsuario->password)==$request->input('password'))
            {
                if(strpos($tUsuario->modulos, "ra") == true)
                {
                    // dd('ingreso y tiene acceso al modulo');
                    $sessionManager->put('Person',$tUsuario);
                    $sessionManager->put('rol','Gestor de materiales');
                    return redirect('recursosAprendizaje/inicio');
                }
                else
                {
                    // dd('no tiene acceso al modulo');
                    return $this->helperdrea->redirectError('No tiene acceso al modulo.', 'user/loginRecursosAprendizaje');
                }
            }
            return $this->helperdrea->redirectError('La contraseña es incorrecta.', 'user/useloginRecursosAprendizajer');
        }
        return view('recursosAprendizaje/login');
    }
    public function actionLogout(Request $request,SessionManager $sessionManager)
    {
        $sessionManager->flush();
        return redirect('/');
    }
    // portal
    public function actionListarPortal(Request $request)
    {
        $grado=$request->grado+1;
        $list=TRecursosaprendizaje::whereRaw('nivel=? and grado=? and tipoMaterial=? and mes=?',[$request->nivel,$grado,$request->tipo,$request->mes])->get();
        return view('portal/recursosAprendizaje/listarPortal',['list'=>$list]);
    }
}
