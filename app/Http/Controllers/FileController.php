<?php

namespace App\Http\Controllers;
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TDocente;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TDetallecursopordocente;
use App\Model\TCursoxdocente;

class FileController extends Controller
{
    public function actionInsert(Request $request,SessionManager $sessionManager)
    {
        if($_POST)
        { 
            try
            {
                if($request->hasFile('file'))
                {
                    DB::beginTransaction();

                    $tCursoxDocente= new TCursoxdocente();
                    $tCursoxDocente->dni=session()->get('Person')->dni;
                    $tCursoxDocente->nombreArchivo='Plan curricular';
                    $tCursoxDocente->formato=strtolower($request->file('file')->getClientOriginalExtension());
                    $tCursoxDocente->subidoPor=$request->input('radioarchivo');
                    $tCursoxDocente->peso=round($request->file('file')->getSize()/1024).'-kb';
                    $tCursoxDocente->nivel=session()->get('Person')->tDocente->tIIEE->nivelm;
                    $tCursoxDocente->periodoAcademico=$request->input('periodo');
                    $tCursoxDocente->comentario=$request->input('txtComentario');
                    $tCursoxDocente->estadoArchivo='0';
    
                    $tCursoxDocente->save();

                    $request->file('file')->move(public_path().'/filedcte'.'/',session()->get('Person')->dni.'-'.$tCursoxDocente->idcd.'.'.$tCursoxDocente->formato);
                
                    if($request->input('radioarchivo')=='Seccion' || $request->input('radioarchivo')=='Ciclo')
                    {
                        if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Secundaria')!==false)
                        {
                            $tCurso=TCurso::whereRaw('nivel=?','Secundaria')->get();
                        }
                        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Primaria')!==false)
                        {
                            $tCurso=TCurso::whereRaw('nivel=?','primaria')->get();
                        }
                        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!==false)
                        {
                            $tCurso=TCurso::whereRaw('nivel=?','Inicial')->get();
                        }
    
                        foreach($tCurso as $item)
                        {
                            $tDetalleCursoporDocente= new TDetallecursopordocente();
    
                            $tDetalleCursoporDocente->idcd=$tCursoxDocente->idcd;   
                            $tDetalleCursoporDocente->idcurso=$item->idcurso;
                            $tDetalleCursoporDocente->idSeccion=$request->input('selectSeccion');
                            $tDetalleCursoporDocente->idgrado=$request->input('selectGrado');
    
                            $tDetalleCursoporDocente->save();
                        }
                    }
                    else
                    {
                        $tDetalleCursoporDocente= new TDetallecursopordocente();
    
                        $tDetalleCursoporDocente->idcd=$tCursoxDocente->idcd;   
                        $tDetalleCursoporDocente->idcurso=$request->input('selectCurso');
                        $tDetalleCursoporDocente->idSeccion=$request->input('selectSeccion');
                        $tDetalleCursoporDocente->idgrado=$request->input('selectGrado');
    
                        $tDetalleCursoporDocente->save();
                    }
                    DB::commit();
                    
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/file');
                }
                else 
                {
                return $this->helperdrea->redirectError('Ocurrió un error.', 'insert/file');
                }
            }

            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
            }
        }

        if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Secundaria')!==false)
        {
            $tGrado=TGrado::all()->take(5);
            $tCurso=TCurso::whereRaw('nivel=?','Secundaria')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Primaria')!==false)
        {
            $tGrado=TGrado::all()->take(6);
            $tCurso=TCurso::whereRaw('nivel=?','Primaria')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!==false)
        {
            $tGrado=TGrado::whereBetween('idgrado',[7,13])->get();
            $tCurso=TCurso::whereRaw('nivel=?','Inicial')->get();
        }
        
        $tSeccion=TSeccion::all();

        $tCursoxDocente=TCursoxDocente::whereRaw('dni=? and nombreArchivo=?',[session()->get('Person')->dni,'Plan curricular'])->get();

        return view('file/insert',['tCurso'=>$tCurso,'tGrado'=>$tGrado,'tSeccion'=>$tSeccion,'tCursoxDocente'=>$tCursoxDocente]);
    }

    public function actionDelete($idcd=null)
    {
        $tArchivoie=TCursoxdocente::find($idcd);
        
        if($tArchivoie!=null)
        {
            if($tArchivoie->delete())
            {
                $rutaArchivo = public_path().'/filedcte/'.session()->get('Person')->dni.'-'.$tArchivoie->idcd.'.'.$tArchivoie->formato;

                if(File::delete($rutaArchivo))
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/file');
                }
                else
                {
                    return $this->helperdrea->redirectError('Ocurrió un error.', 'insert/file');
                }
            }
        }

        return $this->helperdrea->redirectError('Ocurrió un error.', 'insert/file');

    }

    public function actionInsertSecundaria(Request $request)
    {
        // dd(env('spaceDcte'));
        if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'secundaria')!=false)
        {
           try
           {
                if($request->hasFile('file'))
                {
                    DB::beginTransaction();

                        $tCursoxDocente= new TCursoxdocente();
                        $tCursoxDocente->dni=session()->get('Person')->dni;
                        $tCursoxDocente->nombreArchivo='Plan curricular';
                        $tCursoxDocente->formato=strtolower($request->file('file')->getClientOriginalExtension());
                        $tCursoxDocente->peso=round($request->file('file')->getSize()/1024).'-kb';
                        $tCursoxDocente->nivel=session()->get('Person')->tDocente->tIIEE->nivelm;
                        $tCursoxDocente->periodoAcademico=$request->input('periodo');
                        $tCursoxDocente->comentario=$request->input('txtComentario');
                        $tCursoxDocente->estadoArchivo='0';
        
                        $tCursoxDocente->save();

                            for($j=0; $j <count($request->get('selectSeccions')) ; $j++)
                            {

                                $tDetallecursopordocente= new TDetallecursopordocente();

                                $tDetallecursopordocente->idcd=$tCursoxDocente->idcd;
                                $tDetallecursopordocente->idcurso=($request->get('selectCursos'));
                                $tDetallecursopordocente->idseccion=($request->get('selectSeccions'))[$j];
                                $tDetallecursopordocente->idgrado=($request->get('selectGrados'));
                                $tDetallecursopordocente->save();
                            }

                        $request->file('file')->move(public_path().'/filedcte'.'/',session()->get('Person')->dni.'-'.$tCursoxDocente->idcd.'.'.$tCursoxDocente->formato);
                    
                    DB::commit();
                    
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/file');
                }     
                return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
   
           }

           catch(\exception $ex)
           {
               DB::rollback();
               return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
           }
        }

        else
        {
            return $this->helperdrea->redirectError('Ocurrió un error, por favor vuelva a intentarlo', '/');
        }
    }

    public function actionInsertEducationalMaterial(Request $request)
    {
        if($_POST)
        {
            try
            {
                 DB::beginTransaction();
 
                    $tCursoxDocente= new TCursoxdocente();

                    $tCursoxDocente->dni=session()->get('Person')->dni;
                    $tCursoxDocente->comentario=$request->input('txtComentario');
                    $tCursoxDocente->url=$request->input('txtLink');
                    if($request->hasFile('file'))
                    {
                    $tCursoxDocente->formato=strtolower($request->file('file')->getClientOriginalExtension());
                    $tCursoxDocente->peso=round($request->file('file')->getSize()/1024).'-kb';
                    }
                    $tCursoxDocente->nivel=session()->get('Person')->tDocente->tIIEE->nivelm;
                    $tCursoxDocente->nombreArchivo=$request->input('txtNombre');
                    $tCursoxDocente->periodoAcademico=$request->input('');
                    $tCursoxDocente->estadoArchivo='0';
         
                    $tCursoxDocente->save();

                    if($request->hasFile('file'))
                    {
                        $request->file('file')->move(public_path().'/filedcte'.'/',session()->get('Person')->dni.'-'.$tCursoxDocente->idcd.'.'.$tCursoxDocente->formato);

                    }

                    $tDetallecursopordocente= new TDetallecursopordocente();

                    $tDetallecursopordocente->idcd=$tCursoxDocente->idcd;
                    $tDetallecursopordocente->idcurso=($request->get('selectCurso'));
                    $tDetallecursopordocente->idseccion=($request->get('selectSeccion'));
                    $tDetallecursopordocente->idgrado=($request->get('selectGrado'));

                    $tDetallecursopordocente->save();
                     
                     DB::commit();
                     
                     return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/educationalmaterial');    
            }
 
            catch(\exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
            }
        }
        if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Secundaria')!==false)
        {
            $tGrado=TGrado::all()->take(5);
            $tCurso=TCurso::whereRaw('nivel=?','Secundaria')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'Primaria')!==false)
        {
            $tGrado=TGrado::all()->take(6);
            $tCurso=TCurso::whereRaw('nivel=?','Inicial')->get();
        }
        else if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!==false)
        {
            $tGrado=TGrado::whereBetween('idgrado',[7,13])->get();
            $tCurso=TCurso::whereRaw('nivel=?','Inicial')->get();
        }
        
        $tSeccion=TSeccion::all();

        $tCursoxDocente=TCursoxDocente::whereRaw('dni=? and nombreArchivo!=?',[session()->get('Person')->dni,'Plan curricular'])->get();

        return view('file/educationalmaterial',['tGrado'=>$tGrado,'tSeccion'=>$tSeccion,'tCurso'=>$tCurso,'tCursoxDocente'=>$tCursoxDocente]);
    }

    public function actionDeleteEducationalMaterial($idcd)
    {
        $tArchivo=TCursoxDocente::find($idcd);

        $tArchivo->delete();

        return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/educationalmaterial');    
    }

    public function actionInsertPrimaria(Request $request)
    {
        if($request->hasFile('file'))
        {
            try
            {
                DB::beginTransaction();
                
                $tCursoxDocente=new TCursoxDocente();

                $tCursoxDocente->dni=session()->get('Person')->dni;
                $tCursoxDocente->nombreArchivo='Plan curricular';
                $tCursoxDocente->formato=strtolower($request->file('file')->getClientOriginalExtension());
                $tCursoxDocente->peso=round($request->file('file')->getSize()/1024).'-kb';
                $tCursoxDocente->nivel=session()->get('Person')->tDocente->tIIEE->nivelm;
                $tCursoxDocente->periodoAcademico=$request->input('periodo');
                $tCursoxDocente->estadoArchivo='0';
                $tCursoxDocente->comentario=$request->input('txtComentario');

                $tCursoxDocente->save();

                $request->file('file')->move(public_path().'/filedcte'.'/',session()->get('Person')->dni.'-'.$tCursoxDocente->idcd.'.'.$tCursoxDocente->formato);
                
                for($i=0; $i<count($request->get('selectCursos'));$i++)
                {
                    if($request->input('docentes')=='Unidocente' || $request->input('docentes')=='Unidocente')
                    {
                        for($j=0; $j<count($request->get('selectGrados'));$j++)
                        {
                            $tDetalleCursoporDocente= new TDetalleCursoporDocente();
                    
                            $tDetalleCursoporDocente->idcd=$tCursoxDocente->idcd;
                            $tDetalleCursoporDocente->idCurso=($request->get('selectCursos'))[$i];
                            $tDetalleCursoporDocente->idseccion=8;
                            $tDetalleCursoporDocente->idgrado=($request->get('selectGrados'))[$j];
                            
                            $tDetalleCursoporDocente->save();
                        }
                        
                    }
                   
                    else if($request->input('docentes')=='Polidocente')
                    {
                        $tDetalleCursoporDocente= new TDetalleCursoporDocente();
                    
                        $tDetalleCursoporDocente->idcd=$tCursoxDocente->idcd;
                        $tDetalleCursoporDocente->idCurso=($request->get('selectCursos'))[$i];
                        $tDetalleCursoporDocente->idseccion=$request->input('selectSeccions2');
                        $tDetalleCursoporDocente->idgrado=$request->get('selectGradosPolidocente');

                        $tDetalleCursoporDocente->save();
                    }
                }
                DB::commit();
                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'insert/file');    

            }

            catch(\Exception $ex)
            {
                DB::rollback();
                return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
            }
        }
        return $this->helperdrea->redirectError('Algo salió mal.', 'insert/file');
    }
}