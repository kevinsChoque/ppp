<?php
namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Response;

use Session;
use DB;
use Mail;

use App\Model\TIIEE;
use App\Model\TDocente;
use App\Model\TPersona;

class IeController extends Controller
{
    public function actionUpdateDcte(Request $request,SessionManager $sessionManager)
    {
        return view('ie/updateDcte');
    }
    public function actionGetiee(Request $request,SessionManager $sessionManager)
    {
        $ie = TIIEE::where('codigomodular',$request->cm)->first();
        $director = TDocente::where('ie',$request->cm)
        	->where('director','1')
        	->first();
        if($director!=null)
        {
            $director=$director->tPersona_;
        }
        else
        {
            $director='No especifica';
        }
        $docentes = TPersona::select('persona.*')
            ->join('docente', 'docente.dni', '=', 'persona.dni')
            ->where('docente.ie',$request->cm)
            ->get();

        return response()->json(['data'=>$ie,'ugel'=>$ie->tUgel->ugel_nombre,'director'=>$director,'docentes'=>$docentes]);
    }
    public function actionUpdateDirector(Request $request,SessionManager $sessionManager)
    {
        if($request->director!=null)
        {
            $director = TDocente::where('ie',$request->cm)
                ->where('director','1')
                ->first();
            $director->director=0;

            if($director->save())
            {
                $directorNew = TDocente::where('dni',$request->director)->first();
                $directorNew->director=1;

                if($directorNew->save())
                {
                    // echo($directorNew->tPersona_);
                    return response()->json(['update'=>1,'directorNew'=>$directorNew->tPersona_]);
                }
            }
        }
        return response()->json(['update'=>0]);
    }
    
}
