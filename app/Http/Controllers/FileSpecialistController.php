<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

use Illuminate\Http\Request;
use Illuminate\Session\SessionManager;
use Illuminate\Encryption\Encrypter;
use Illuminate\Support\Facades\Response;
use Illuminate\Support\Facades\File;
 
use Session;
use DB;
use Mail;

use App\Model\TCurso;
use App\Model\TGrado;
use App\Model\TSeccion;
use App\Model\TArchivoie;
use App\Model\TCursoxdocente;
use App\Model\TIIEE;
use App\Model\TArchivospecialist;

class FileSpecialistController extends Controller
{
	protected $documentos = array("pdf", "docx", "xlsx");

    public function actionInsertShare(Request $request,SessionManager $sessionManager)
    {
    	$dni = $request->session()->get('Person')->dni;

    	if($_POST)
    	{
    		$tAe = new TArchivospecialist();
    		$tAe->idespecialista = $dni;
    		$tAe->cie = $request->input('ies');
    		$tAe->nombre = $request->input('nombre');
    		$tAe->nombrereal = uniqid();
    		$tAe->comentario = $request->input('comentario');
    		$tAe->video = $request->input('video');
    		$tAe->createdby = $dni;
    		$tAe->createddate = date('Y-m-d H:m:s');
    		$tAe->status = $request->input('publico')!=''?'1':'0';
            $tAe->otherspecialist = $request->input('compartir')!=''?'1':'0';
            
    		// echo $file = $request->file('file');exit();
    		if($tAe->save())
    		{
    			$file = $request->file('file');
    			if($file!='')
    			{
    				$formato = explode('.', $file->getClientOriginalName())[1];
    				$peso = round(filesize($file)/1024).'-kb';
    				if(in_array($formato, $this->documentos))
    				{
    					$tAe = TArchivospecialist::where('nombrereal','=',$tAe->nombrereal)->first();
		    			$ruta = public_path().'/fileSpecialist';
		    			$fileName = $tAe->nombrereal.'-'.$tAe->idarchivospecialist.'.'.$formato;
		    			$file->move($ruta,$fileName);

		    			$tAe->nombrereal = $tAe->nombrereal.'-'.$tAe->idarchivospecialist;
		    			$tAe->formato = $formato;
	    				$tAe->peso = $peso;
	    				//if($tArchivoie->save())//usar para probar la transaccion
		    			if($tAe->save())
		    			{
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertShare');
		    			}
		    			else
		    			{
		    				$sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
		    			}
    				}
    				else
    				{
    					$sessionManager->flash('estado','no tiene el formato correcto');
    				}
    			}
    			else
    			{
                    return $this->helperdrea->redirectCorrect('Operación realizada exitosamente, pero no subio ningun archivo.', 'fileSpecialist/insertShare');
    			}
    		}
    		else
    		{
    			$sessionManager->flash('no se pudo guardar el registro');
    		}
    		// echo $request->input('publico')==''?'si':'no';exit();
    	}
    	$listTae = TArchivospecialist::where('idespecialista',$dni)->where('cie','!=','')->get();
    	// echo $listTae;exit();
    	return view('fileSpecialist/insertShare',['listTae'=>$listTae]);
    }
    public function actionGetIe(Request $request,SessionManager $sessionManager)
    {
    	if($request->ajax())
    	{
            $nivel = $request->nivel;
    		// $filtro = ' and nivelm like "%'.$nivel.'%"';
    		$idugel = $request->session()->get('Person')->tEspecialista->ugelid;
            // $ie = $this->modelsManager->executeQuery('select lpad(codigomodular,7,0) as codigomodular, ie_nombre from Iiee where ugelid='.$ugel.$filtro0);
            $listIe = TIIEE::select('codigomodular','ie_nombre')
            	->where('ugelid',$idugel)
            	->where('nivelm','LIKE','%'.$nivel.'%')
            	->get();

            return response::json($listIe);
        }
        else{
            // return $this->response->redirect('index');
        }
    }
    public function actionGetIeGeneralUgel(Request $request,SessionManager $sessionManager)
    {
        if($request->ajax())
        {

            $nivel = $request->nivel;
            $idugel = $request->idugel;
            
            $listIe = TIIEE::select('codigomodular','ie_nombre')
                ->where('ugelid',$idugel)
                ->where('nivelm','LIKE','%'.$nivel.'%')
                ->get();

            return response::json($listIe);
        }
    }
    public function actionDelete(Request $request,SessionManager $sessionManager,$idarchivospecialist=null)
    {
        $tAe=Tarchivospecialist::find($idarchivospecialist);
        $ruta = $tAe->cie==null?'insertMaterial':'insertShare';

        if($tAe!=null)
        {
            if($tAe->delete())
            {
                $rutaArchivo = public_path().'/fileSpecialist/'.$tAe->nombrereal.'.'.$tAe->formato;

                if(File::delete($rutaArchivo))
                {
                    // $sessionManager->flash('estado','se elimino exitosamente');
                    return $this->helperdrea->redirectCorrect('Se elimino exitosamente.', 'fileSpecialist/'.$ruta);
                }
                else
                {
                    // File::move($rutaArchivo,public_path().'/fileie/'.'borrar.'.$tArchivoie->formato);
                    // $sessionManager->flash('estado','se elimino el registro pero el archivo no se pudo eliminar.');en realidad seria este el mensaje
                    return $this->helperdrea->redirectCorrect('Se elimino exitosamente.', 'fileSpecialist/'.$ruta);
                    // $sessionManager->flash('estado','se elimino exitosamente');
                }
            }
        }
        return redirect('fileSpecialist/insertShare');
    }
    public function actionEdit(Request $request,SessionManager $sessionManager)
    {
        $tAe=TArchivospecialist::find($request->idarchivospecialist);
        if($_POST)
        {
        	$tAe=TArchivospecialist::find($request->idarchivospecialist);
            $tAe->nombre = $request->nombre;
            $tAe->comentario = $request->comentario;
            $tAe->video = $request->video;
            $tAe->status = $request->input('publico')!=''?'1':'0';
            $tAe->otherspecialist = $request->input('compartir')!=''?'1':'0';
            
            if($request->ies!='') $tAe->cie = $request->ies;

            $file = $request->file('file');
            if($file!='')
            {
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';
                if(strpbrk($tAe->nombrereal, '-')!='')
                {
                    $rutaArchivo = public_path().'/fileSpecialist/'.$tAe->nombrereal.'.'.$tAe->formato;
                    if(File::delete($rutaArchivo))
                    {
     //                	echo 'lo elimino';
	    //         	echo $rutaArchivo;
					// exit();
                        if(in_array($formato, $this->documentos))
                        {
                            $ruta = public_path().'/fileSpecialist';
                            $fileName = $tAe->nombrereal.'.'.$tAe->formato;
                            $file->move($ruta,$fileName);

                            $tAe->formato = $formato;
                            $tAe->peso = $peso;

                            if($tAe->save())
                            {
                                // $sessionManager->flash('estado','se guardo exitosamente');
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertShare');
                            }
                            else
                            {
                                $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                            }
                        }
                        else
                        {
                            $sessionManager->flash('estado','no tiene el formato correcto');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no se pudo eliminar el archivo');
                    }
                }
                else
                {
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/fileSpecialist';
                        $fileName = $tAe->nombrereal.'-'.$tAe->idarchivoie.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tAe->nombrereal = $tAe->nombrereal.'-'.$tAe->idarchivoie;
                        $tAe->formato = $formato;
                        $tAe->peso = $peso;

                        if($tAe->save())
                        {
                            // $sessionManager->flash('estado','se guardo exitosamente');
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertShare');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
            }
            else
            {
                if($tAe->save())
                {
                    // $sessionManager->flash('estado','se guardo exitosamente');
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertShare');
                }
            }
            return redirect('fileIe/insertShare');
        }
        return response()->json(['data'=>$tAe]);
    }
    public function actionInsertMaterial(Request $request,SessionManager $sessionManager)//= que insertShare, solo que no guarda ala ie
    {
        $dni = $request->session()->get('Person')->dni;

        if($_POST)
        {
            // dd($request->all());
            $tAe = new TArchivospecialist();
            $tAe->idespecialista = $dni;
            $tAe->nombre = $request->input('nombre');
            $tAe->nombrereal = uniqid();
            $tAe->comentario = $request->input('comentario');
            $tAe->video = $request->input('video');
            $tAe->cnivel = $request->input('nivel');
            $tAe->createdby = $dni;
            $tAe->createddate = date('Y-m-d H:m:s');
            $tAe->status = $request->input('publico')!=''?'1':'0';
            $tAe->otherspecialist = $request->input('compartir')!=''?'1':'0';

            $tAe->semana = $request->input('semana');
            $tAe->contenido = $request->input('contenido');
            $tAe->grado = $request->input('grado');
            $tAe->curso = $request->input('curso');
            $tAe->ugel = $request->input('ugel');
            
            if($tAe->save())
            {
                $file = $request->file('file');
                if($file!='')
                {
                    // echo('hay archivo');
                    $formato = explode('.', $file->getClientOriginalName())[1];
                    $peso = round(filesize($file)/1024).'-kb';
                    if(in_array($formato, $this->documentos))
                    {
                        // echo('--cumple con el formato');
                        $tAe = TArchivospecialist::where('nombrereal','=',$tAe->nombrereal)->first();
                        $ruta = public_path().'/fileSpecialist';
                        $fileName = $tAe->nombrereal.'-'.$tAe->idarchivospecialist.'.'.$formato;
                        $file->move($ruta,$fileName);
                        // echo $file->move($ruta,$fileName);
                        // echo('--');
                        // exit();

                        $tAe->nombrereal = $tAe->nombrereal.'-'.$tAe->idarchivospecialist;
                        $tAe->formato = $formato;
                        $tAe->peso = $peso;
                        //if($tArchivoie->save())//usar para probar la transaccion
                        // echo 'llego hasta aki';exit();
                        if($tAe->save())
                        {
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertMaterial');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
                else
                {
                    return $this->helperdrea->redirectCorrect('Operación realizada exitosamente, pero no subio ningun archivo.', 'fileSpecialist/insertMaterial');
                }
            }
            else
            {
                $sessionManager->flash('no se pudo guardar el registro');
            }
            // echo $request->input('publico')==''?'si':'no';exit();
        }
        $listTae = TArchivospecialist::where('idespecialista',$dni)->where('cie',null)->orderBy('semana','desc')->get();
        // echo $listTae;exit();
        return view('fileSpecialist/insertMaterial',['listTae'=>$listTae]);
    }
    public function actionEditMaterial(Request $request,SessionManager $sessionManager)
    {
        $tAe=TArchivospecialist::find($request->idarchivospecialist);
        if($_POST)
        {
            $tAe=TArchivospecialist::find($request->idarchivospecialist);
            $tAe->nombre = $request->nombre;
            $tAe->comentario = $request->comentario;
            $tAe->video = $request->video;
            $tAe->status = $request->input('publico')!=''?'1':'0';
            $tAe->otherspecialist = $request->input('compartir')!=''?'1':'0';
            
            if($request->ies!='') $tAe->cie = $request->ies;

            $file = $request->file('file');
            if($file!='')
            {
                $formato = explode('.', $file->getClientOriginalName())[1];
                $peso = round(filesize($file)/1024).'-kb';
                if(strpbrk($tAe->nombrereal, '-')!='')
                {
                    $rutaArchivo = public_path().'/fileSpecialist/'.$tAe->nombrereal.'.'.$tAe->formato;
                    if(File::delete($rutaArchivo))
                    {
                        if(in_array($formato, $this->documentos))
                        {
                            $ruta = public_path().'/fileSpecialist';
                            $fileName = $tAe->nombrereal.'.'.$tAe->formato;
                            $file->move($ruta,$fileName);

                            $tAe->formato = $formato;
                            $tAe->peso = $peso;

                            if($tAe->save())
                            {
                                // $sessionManager->flash('estado','se guardo exitosamente');
                                return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertMaterial');
                            }
                            else
                            {
                                $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                            }
                        }
                        else
                        {
                            $sessionManager->flash('estado','no tiene el formato correcto');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no se pudo eliminar el archivo');
                    }
                }
                else
                {
                    if(in_array($formato, $this->documentos))
                    {
                        $ruta = public_path().'/fileSpecialist';
                        $fileName = $tAe->nombrereal.'-'.$tAe->idarchivoie.'.'.$formato;
                        $file->move($ruta,$fileName);

                        $tAe->nombrereal = $tAe->nombrereal.'-'.$tAe->idarchivoie;
                        $tAe->formato = $formato;
                        $tAe->peso = $peso;

                        if($tAe->save())
                        {
                            // $sessionManager->flash('estado','se guardo exitosamente');
                            return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertMaterial');
                        }
                        else
                        {
                            $sessionManager->flash('estado','hubo problemas con el archivo al momento de guardar, contactese con el administrador');
                        }
                    }
                    else
                    {
                        $sessionManager->flash('estado','no tiene el formato correcto');
                    }
                }
            }
            else
            {
                if($tAe->save())
                {
                    // $sessionManager->flash('estado','se guardo exitosamente');
                    return $this->helperdrea->redirectCorrect('Operación realizada correctamente.', 'fileSpecialist/insertMaterial');
                }
            }
            return redirect('fileIe/insertMaterial');
        }
        return response()->json(['data'=>$tAe]);
    }
}
