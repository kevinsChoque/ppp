<?php
namespace App\Http\Middleware;

use Closure;
use Session;


class MDGeneric
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
      if( Session::has('Person'))
        {           
            $response = $next($request);
            return $response;
            
        }
        //Session::flash('msj-error', 'Inicie sessión');
        return redirect('index');
    }
}