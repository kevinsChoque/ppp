<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>
<script src="{{asset('js/export/dataTables.buttons.min.js')}}"></script>
<script src="{{asset('js/export/buttons.flash.min.js')}}"></script>
<script src="{{asset('js/export/jszip.min.js')}}"></script>

<script src="{{asset('js/export/pdfmake.min.js')}}"></script>

<script src="{{asset('js/export/vfs_fonts.js')}}"></script>
<script src="{{asset('js/export/buttons.html5.min.js')}}"></script>
<script src="{{asset('js/export/buttons.print.min.js')}}"></script>

<style>
	.dt-buttons{
		float: left !important;
	}
</style>