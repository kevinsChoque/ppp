@extends('recursosAprendizaje/template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de material</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('recursosAprendizaje/registrar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                <!-- justify-content-center align-items-center /conjuntamente con row-->
                <div class="row">
                    <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Tioo de material:</label>
                            <select name="tipoMaterial" id="tipoMaterial" class="form-control form-control-sm tipoMaterial">
                                <option disabled="" selected="">Elija el tipo:</option>
                                <option value="expApr">Experiencias de aprendizaje</option>
                                <option value="ficAut">Fichas de autoaprendizaje</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
	                        <label class="m-0">Ugel:</label>
	                        <select name="ugel" id="ugel" class="form-control form-control-sm">
	                            <option disabled="" selected="">Elija la ugel:</option>
	                            <option value="UGEL Abancay">UGEL Abancay</option>
	                            <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
	                            <option value="UGEL Antabamba">UGEL Antabamba</option>
	                            <option value="UGEL Aymaraes">UGEL Aymaraes</option>
	                            <option value="UGEL Cotabambas">UGEL Cotabambas</option>
	                            <option value="UGEL Chincheros">UGEL Chincheros</option>
	                            <option value="UGEL Grau">UGEL Grau</option>
	                            <option value="UGEL Huancarama">UGEL Huancarama</option>
	                        </select>
	                    </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Mes:</label>
                            <select name="mes" id="mes" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija el mes:</option>
                                <option value="Enero">Enero</option>
                                <option value="Febrero">Febrero</option>
                                <option value="Marzo">Marzo</option>
                                <option value="Abril">Abril</option>
                                <option value="Mayo">Mayo</option>
                                <option value="Junio">Junio</option>
                                <option value="Julio">Julio</option>
                                <option value="Agosto">Agosto</option>
                                <option value="Setimbre">Setimbre</option>
                                <option value="Octubre">Octubre</option>
                                <option value="Noviembre">Noviembre</option>
                                <option value="Diciembre">Diciembre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <select name="nivel" id="nivel" class="form-control form-control-sm nivel">
                                <option disabled="" selected="">Elija el nivel:</option>
                                <option value="Inicial">Inicial</option>
                                <option value="Primaria">Primaria</option>
                                <option value="Secundaria">Secundaria</option>
                            </select>
                        </div>
                    </div>
                    <!-- <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Grado/edad:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija el grado/edad:</option>
                                <option value="Inicial">Inicial</option>
                                <option value="Primaria">Primaria</option>
                                <option value="Secundaria">Secundaria</option>
                            </select>
                        </div>
                    </div> -->
                    <div class="col-md-4 gradoIni" style="display: none;">
                        <div class="form-group mb-2">
                            <label class="m-0">Edad:</label>
                            <input type="text" name="grado" id="grado" class="form-control form-control-sm" value="5 años" disabled>
                        </div>
                    </div>
                    <div class="col-md-4 gradoPs">
                        <div class="form-group mb-2">
                            <label class="m-0">Grado:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Grado:</option>
                                <option value="1">1° Grado</option>
                                <option value="2">2° Grado</option>
                                <option value="3">3° Grado</option>
                                <option value="4">4° Grado</option>
                                <option value="5">5° Grado</option>
                                <option value="6">6° Grado</option>
                            </select>
                        </div>
                    </div>

                    <!-- <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="m-0">Edad:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Edad:</option>
                                <option value="0">0 Años</option>
                                <option value="1">1 Años</option>
                                <option value="2">2 Años</option>
                                <option value="3">3 Años</option>
                                <option value="4">4 Años</option>
                                <option value="5">5 Años</option>
                            </select>
                        </div>
                    </div>
                     -->
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Curso:</label>
                            <select name="curso" id="curso" class="form-control form-control-sm curso">
                                <option disabled="" selected="">Elija el curso:</option>
                                <option value="ciencias sociales">Ciencias Sociales</option>
                                <option value="comunicacion">Comunicación</option>
                                <option value="matematica">Matemática</option>
                                <option value="ciencia y tecnologia">Ciencia y Tecnología</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="m-0">Descripcion:</label>
                            <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Subir archivo:</label>
                            <input class="form-control form-control-sm" type="file" name="archivo" id="archivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Enlace de archivo:</label>
                            <input class="form-control form-control-sm" type="text" name="earchivo" id="earchivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="evideo" id="evideo" class="form-control form-control-sm" placeholder="Video">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de audio:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="eaudio" id="eaudio" class="form-control form-control-sm" placeholder="Audio">
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
            <!-- <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar"> -->
        </div>
    </div>
</div>
<script>
    $('.nivel').on('change',function(){
        if($(this).val()=='Inicial')
        {
            $('.gradoIni').css('display','block');
            $('.gradoPs').css('display','none');
        }
        else
        {
            $('.gradoIni').css('display','none');
            $('.gradoPs').css('display','block');
        }
    });
    $('.tipoMaterial').on('change',function(){
        if($(this).val()=='expApr')
        {
            $('.curso').prop('disabled',true);
        }
        else
        {
            $('.curso').prop('disabled',false);
        }
    });
</script>
@endsection