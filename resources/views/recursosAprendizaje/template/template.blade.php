<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{csrf_token()}}" />
  <title>DREA-REPOSITORIO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}"> -->
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/summernote/summernote-bs4.css')}}">

  <script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>
  
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
  <link rel="stylesheet" href="{{asset('plugin/pnotify/pnotify.custom.min.css')}}">
  <!-- <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/select2/css/select2.min.css')}}"> -->
  <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/styleGeneral.css')}}">
  <!-- jQuery -->
  <script src="{{asset('plugin/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

	<script src="{{asset('plugin/pnotify/pnotify.custom.min.js')}}"></script>

    <!-- prueba diana  form validation -->
    <!--<link rel="stylesheet" href="{{asset('plugin/formvalidation//bootstrap.min.css')}}">
    <script src="{{asset('plugin/formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('plugin/formvalidation/bootstrap.validation.min.js')}}"></script>
    <script src="{{asset('plugin/formvalidation/bootstrap.min.js')}}"></script>-->

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
  <script src="{{asset('js/popper.min.js')}}"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

  <script src="{{asset('js/datatables.min.js')}}"></script>

  <script src="{{asset('js/helperdrea.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/additional-methods.min.js')}}"></script>

</head>
<body class="hold-transition sidebar-mini layout-fixed">
    <style>
    .select2-selection.select2-selection--single,.select2-selection.select2-selection--multiple{border: 1px solid #ced4da !important;}
    .notificaciones1{
        background: rgba(255, 193, 7,.5);
        border-radius: 70%;
    }
    .notificaciones2{
        background: rgba(23, 162, 184,.2);
        border-radius: 70%;
    }
    .controlarParrafo{white-space: nowrap !important;}
    .controlarP{overflow: hidden !important;}
    </style>
<div class="loader"></div>
<div class="wrapper">

  <!-- Navbar -->
  <nav class="main-header navbar navbar-expand navbar-white navbar-light p-0">
    <!-- Left navbar links -->
    <ul class="navbar-nav">
        <li class="nav-item">
            <a class="nav-link" data-widget="pushmenu" href="#"><i class="fas fa-bars"></i></a>
        </li>
        <!-- <li class="nav-item d-none d-sm-inline-block">
            <a href="index3.html" class="nav-link">Inicio</a>
        </li> -->
        <li class="nav-item d-none d-sm-inline-block">
            <a href="{{url('user/logout')}}" class="nav-link">Cerrar sesión</a>
        </li>
    </ul>

    <!-- SEARCH FORM -->
    <!-- <form class="form-inline ml-3">
      <div class="input-group input-group-sm">
        <input class="form-control form-control-navbar" type="search" placeholder="Search" aria-label="Search">
        <div class="input-group-append">
          <button class="btn btn-navbar" type="submit">
            <i class="fas fa-search"></i>
          </button>
        </div>
      </div>
    </form> -->

    <!-- Right navbar links -->
    <ul class="navbar-nav ml-auto">
        <li class="nav-item dropdown">
            <a class="nav-link" data-toggle="dropdown" href="#">
                <i class="fa fa-university"></i>
                GESTOR DE MATERIALES
                <strong>|</strong>
                <i class="fa fa-user-circle"></i>
                {{(session()->get('Person')->nombres)}} {{(session()->get('Person')->apaterno)}}
            </a>
        </li>
        <!-- Notifications Dropdown Menu -->
        <li class="nav-item dropdown">
            <a class="nav-link notificaciones1 mr-1" data-toggle="dropdown" href="#">
                <i class="fa fa-user"></i>
                <!-- <span class="badge badge-warning navbar-badge">15</span> -->
            </a>
            <div class="dropdown-menu dropdown-menu-lg dropdown-menu-right" style="min-width: 200px !important;">
                <!-- <span class="dropdown-item dropdown-header">15 Notifications</span> -->
                <!-- <div class="dropdown-divider"></div>
                    <a href="#" class="dropdown-item"><i class="fas fa-envelope mr-2"></i> 4 new messages
                        <span class="float-right text-muted text-sm">3 mins</span>
                    </a>
                <div class="dropdown-divider"></div> -->
                
                
                <a href="" class="dropdown-item" target="_blank">
                    <i class="fa fa-download mr-2"></i> Descargar manual.
                    <!-- <span class="float-right text-muted text-sm">12 hours</span> -->
                </a>

                <div class="dropdown-divider"></div>
                <a href="{{url('user/logout')}}" class="dropdown-item">
                    <i class="fa fa-power-off mr-2"></i> Cerrar sesión.
                    <!-- <span class="float-right text-muted text-sm">2 days</span> -->
                </a>
                <!-- <div class="dropdown-divider"></div> -->
                <!-- <a href="#" class="dropdown-item dropdown-footer">See All Notifications</a> -->
            </div>
        </li>
        <!-- Messages Dropdown Menu -->
        <!-- <li class="nav-item dropdown">
            <a class="nav-link notificaciones2" data-toggle="dropdown" href="#">
                <i class="far fa-bell"></i>
                <span class="badge badge-danger navbar-badge getCant" style="font-size: 0.8rem !important;"></span>
            </a>
            
        </li> -->
        
      <!-- <li class="nav-item">
        <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#">
          <i class="fas fa-th-large"></i>
        </a>
      </li> -->
    </ul>
  </nav>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="{{url('/')}}" class="brand-link">
      <img src="{{asset('imgSocio/logo-blanco-sombra.png')}}" alt="AdminLTE Logo" class="brand-image  elevation-3"
           style="opacity: .8;margin-left: .1rem !important;">
        <span class="brand-text font-weight-light" style="font-size: 0.9rem;">
            RECURSOS DE APRENDIZAJE
        </span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user panel (optional) -->
      <div class="user-panel mt-2 pb-2 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('plugin/adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
            <!-- <a href="#" class="d-block">
                {{(session()->get('Person')->nombres)}} {{(session()->get('Person')->apaterno)}}
            </a> -->
            <i class="text-white">DRE Apurimac
                | {{(session()->get('Person')->nombres)}}
              </i>
        </div>
      </div>

      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column nav-compact" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
               @include('recursosAprendizaje/template/nav/navmateriales')
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>
    @if(Session::has('globalMessage'))
    <script>
		$(function()
		{
			@if(Session::get('type')=='error')
                @foreach(explode('__BREAKLINE__', Session::get('globalMessage')) as $value)
					@if(trim($value)!='')
					   new PNotify(
						{
							title : 'No se pudo proceder',
							text : '{{$value}}',
							type : '{{Session::get('type')}}'
						});
					@endif
				@endforeach
			@else
    			swal(
    			{
    				title : '{{Session::get('type')=='success' ? 'Correcto' : 'Alerta'}}',
    				text : '{!!Session::get('globalMessage')!!}',
    				icon : '{{Session::get('type')=='success' ? 'success' : 'warning'}}',
    				timer: {{Session::get('type')=='success' ? '2000' : '60000'}}
    			});
    		@endif
		});
    </script>
	@endif
 <!-- @include('sweet::alert') -->
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
        <!-- Content Header (Page header) -->
    <!-- <section class="content-header">
      <div class="container-fluid"></div>
    </section> -->
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Main row -->
        <div class="row">
          <!-- /.Left col -->
          <!-- right col (We are only adding the ID to make the widgets sortable)-->
          <section class="col-lg-12 connectedSortable">
           @yield('generalBody')
          </section>
          <!-- right col -->
        </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <footer class="main-footer py-1">
    <strong>Copyright &copy; 2020 <a >DREA</a>.</strong>
    All rights reserved.
    <div class="float-right d-none d-sm-inline-block">
      <b>Version</b> 2.1.0
    </div>
  </footer>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->


<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugin/adminlte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugin/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('plugin/adminlte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugin/adminlte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->

<!--<script src="{{asset('plugin/adminlte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('plugin/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>-->
<!-- Select2 -->
<!-- <script src="{{asset('plugin/adminlte/plugins/select2/js/select2.full.min.js')}}"></script> -->
<script src="{{asset('/js/select2.min.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugin/adminlte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('plugin/adminlte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugin/adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugin/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('plugin/adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('plugin/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('plugin/adminlte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('plugin/adminlte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('plugin/adminlte/dist/js/demo.js')}}"></script>
<script>
 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();  
  $('.pagination').addClass('mb-1'); 
});

    //Initialize Select2 Elements
    // $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })


/*$('.select').select2(
{
	language:
	{
		noResults: function()
		{
			return "No se encontraron resultados.";        
		},
		searching: function()
		{
			return "Buscando...";
		},
		inputTooShort: function()
		{ 
			return 'Por favor ingrese 3 o más caracteres';
		}
	},
	placeholder: 'Buscar...'
});*/

</script>
</body>
</html>
