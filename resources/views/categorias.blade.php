@extends('template/templatePortal')
@section('generalBodyPortal')
<div class="p-1" style="background-image: url({{asset('imgSocio/fondo.png')}}); background-repeat: round;">
	<div class="container-fluid m-0 p-0" >
		<div class="row justify-content-center align-items-center m-0">
			<!-- <div class="col-lg-5 text-center">
				<p class="text-center font-weight-bold" style="font-size: 2rem;">APOYO SOCIOEMOCIONAL</p>
				<img src="{{asset('imgSocio/ninos.png')}}" class="w-50">
		 	</div> -->
		 	<div class="col-lg-12 text-center font-weight-bold">
				<p style="font-size: 1.2rem; text-shadow: 2px 2px 2px #e2c4c4;">Los equipos de convivencia de la DREA y UGEL de la region de apurimac desde el año 2020 ante la situacion de emergencia sanitaria mundial, ofrece recursos tecnologicos de contenido socioemocional, para atender a estudiantes, docentes y padres de familia integrantes de la comunidad educativa; los recursos contemplan el contexto culturaly linguistico de la dinamica educativa local considerando la siguiente categorizacion:</p>
		 	</div>
		</div>
	</div>
	<div class="container-fluid p-2" >
		<div class="row" style="    margin: 0 5rem;">
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-persona.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
				    	<!-- <p class="card-text">Autoestima, autoconocimiento, autoconcepto, autocuidado, proyecto de vida.</p> -->
				    	<h5>LA PERSONA</h5>
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'1categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-trabajo.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
				    	<!-- <p class="card-text">Equipo, confianza, empatìa, fortaleza.</p> -->
				    	<h5 style="font-size: 1.3rem;">TRABAJO EN EQUIPO</h5>
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'2categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-emociones.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
				    	<!-- <p class="card-text">Gestiòn del conocimiento, gestiòn del recuerdo, inteligencia emocional, pensamiento positivo, proactividad, relajaciòn, concentraciòn.</p> -->
				    	<h5>LAS EMOCIONES</h5>
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'3categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
			<!-- <div class="col-lg-12"><h1></h1></div> -->
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-alertas.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
				    	<!-- <p class="card-text">Signos de alerta, duelo, miedo, estrès, canalizar la agresividad, control de impulsos.</p> -->
				    	<h5>ALERTAS</h5>
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'4categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-familia.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
				    	<!-- <p class="card-text">Convivencia familiar, rutinas, violencia, hogar escuela.</p> -->
				    	<h5>LA FAMILIA</h5>
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'5categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
			<div class="col-lg-2 p-1">
				<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
				  	<img class="card-img-top" src="{{asset('imgSocio/cat-covid.png')}}" alt="Card image cap">
				  	<div class="card-body text-center p-1">
			  			<h5>COVID</h5>
				    	<!-- <p class="card-text">Cuidados ante el COVID, noticias COVID.</p> -->
				    	<a href="{{url('/portal/socioEmocional/material').'/'.'6categoria'}}" class="btn btn-primary font-weight-bold">Ingresa aquì</a>
				  	</div>
				</div>
			</div>
		</div>
    </div>
</div>
<script>
	$('.titulo-banner').html('APOYO SOCIOEMOCIONAL');
	$('body').css('background-image','url({{asset('imgSocio/fondo.jpg')}})');
	$('body').css('background-repeat','round');
</script>
@endsection