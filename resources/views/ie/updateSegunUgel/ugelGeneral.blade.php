<style>
    .selectedRow{background: rgba(1, 1, 1, 0.22);}
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<meta name="_token" content="{{csrf_token()}}" />
@if(session()->has('estado'))
    <div class="alert alert-info mt-3 text-center font-weight-bold p-1">
    {{session()->get('estado')}}
    </div>
@endif
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Buscar institucion educativa</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 py-2">
            <div class="row">
                <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                <div class="col-md-6">
                    <input type="hidden" id="typeNivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                    @if(session()->get('Person')->tEspecialista->nivel=='General')
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label class="m-0">Ugel:</label>
                                <select name="ugel" id="ugel" class="form-control form-control-sm">
                                    <option disabled selected>Elija la ugel:</option>
                                    <option value="30001">UGEL Abancay</option>
                                    <option value="30002">UGEL Andahuaylas</option>
                                    <option value="30003">UGEL Antabamba</option>
                                    <option value="30004">UGEL Aymaraes</option>
                                    <option value="30005">UGEL Cotabambas</option>
                                    <option value="30006">UGEL Chincheros</option>
                                    <option value="30007">UGEL Grau</option>
                                    <option value="30008">UGEL Huancarama</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group mb-2">
                                <label class="m-0">Nivel:</label>
                                <select name="nivel" id="nivel" class="form-control form-control-sm">
                                    <option disabled selected>Elija el nivel:</option>
                                    <option value="inicial">Inicial</option>
                                    <option value="primaria">Primaria</option>
                                    <option value="secundaria">Secundaria</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    @else
                    <div class="form-group mb-2">
                        <label class="m-0">Nivel:</label>
                        <input type="text" id="nivel" name="nivel" value="{{session()->get('Person')->tEspecialista->nivel}}" class="form-control form-control-sm" disabled>
                    </div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group mb-2">
                        <label class="m-0">I.E.:</label>
                        <select name="ies" id="ies" class="form-control form-control-sm select2">
                            <option disabled selected>Primero seleccione nivel:</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group mb-2">
                        <label class="m-0" style="visibility: hidden;"></label>
                        <input type="button" value="Cargar I.E." class="btn btn-success btn-sm w-100 loadIe">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid my-3 msj">
    <div class="row">
        <div class="alert alert-info w-100 text-center m-0 font-weight-bold">Elija una institucion educativa.</div>
    </div>
</div>
<div class="container-fluid my-3 dataIe" style="display: none;">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Institucion educativa</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 py-2">
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Código modular:</label>
                        <input type="text" id="cm" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Código de local:</label>
                        <input type="text" id="cLocal" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Nombre I. E.:</label>
                        <input type="text" id="nombre" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Nivel - Modalidad:</label>
                        <input type="text" id="nivelm" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Forma:</label>
                        <input type="text" id="forma" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Tipo de gestión:</label>
                        <input type="text" id="tGestion" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Dependencia:</label>
                        <input type="text" id="dependencia" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Dirección:</label>
                        <input type="text" id="direccion" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Centro poblado:</label>
                        <input type="text" id="centroPoblado" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Localidad:</label>
                        <input type="text" id="localidad" class="form-control form-control-sm" readonly placeholder="--">
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Intervención:</label>
                        <input type="text" id="intervencion" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Ugel:</label>
                        <input type="text" id="ugelPertenece" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <!-- <div class="col-md-3">
                    <div class="form-group mb-2">
                        <label class="m-0">Estado:</label>
                        <input type="text" id="estado" class="form-control form-control-sm" readonly>
                    </div>
                </div> -->
                <div class="col-md-5">
                    <div class="form-group mb-2">
                        <label class="m-0">Director:</label>
                        <input type="text" id="director" class="form-control form-control-sm" readonly>
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group mb-2">
                        <label class="m-0">Actualizar Director A:</label>
                        <select name="docentes" id="docentes" class="form-control form-control-sm select2">
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group mb-2">
                        <label class="m-0" style="visibility: hidden;"></label>
                        <input type="button" value="Guardar" class="btn btn-success btn-sm w-100 btnUpdateDcte">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    var ppp = $('#typeNivel').val();
    if(ppp!='general')
        cargaie(ppp);
    console.log(ppp);
    $('#nivel,#ugel').on('change',function(ev){
        var nivel=$('#nivel').val();
        var ugel=$('#ugel').val();
        if(nivel!==null && ugel!==null)
        {
            console.log('cargaie(nivel);');
            cargaie(nivel);
        }
            
    });
    $( document ).ready(function() 
    { 
        $('#ies').select2({
            placeholder: "Seleccione un curso",
        });
        $('#docentes').select2({
            placeholder: "Seleccione un curso",
        });
    });

    $('.loadIe').on('click',function(){
        // alert($('#ies').val());
        console.log('aqui ta');
        if($('#ies').val()===null)
        {
            console.log('no mandara ajax');
        }
        else
        {
            console.log('mandara ajax aaaa');
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax(
            { 
                url: "{{ url('/ie/getiee') }}",
                data: {cm:$('#ies').val()},
                method: 'get',
                success: function(result){
                    $('.msj').css('display','none');
                    $('.dataIe').css('display','block');
                    $('#cm').val(result.data.codigomodular);
                    $('#cLocal').val(result.data.codigolocal);
                    $('#nombre').val(result.data.ie_nombre);
                    $('#nivelm').val(result.data.nivelm);
                    $('#forma').val(result.data.forma);
                    $('#tGestion').val(result.data.tgestion);
                    $('#dependencia').val(result.data.gdependencia);
                    $('#direccion').val(result.data.direccion);
                    $('#centroPoblado').val(result.data.centropoblado);
                    $('#localidad').val(result.data.localidad);
                    $('#intervencion').val(result.data.intervencion);
                    $('#ugelPertenece').val(result.ugel);
                    // $('#estado').val(result.data.estado);
                    if(result.director=='No especifica')
                    {
                        directorIe=result.director;
                    }
                    else
                    {
                        directorIe=result.director.dni+' - '+result.director.nombres+' '+result.director.apaterno+' '+result.director.amaterno;
                    }
                    $('#director').val(directorIe);
                    $("#docentes").empty();
                    $("#docentes").append("<option disabled selected>Seleccione nuevo director.</option>");
                    $.each( result.docentes, function(key, value){
                        $("#docentes").append("<option value='" + value.dni + "'>"+value.dni+" - "+value.nombres+" "+value.apaterno +" "+value.amaterno+ "</option>");
                    });
                }
            });
        }
        
    });
    $('.btnUpdateDcte').on('click',function(){
        // alert($('#docentes').val());
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/ie/updateDirector') }}",
            data: {cm:$('#ies').val(),director:$('#docentes').val()},
            method: 'get',
            success: function(result){
                if(result.update==1)
                {
                    directorIe=result.directorNew.dni+' - '+result.directorNew.nombres+' '+result.directorNew.apaterno+' '+result.directorNew.amaterno;

                    $('#director').val(directorIe);
                    new PNotify(
                        {
                            title : 'Operacion exitosa',
                            text : 'Se actualizo el director.',
                            type : 'success'
                        });
                }
                else
                {
                    new PNotify(
                    {
                        title : 'No se pudo proceder',
                        text : 'Se requiere que elija al docente.',
                        type : 'error'
                    });
                }
            }
        });
    });
    function cargaie(nivel)
    {
        console.log('entro a la funcion para cargar las ie');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getIeGeneralUgel') }}",
            data: {nivel:nivel,idugel:$('#ugel').val()},
            method: 'get',
            success: function(result){
                // $('.select2-selection.select2-selection--single').addClass('pt-1');
                $("#ies").empty();
                $("#ies").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    var valueIe = value.codigomodular+"//"+value.ie_nombre+"//"+$('#nivel').val();
                    $("#ies").append("<option value='" + valueIe + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }
</script>