@extends('template/template')
@section('generalBody')
@if(session()->get('Person')->tEspecialista->ugelid=='30000')
    @include('ie/updateSegunUgel/ugelGeneral')
@else
    @include('ie/updateSegunUgel/ugelParticular')
@endif
@endsection