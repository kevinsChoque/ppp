<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Agregar planificacion anual</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="frmInsertFileSecundaria" action="{{url('insert/filesecundaria')}}" method="post" enctype="multipart/form-data" class="m-0"> 
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Grado:</label>
                            <select class="form-control form-control-sm select2bs4" id="selectGrados" name="selectGrados" style="width: 100%;">
                                <option disabled selected>Seleccione un grado</option>
                                @foreach($tGrado as $item)
                                    <option value="{{$item->idgrado}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Sección</label>
                            <select class="form-control form-control-sm select2bs4" multiple="multiple" id="selectSeccions" name="selectSeccions[]" style="width: 100%;">
                                @foreach($tSeccion as $item)
                                <option value="{{$item->idseccion}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Periodo académico:</label>
                            <div class='form-control form-control-sm text-center' class="radio">
                                <input type="radio" name="periodo" id="Trimestre" value="Trimestre" checked> 
                                <label for="Trimestre" style='margin-right:20px' >Trimestre</label>
                                <input type="radio" name="periodo" id="Bimestre" value="Bimestre">
                                <label for="Bimestre">Bimestre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Curso:</label>
                            <select class="form-control form-control-sm select2bs4" id="selectCursos" name="selectCursos" data-placeholder="Seleccionar curso" style="width: 100%;">  
                                <option disabled selected>Seleccione un curso</option> 
                                @foreach($tCurso as $item)
                                <option value="{{$item->idcurso}}">{{$item->nombre}}</option>
                                @endforeach
                                </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                            <textarea class="form-control form-control-sm" rows="1" id="txtComentario" name="txtComebtario" placeholder="Enter ..."></textarea>   
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
            </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar"  form="frmInsertFileSecundaria" class="btn btn-success btn-sm float-right ml-1">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de archivos</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Formato</th>
                                <th>Peso</th>
                                <th>Nivel</th>
                                <th>Periodo academico</th>
                                <th>Grado</th>
                                <th>Sección</th>
                                <th>Curso</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($tCursoxDocente as $item)
                          <tr class="text-center">
                                <td>{{$item->nombreArchivo}}</td>
                                <td>{{$item->formato}}</td>
                                <td>{{$item->peso}}</td>
                                <td>{{$item->nivel}}</td>
                                <td>{{$item->periodoAcademico}}</td>
                                <td>
                                    @foreach($item->tdetallecursopordocente as $value)
                                    <span class="right badge bg-info color-palette">{{$value->tgrado->nombre}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    @foreach($item->tdetallecursopordocente as $value)
                                    <span class="right badge bg-info color-palette">{{$value->tseccion->nombre}}</span>
                                    @endforeach
                                </td>
                                <td>
                                    <span class="btn right badge bg-info color-palette" data-toggle="tooltip" title="@foreach($item->tdetallecursopordocente as $value){{$value->tCurso->nombre}}/  @break @endforeach">Ver cursos</span>
                                </td>
                                <td>
                                    <span class="btn btn-primary btn-xs" data-toggle="tooltip" title="Editar" onclick="window.location.href='{{url('file/delete')}}/{{$item->idcd}}';"><i class="fa fa-edit"></i></span>

                                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>

                                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i  class="fa fa-download"></i></a>

                                    <span class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="window.location.href='{{url('delete/file')}}/{{$item->idcd}}';">
                                        <i class="fa fa-trash"></i>
                                    </span>
                    
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!--<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title">Agregar archivo</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body">
            <form id="frmInsertFileSecundaria" action="{{url('insert/filesecundaria')}}" method="post" enctype="multipart/form-data"> 
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Archivo</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                        {{csrf_field()}}
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Periodo académico</label>
                            <div class='form-control form-control-sm' class="radio">
                                <input type="radio" name="periodo" id="Trimestre" value="Trimestre"> 
                                <label for="Trimestre" style='margin-right:20px' >Trimestre</label>
                                <input type="radio" name="periodo" id="Bimestre" value="Bimestre">
                                <label for="Bimestre" style='margin-right:20px' >Bimestre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label>Comentario</label>
                            <textarea class="form-control" rows="1" id="txtComentario" name="txtComebtario" placeholder="Enter ..."></textarea>   
                        </div>
                    </div>
                </div>
                <div class=" p-2">
                    <input type="button" class="btn btn-warning btn-sm float-right ml-1" data-toggle="tooltip" title="Agregar fila" onclick="agregarfila()"><i class="fa fa-plus"></i>
                    <!-- <button class="btn btn-warning btn-sm float-right ml-1" data-toggle="tooltip" title="Agregar fila" onclick="agregarfila()">
                        <i class="fa fa-plus">
                    </button> -->
                </div>
                <!-- <div class="row">
                    <table id="agregar" class="table table-sm table-bordered table-striped">
                        <thead>
                            <tr>
                                <th >Grado</th>
                                <th>Seccion</th>
                                <th>Curso</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control form-control-sm select2bs4" id="selectGrados2" name="selectGrados2" style="width: 100%;">
                                            @foreach($tGrado as $item)
                                                <option value="{{$item->idgrado}}">{{$item->nombre}}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control form-control-sm select2bs4" multiple="multiple" id="selectSeccions2" name="selectSeccions2[]" style="width: 100%;">
                                        @foreach($tSeccion as $item)
                                        <option value="{{$item->idseccion}}">{{$item->nombre}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <select class="form-control form-control-sm select2bs4" id="selectCursos2" name="selectCursos2" data-placeholder="Seleccionar curso" style="width: 100%;">   
                                        @foreach($tCurso as $item)
                                        <option value="{{$item->idcurso}}">{{$item->nombre}}</option>
                                        @endforeach
                                        </select>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div> -->
                <!-- <div class=" p-2">
                    {{csrf_field()}}
                    <input type="button" value="Guardar"  form="frmInsertFile" class="btn btn-success btn-sm float-right ml-1">
                    <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1">
                </div> -->
            </form>
        </div>
    </div>
</div>-->
<script>
     var cont=2;
    function agregarfila()
    {
        var fila='<tr><td><div class="form-group"><select class="form-control form-control-sm select2bs4" id="selectGrados3" name="selectGrados3" style="width: 100%;">@foreach($tGrado as $item)<option value="{{$item->idgrado}}">{{$item->nombre}}</option>@endforeach</select></div></td><td><div class="form-group"><select class="form-control form-control-sm select2bs4" multiple="multiple" id="selectSeccions6" name="selectSeccions6[]" style="width: 100%;">@foreach($tSeccion as $item)<option value="{{$item->idseccion}}">{{$item->nombre}}</option>@endforeach</select></div></td><td><div class="form-group"><select class="form-control form-control-sm select2bs4" id="selectCursos2" name="selectCursos2" data-placeholder="Seleccionar curso" style="width: 100%;">@foreach($tCurso as $item)<option value="{{$item->idcurso}}">{{$item->nombre}}</option>@endforeach</select></div></td></tr>'
    $('#agregar').append(fila);

    }
</script>
<script>
    $("#frmInsertFileSecundaria").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            selectGrados: "required",
            selectCursos: "required",
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
        },
        messages: {
            selectGrados: "Seleccione el grado",
            selectCursos: "Seleccione el curso",
            file: {
                required: 'Ingrese un documento',
                extension:"Solo se acepta word, pdf, excel"
            }
        },
    });
</script>
<script src="{{asset('viewResources/file/insert.js')}}"></script>
