@extends('template/template')
@section('generalBody')
<style>
    .showEdit{visibility: hidden;}
    .seleccionado{background:rgba(91, 208, 43,.7) !important;}
</style>
<div class="container-fluid my-3">
    <div class="row">
        @if(count($tCursoxDocente)==0)
        <div class="alert alert-warning w-100 text-center m-0">Debe de <strong><u>cargar las unidades </u></strong> para poder subir los archivos de sesiones.</div>
        @endif
        @foreach($tCursoxDocente as $item)
        <div class="col-lg-3 col-6">
            <div class="small-box bg-info m-0">
                <div class="inner py-0">
                    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false)
                        <h3 class="m-0 pt-2 text-lg" style="font-size: 1rem !important;">{{$item->tcurso->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">{{$item->tGrado->nombre}}</p>
                        <p class="mb-0" id="cantU{{$item->idcd}}" data-cantU="{{count($item->tUnidad)}}"><i class="fa fa-file"></i> Tiene {{count($item->tUnidad)}} unidad</p>
                        <p class="mb-0"><i class="fa fa-file"></i> Tiene 
                        @php $totalS = 0; @endphp
                        @foreach($item->tUnidad as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tSesion as $item3)
                                @php
                                    $subtotal=$subtotal+1;
                                @endphp
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} sesion</p>
                    @else
                        <h3 class="m-0 pt-2 text-lg">{{$item->tgrado->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">Seccion {{$item->tSeccion->nombre}}</p>
                        <p class="mb-0" id="cantU{{$item->idcd}}" data-cantU="{{count($item->tUnidad)}}"><i class="fa fa-file"></i> Tiene {{count($item->tUnidad)}} unidad</p>
                        <p class="mb-0"><i class="fa fa-file"></i> Tiene 
                        @php $totalS = 0; @endphp
                        @foreach($item->tUnidad as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tSesion as $item3)
                                @php
                                    $subtotal=$subtotal+1;
                                @endphp
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} sesion</p>
                    @endif
                </div>
                <div class="icon"><i class="fa fa-briefcase"></i></div>
                <a href="#" class="small-box-footer p-0 showFormUs" data-id="{{$item->idcd}}" data-toggle="collapse" data-target="#id{{$item->idcd}}" aria-expanded="false" aria-controls="id{{$item->idcd}}"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        @endforeach
    </div>
</div>
<div class="alert alert-warning w-100 text-center m-0 ausenciaUnidad" style="display: none;">Debe de <strong><u>cargar las unidades </u></strong> para poder subir los archivos de sesiones.</div>
<div class="contenedor-ajax">
    <div class="container-fluid p-0">
        <div class="card card-default card-info card-outline">
            <div class="card-header py-2 pl-2 ui-sortable-handle" style="cursor: move;">
                <h3 class="card-title font-weight-bold">Lista de todas las sesiones</h3>
                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                </div>
            </div>
            @include('fileDcte/share/listSesion')
        </div>
    </div>
</div>
<script>
    $('.showFormUs').on('click',function(){
        $('.seleccionado').removeClass('seleccionado');
        $(this).addClass('seleccionado');

        if($('#cantU'+$(this).attr('data-id')).attr('data-cantU')==0)
        {
            $('.ausenciaUnidad').css('display','block');
            $('.contenedor-ajax').css('display','none');
            return 0;
        }
        $('.ausenciaUnidad').css('display','none');
        $('.contenedor-ajax').css('display','block');

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSesion/showFormInsertS') }}",
            data: {idcd:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                $('.contenedor-ajax>div').remove();
                $('.contenedor-ajax').append(result);
                loadUnid();
            }
        });
    });
    function loadUnid()
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSesion/loadUnid') }}",
            data: {idcd:$('.seleccionado').attr('data-id')},
            method: 'get',
            success: function(result){
                $("#selectUnidad").empty();
                $("#selectUnidad").append("<option disabled selected>Seleccione unidad</option>");
                $.each( result, function(key, value){
                    $("#selectUnidad").append("<option value='" + value.idunidad + "'>"+value.nombre+"</option>");
                });
            }
        });
    }
</script>
@endsection