@extends('template/template')
@section('generalBody')
<div class="container-fluid p-0 mt-3">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Programaciones anuales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
        	<fieldset class="border border-secundary p-2">
        		<legend class="p-2">Planes anules</legend>
        	</fieldset>
            <!-- <form id="frmInsertFileSecundaria" action="http://localhost/drea/public/insert/filesecundaria" method="post" enctype="multipart/form-data" class="m-0"> 
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class="form-control form-control-sm" type="File" id="file" name="file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                            <textarea class="form-control form-control-sm" rows="1" id="txtComentario" name="txtComebtario" placeholder="Enter ..."></textarea>   
                        </div>
                    </div>
                </div>
                <input type="hidden" name="_token" value="qRfsTfJEvRYXBxaOmOHkhPtogmAxyH9BCMGBYPLX">
            </form> -->
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="frmInsertFileSecundaria" class="btn btn-success btn-sm float-right ml-1">
            <a href="http://localhost/drea/public/fileDcte/insertUS" class="btn btn-primary btn-sm float-right ml-1">Mostrar todos los archivos</a>
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1">
        </div>
    </div>
</div>
@endsection