@extends('template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Agregar archivo</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="frmInsertEducationalMaterial" action="{{url('insert/educationalmaterial')}}" method="post" enctype="multipart/form-data"> 
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Edad:</label>
                            <select class="form-control form-control-sm" id="selectGrado" name="selectGrado" style="width: 100%;">
                                <option selected='selected' disabled='disabled'></option>
                                @foreach($tGrado as $item)
                                    <option value="{{$item->idgrado}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Curso:</label>
                            <select class="form-control form-control-sm" id="selectCurso" name="selectCurso" style="width: 100%;">
                                <option selected='selected' disabled='disabled'></option>   
                                @foreach($tCurso as $item)
                                <option value="{{$item->idcurso}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                    </div>
                    {{csrf_field()}}
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Sección</label>
                            <select class="form-control form-control-sm" id="selectSeccion" name="selectSeccion" style="width: 100%;">
                            <option selected='selected' disabled='disabled'></option>
                            @foreach($tSeccion as $item)
                            <option value="{{$item->idseccion}}">{{$item->nombre}}</option>
                            @endforeach
                            </select>
                        </div>
                    </div> -->
                    <!-- <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Link del video</label>
                            <input type="text" class='form-control form-control-sm' name="txtLink" id="txtLink" placeholder='Link del archivo'> 
                        </div>
                    </div> -->
                    <div class="col-md-4">
                        <label class="m-0">Link de video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="text" name="txtLink" id="txtLink" class="form-control form-control-sm" placeholder="Link">
                        </div>
                    </div>
                    
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Nombre:</label>
                            <input type="text" class='form-control form-control-sm' name="txtNombre" id="txtNombre" placeholder='Nombre del archivo'> 
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                                <textarea class="form-control form-control-sm" id="txtComentario" name="txtComentario" rows="1"></textarea>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar"  form="frmInsertEducationalMaterial" class="btn btn-success btn-sm float-right ml-1">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales educativos</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="agregar" class="table table-sm table-hover m-0">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Formato</th>
                                <th>Peso</th>
                                <th>Nivel</th>
                                <th>Edad</th>
                                <th>Curso</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($tCursoxDocente as $item)
                          <tr class="text-center">
                                <td>{{$item->nombreArchivo}}</td>
                                <td>
                                    @if($item->formato!='')
                                    <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                                    {{$item->formato=='docx'?'fa-file-word':''}}
                                    {{$item->formato=='xlsx'?'fa-file-excel':''}}"></span> {{$item->formato}}
                                    @else- -
                                    @endif
                                </td>
                                <td>{{$item->peso}}</td>
                                <td>{{$item->nivel}}</td>
                                <td class='text-center'>
                                    @foreach($item->tdetallecursopordocente as $value)
                                        {{$value->tGrado->nombre}}
                                    @endforeach
                                </td>
                                <td class='text-center'>
                                    <span class="btn right badge bg-info color-palette" data-toggle="tooltip" title="@foreach($item->tdetallecursopordocente as $value){{$value->tCurso->nombre}}@endforeach">{{$item->tdetallecursopordocente[0]->tCurso->nombre}}</span>
                    
                                </td>
                                <td class='text-center'>
                                    <span class="btn btn-primary btn-xs" data-toggle="tooltip" title="Editar" onclick="window.location.href='{{url('file/delete')}}/{{$item->idcd}}';"><i class="fa fa-edit"></i></span>
                                    
                                    @if($item->url!= null)
                                    <a href="{{$item->url}}" data-toggle="tooltip" title="Ver video" class="btn btn-warning btn-xs" target="_blank"><i class="fas fa-video"></i></a>
                                    @endif

                                    <span class="btn btn-info btn-xs fab fa-rocketchat" data-toggle="tooltip" title="{{$item->comentario}}"></span>
                                    @if($item->formato!= null)
                                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                    @endif
                                    <span class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="window.location.href='{{url('delete/educationalmaterial')}}/{{$item->idcd}}';"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#frmInsertEducationalMaterial").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            selectGrado: "required",
            selectCurso: "required",
            txtNombre: "required",
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
        },
        messages: {
            selectGrado: "Seleccione el grado",
            selectCurso: "Seleccione el curso",
            txtNombre: "Ingrese el nombre",
            file: {
                required: 'Ingrese un documento',
                extension:"Solo se acepta word, pdf, excel"
            }
        },
    });
</script>
<script src="{{asset('viewResources/file/inserteducationalmaterial.js')}}"></script>
@endsection


