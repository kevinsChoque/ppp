<!-- <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" />
<script src="{{asset('/js/select2.min.js')}}"></script> -->
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Agregar plan curricular</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="frmInsertFile" action="{{url('insert/file')}}" method="post" enctype="multipart/form-data" class="m-0"> 
                <div class="row justify-content-center align-items-center">
                    <h6 class="font-weight-bold">Subir plan curricular por: </h6>
                    <div class="mx-2">
                        <input type="radio" name="radioarchivo" id="Cliclo" value="Ciclo">
                        <label for="Cliclo" style='margin-right:20px' class="font-weight-normal">Edad</label>
                        <input type="radio" name="radioarchivo" id="Seccion" value="Seccion"> 
                        <label for="Seccion" style='margin-right:20px' class="font-weight-normal">Sección</label>
                        <input type="radio" name="radioarchivo" id="Curso" value="Curso" checked>
                        <label for="Curso" style='margin-right:20px' class="font-weight-normal">Curso</label>
                    </div>
                </div>
                <hr class="mt-0">
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Edad:</label>
                            <select class="form-control form-control-sm" id="selectGrado" name="selectGrado" style="width: 100%;">
                                <option selected disabled>Seleccione la edad</option>
                                @foreach($tGrado as $item)
                                    <option value="{{$item->idgrado}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" id="file" name="file">
                        </div>
                        {{csrf_field()}}
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Sección:</label>
                            <select class="form-control form-control-sm" id="selectSeccion" name="selectSeccion" style="width: 100%;">
                            <option selected disabled>Seleccione la seccion</option>
                            @foreach($tSeccion as $item)
                            <option value="{{$item->idseccion}}">{{$item->nombre}}</option>
                            @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Periodo académico:</label>
                            <div class="form-control form-control-sm text-center" class="radio">
                                <input type="radio" name="periodo" id="Trimestre" value="Trimestre" checked> 
                                <label for="Trimestre" style='margin-right:20px' >Trimestre</label>
                                <input type="radio" name="periodo" id="Bimestre" value="Bimestre">
                                <label for="Bimestre">Bimestre</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Curso:</label>
                            <!-- <select id="shareDocente" class="form-control form-control-sm select2" name="docentes[]" multiple="multiple"> -->
                            <select id="selectCurso" name="selectCurso[]" class="form-control form-control-sm" style="width: 100%;" multiple="multiple">
                                @foreach($tCurso as $item)
                                <option value="{{$item->idcurso}}">{{$item->nombre}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                                <textarea class="form-control form-control-sm" id="txtComentario" name="txtComentario" rows="1"></textarea>
                        </div>
                    </div>
                    <!-- <div class="col-md-12">
                        <div class="form-group">
                            <label>Comentario</label>
                                <textarea class="form-control form-control-sm" id="txtComentario" name="txtComentario"></textarea>
                        </div>
                    </div> -->
                </div>
            </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar"  form="frmInsertFile" class="btn btn-success btn-sm float-right ml-1">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de plan curricular</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="agregar" class="table table-sm table-hover m-0">
                        <thead class="text-center">
                            <tr>
                                <th>Nombre</th>
                                <th>Formato</th>
                                <th>Peso</th>
                                <th>Nivel</th>
                                <th>Periodo academico</th>
                                <th>Edad</th>
                                <th>Sección</th>
                                <th>Curso</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                          @foreach($tCursoxDocente as $item)
                          <tr class="text-center">
                                <td>{{$item->nombreArchivo}}</td>
                                <td>
                                    @if($item->formato!='')
                                    <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                                    {{$item->formato=='docx'?'fa-file-word':''}}
                                    {{$item->formato=='xlsx'?'fa-file-excel':''}}"></span> {{$item->formato}}
                                    @else- -
                                    @endif
                                </td>
                                <td>{{$item->peso}}</td>
                                <td>{{$item->nivel}}</td>
                                <td>{{$item->periodoAcademico}}</td>
                                <td>
                                    @foreach($item->tdetallecursopordocente as $value)
                                    <span class="right badge bg-info color-palette">{{$value->tgrado->nombre}}</span>
                                    @break
                                    @endforeach
                                </td>
                                <td>
                                    @if(count($item->tdetallecursopordocente)==7 && $item->tdetallecursopordocente[0]->idseccion==null) 
                                        <span class="right badge bg-info color-palette">Unica seccion</span>
                                    @else
                                        @foreach($item->tdetallecursopordocente as $value)
                                        <span class="right badge bg-info color-palette">{{$value->tseccion->nombre}}</span>
                                        @break
                                        @endforeach
                                    @endif
                                </td>
                                <td>
                                    <span class="btn right badge bg-info color-palette" data-toggle="tooltip" title="@foreach($item->tdetallecursopordocente as $value){{$value->tCurso->nombre}}/@endforeach">Ver cursos</span>
                                </td>
                                <td>
                                <a href="#" class="edit btn btn-primary btn-xs" data-id="{{$item->idcd}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a>

                                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>

                                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                                    
                                    
                                    <span class="btn btn-danger btn-xs" data-toggle="tooltip" title="Eliminar" onclick="window.location.href='{{url('delete/file')}}/{{$item->idcd}}';"><i class="fa fa-trash"></i></span>
                                </td>
                            </tr>
                          @endforeach
                        </tbody>
                        <tfoot>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    
    $( document ).ready(function() 
    { 
        $("#selectCurso").select2({
            placeholder: "Seleccione un curso",
        });
    });

    $('.limpiar').on('click',function(){
        $('#selectGrado option:nth-child(1)').prop("selected",true);
        $('#selectSeccion option:nth-child(1)').prop("selected",true);
        $('#frmInsertFile').attr('action','{{url('insert/file')}}');
        $('#selectCurso').val(null).trigger('change');
        $('#file').val('');
        $('#txtComentario').val('');
    });

    $("#frmInsertFile").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            selectGrado:"required",
            selectSeccion:"required",
            selectCurso:"required",
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
        },
        messages: {
            selectGrado:"Seleccione una edad",
            selectSeccion:"Seleccione la seccion",
            selectCurso:"Seleccione el curso",
            file: {
                required: 'Ingrese un documento',
                extension:"Solo se acepta word, pdf, excel"
            }
        },
    });
</script>
<script>
    $('.edit').on('click',function(){
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileIe/edit') }}",
            data: {idcd:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                console.log(result.data);
                $('#formInsert').attr('action','{{url('fileIe/edit')}}');
                $('.btnSubmit').val('Guardar cambios');
                
                
                $('#txtComentario').val(result.data.comentario);
                $('#file').addClass('ignore');
            }
        });
    });
</script>
<script src="{{asset('viewResources/file/insert.js')}}"></script>