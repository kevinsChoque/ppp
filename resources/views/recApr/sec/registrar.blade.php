@extends('recursosAprendizaje/template/template')
@section('generalBody')
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de experiencias de aprendizaje para secundaria</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('expAprSec/registrar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                <div class="row">
                    <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
	                        <label class="m-0">Ugel:</label>
	                        <select name="ugel" id="ugel" class="form-control form-control-sm" disabled>
	                            <option disabled="" selected="">Elija la ugel:</option>
	                            <option value="UGEL Abancay">UGEL Abancay</option>
	                            <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
	                            <option value="UGEL Antabamba">UGEL Antabamba</option>
	                            <option value="UGEL Aymaraes">UGEL Aymaraes</option>
	                            <option value="UGEL Cotabambas">UGEL Cotabambas</option>
	                            <option value="UGEL Chincheros">UGEL Chincheros</option>
	                            <option value="UGEL Grau">UGEL Grau</option>
	                            <option value="UGEL Huancarama">UGEL Huancarama</option>
	                        </select>
	                    </div>
                    </div>
                    
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Mes:</label>
                            <select name="mes" id="mes" class="form-control form-control-sm">
                                <option disabled="" selected="">Elija el mes:</option>
                                <option value="Enero">Enero</option>
                                <option value="Febrero">Febrero</option>
                                <option value="Marzo">Marzo</option>
                                <option value="Abril">Abril</option>
                                <option value="Mayo">Mayo</option>
                                <option value="Junio">Junio</option>
                                <option value="Julio">Julio</option>
                                
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3 gradoPs">
                        <div class="form-group mb-2">
                            <label class="m-0">Grado:</label>
                            <select name="grado" id="grado" class="form-control form-control-sm">
                                <option disabled selected>Grado:</option>
                                <option value="1">1° Grado</option>
                                <option value="2">2° Grado</option>
                                <option value="3">3° Grado</option>
                                <option value="4">4° Grado</option>
                                <option value="5">5° Grado</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="m-0">Descripcion:</label>
                            <textarea name="descripcion" id="descripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Subir archivo en pdf:</label>
                            <input class="form-control form-control-sm" type="file" name="archivoPdf" id="archivoPdf">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Subir archivo en word:</label>
                            <input class="form-control form-control-sm" type="file" name="archivoDoc" id="archivoDoc">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <label class="m-0">Enlace de archivo:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                <span class="input-group-text"><i class="fa fa-link"></i></span>
                            </div>
                            <input type="text" name="earchivo" id="earchivo" class="form-control form-control-sm" placeholder="Archivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                <span class="input-group-text"><i class="fa fa-link"></i></span>
                            </div>
                            <input type="text" name="evideo" id="evideo" class="form-control form-control-sm" placeholder="Archivo">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Enlace de audio:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                <span class="input-group-text"><i class="fa fa-link"></i></span>
                            </div>
                            <input type="text" name="eaudio" id="eaudio" class="form-control form-control-sm" placeholder="Archivo">
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
            <!-- <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar"> -->
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0 w-100">
                        <thead class="text-center">
                            <tr>
                                <th>Ugel</th>
                                <th>Mes</th>
                                <th>Grado</th>
                                <th>Nombre</th>
                                <th>Descripcion</th>
                                <th>Archivo</th>
                                <th>Enlaces</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($list as $item)
                            <tr class="text-center">
                                <td>{{$item->ugel}}</td>
                                <td>{{$item->mes}}</td>
                                <td>{{$item->grado}}</td>
                                <td>{{$item->nombre}}</td>
                                <td>{{$item->descripcion}}</td>
                                <td>{{$item->archivoPdf}}</td>
                                <td>
                                    @if($item->earchivo!='')
                                        <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
                                    @endif
                                    @if($item->evideo!='')
                                        <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
                                    @endif
                                    @if($item->eaudio!='')
                                        <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
                                    @endif
                                </td>
                                <td>
                                    <!-- {{asset('fileea/sec/pdf').'/'.$item->archivoPdf}} -->
                                    @if($item->archivoPdf!='')
                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
                                    @endif
                                    @if($item->archivoDoc!='')
                                        <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
                                    @endif
                                    <a href="#" class="btn btn-primary btn-xs editar" data-ide="{{$item->ide}}" title="Editar"><i class="fa fa-edit fa-2x"></i></a>
                                    <a href="{{url('/expAprSec/delete').'/'.$item->ide}}" class="btn btn-danger btn-xs" title="Eliminar"><i class="fa fa-trash fa-2x"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- MODAL -->
<div class="modal fade" id="modalEditar" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered modal-xl">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title" id="exampleModalLabel">Editar registro</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row text-center">
                    <div class="col-lg-6 ">
                        <div class="alert alert-warning py-0">
                            <p class="m-0 font-weight-bold">Al subir un nuevo archivo se eliminara el anterior archivo.</p>
                        </div>
                    </div>
                    <div class="col-lg-6 ">
                        <div class="alert alert-info py-0">
                            <p class="m-0"><span class="btn btn-secundary p-1"><i class="fa fa-eraser" style="cursor: pointer;"></i></span> Boton para borrar el enlace.</p>
                        </div>
                    </div>
                </div>
                <form id="formEditar" action="{{url('expAprSec/editar')}}" method="post" enctype="multipart/form-data" class="m-0" novalidate="novalidate">
                    <input type="hidden" name="ide" id="ide">
                    <div class="row">
                        <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                        <div class="col-md-3">
                            <div class="form-group mb-2">
                                <label class="m-0">Ugel:</label>
                                <select name="eugel" id="eugel" class="form-control form-control-sm" disabled>
                                    <option disabled="" selected="">Elija la ugel:</option>
                                    <option value="UGEL Abancay">UGEL Abancay</option>
                                    <option value="UGEL Andahuaylas">UGEL Andahuaylas</option>
                                    <option value="UGEL Antabamba">UGEL Antabamba</option>
                                    <option value="UGEL Aymaraes">UGEL Aymaraes</option>
                                    <option value="UGEL Cotabambas">UGEL Cotabambas</option>
                                    <option value="UGEL Chincheros">UGEL Chincheros</option>
                                    <option value="UGEL Grau">UGEL Grau</option>
                                    <option value="UGEL Huancarama">UGEL Huancarama</option>
                                </select>
                            </div>
                        </div>
                        
                        <div class="col-md-3">
                            <div class="form-group mb-2">
                                <label class="m-0">Mes:</label>
                                <select name="emes" id="emes" class="form-control form-control-sm">
                                    <option disabled="" selected="">Elija el mes:</option>
                                    <option value="Enero">Enero</option>
                                    <option value="Febrero">Febrero</option>
                                    <option value="Marzo">Marzo</option>
                                    <option value="Abril">Abril</option>
                                    <option value="Mayo">Mayo</option>
                                    <option value="Junio">Junio</option>
                                    <option value="Julio">Julio</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3 gradoPs">
                            <div class="form-group mb-2">
                                <label class="m-0">Grado:</label>
                                <select name="egrado" id="egrado" class="form-control form-control-sm">
                                    <option disabled selected>Grado:</option>
                                    <option value="1">1° Grado</option>
                                    <option value="2">2° Grado</option>
                                    <option value="3">3° Grado</option>
                                    <option value="4">4° Grado</option>
                                    <option value="5">5° Grado</option>
                                </select>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <div class="form-group mb-2">
                                <label class="m-0">Nombre:</label>
                                <input type="text" name="enombre" id="enombre" class="form-control form-control-sm">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="m-0">Descripcion:</label>
                                <textarea name="edescripcion" id="edescripcion" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="m-0">Subir archivo en pdf: 
                                    <a class="text-success carchivoPdf" target="_blank" style="display: none;">(ver archivo)</a>
                                    <span class="text-warning sarchivoPdf" style="display: none;">(registro sin archivo)</span>
                                </label>
                                <input class="form-control form-control-sm" type="file" name="earchivoPdf" id="earchivoPdf">
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label class="m-0">Subir archivo en word: 
                                    <a class="text-success carchivoDoc" target="_blank" style="display: none;">(ver archivo)</a>
                                    <span class="text-warning sarchivoDoc" style="display: none;">(registro sin archivo)</span>
                                </label>
                                <input class="form-control form-control-sm" type="file" name="earchivoDoc" id="earchivoDoc">
                            </div>
                        </div>
                        <!-- <div class="col-md-4">
                            <div class="form-group">
                                <label class="m-0">Subir archivo en word:</label>
                                <input class="form-control form-control-sm" type="file" name="earchivoDoc" id="earchivoDoc">
                            </div>
                        </div> -->
                        <div class="col-md-4">
                            <label class="m-0">Enlace de archivo:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="eearchivo" id="eearchivo" class="form-control form-control-sm" placeholder="Archivo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="m-0">Enlace de video:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="eevideo" id="eevideo" class="form-control form-control-sm" placeholder="Archivo">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label class="m-0">Enlace de audio:</label>
                            <div class="input-group mb-3">
                                <div class="input-group-prepend">
                                    <span class="input-group-text limpiarLink" style="cursor: pointer;"><i class="fa fa-eraser"></i></span>
                                    <span class="input-group-text"><i class="fa fa-link"></i></span>
                                </div>
                                <input type="text" name="eeaudio" id="eeaudio" class="form-control form-control-sm" placeholder="Archivo">
                            </div>
                        </div>
                    </div>
                    {{csrf_field()}}
                </form>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="submit" form="formEditar" class="btn btn-success btn-sm">Guardar cambios</button>
            </div>
        </div>
    </div>
</div>
<script>
    $(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );

        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
    $('.editar').on('click',function(){
        
        var ide=$(this).attr('data-ide');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/expAprSec/editar') }}",
            data: {ide:ide},
            method: 'get',
            success: function(result){
                console.log(result);
                $('#ide').val(result.data.ide);
                $('#eugel').val(result.data.ugel);
                $("#emes option[value="+ result.data.mes +"]").attr("selected",true);
                $("#egrado option[value="+ result.data.grado +"]").attr("selected",true);
                $('#enombre').val(result.data.nombre);
                $('#edescripcion').val(result.data.descripcion);
                if(result.data.archivoPdf!=null)
                {
                    $('.carchivoPdf').css('display','inherit');
                    $('.sarchivoPdf').css('display','none');
                    $('.carchivoPdf').attr('href','http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/'+result.data.archivoPdf);
                }
                else
                {
                    $('.sarchivoPdf').css('display','inherit');
                    $('.carchivoPdf').css('display','none');
                }
                if(result.data.archivoDoc!=null)
                {
                    $('.carchivoDoc').css('display','inherit');
                    $('.sarchivoDoc').css('display','none');
                    $('.carchivoDoc').attr('href','http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/doc/'+result.data.archivoDoc);
                }
                else
                {
                    $('.sarchivoDoc').css('display','inherit');
                    $('.carchivoDoc').css('display','none');
                }
                $('#eearchivo').val(result.data.earchivo);
                $('#eevideo').val(result.data.evideo);
                $('#eeaudio').val(result.data.eaudio);
                $('#modalEditar').modal('show');
            }
        });
    });
    $('.limpiarLink').on('click',function(){
        $(this).parent().parent().find('input').val('');
    });
</script>
@endsection