<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>DREA | Apurímac</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
  <!-- icheck bootstrap -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/adminlte.min.css')}}">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition login-page">
<div class="login-box">
  <div class="login-logo">
    <a href="{{asset('plugin/adminlte/index2.html')}}"><b>DRE</b>Apurímac | GESTION DE TABLETAS DE PRONATEL</a>
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
      <p class="login-box-msg">Ingrese sus datos para acceder al sistema</p>

        <form id="frmLogin" action="{{url('user/loginTabletasPronatel')}}" method="post">
            <div class="input-group mb-3">
                <input type="text" id='dni' name='dni' class="form-control" placeholder="DNI">
                <div class="input-group-append">
                    <div class="input-group-text"><span class="fas fa-user"></span></div>
                </div>
            </div>
            <div class="input-group mb-3">
                <input type="password" id='password' name='password' class="form-control" placeholder="CONTRASEÑA">
                <div class="input-group-append">
                    <div class="input-group-text"><span class="fas fa-lock"></span></div>
                </div>
            </div>
            
        <div class="row">
          <div class="col-8">
            <!-- <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Recordarme
              </label>
            </div> -->
          </div>
          <!-- /.col -->
          <div class="col-4">
          {{csrf_field()}}
            <button type="submit" class="btn btn-primary btn-block">Ingresar</button>
          </div>
          <!-- /.col -->
        </div>
      </form>
    </div>
    <!-- /.login-card-body -->
  </div>
</div>
<!-- /.login-box -->

<!-- jQuery -->
<script src="{{asset('plugin/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<script src="{{asset('plugin/pnotify/pnotify.custom.min.js')}}"></script>
<link rel="stylesheet" href="{{asset('plugin/pnotify/pnotify.custom.min.css')}}">
<!-- Bootstrap 4 -->
<script src="{{asset('plugin/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('plugin/adminlte/dist/js/adminlte.min.js')}}"></script>
</body>
</html>
@if(Session::has('globalMessage'))
    <script>
    $(function()
    {
      @if(Session::get('type')=='error')
                @foreach(explode('__BREAKLINE__', Session::get('globalMessage')) as $value)
          @if(trim($value)!='')
             new PNotify(
            {
              title : 'No se pudo proceder',
              text : '{{$value}}',
              type : '{{Session::get('type')}}'
            });
          @endif
        @endforeach
      @else
          swal(
          {
            title : '{{Session::get('type')=='success' ? 'Correcto' : 'Alerta'}}',
            text : '{!!Session::get('globalMessage')!!}',
            icon : '{{Session::get('type')=='success' ? 'success' : 'warning'}}',
            timer: {{Session::get('type')=='success' ? '2000' : '60000'}}
          });
        @endif
    });
    </script>
  @endif
