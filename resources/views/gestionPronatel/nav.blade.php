<li class="nav-item has-treeview">
    <a href="https://aulavirtual.dreapurimac.gob.pe/" class="nav-link">
        <i class="fas fa-vr-cardboard"></i>
        <p>AULA VIRTUAL</p>
    </a>
</li>
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>GESTION<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item has-treeview">
            <a href="{{url('pronatelAsignacion/asignacion')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Asignacion</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('expAprPri/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Devolucion</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('expAprSec/registrar')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Perdida</p>
            </a>
        </li>
    </ul>
</li>


