<!-- <li class="nav-item has-treeview">
    <a href="{{url('/')}}" class="nav-link">
        <i class="fas fa-home"></i>
        <p>INICIO</p>
    </a>
</li> -->
<li class="nav-item has-treeview">
    <a href="https://aulavirtual.dreapurimac.gob.pe/" class="nav-link">
        <i class="fas fa-vr-cardboard"></i>
        <p>AULA VIRTUAL</p>
    </a>
</li>
<li class="nav-item has-treeview">
    <a href="{{url('socioEmocional/registrar')}}" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>REGISTRAR MATERIALES</p>
    </a>
</li>
<li class="nav-item has-treeview">
    <a href="{{url('socioEmocional/listar')}}" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>LISTA DE MATERIALES</p>
    </a>
</li>
