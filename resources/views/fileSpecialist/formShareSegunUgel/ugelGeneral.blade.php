<style>
    .selectedRow{background: rgba(1, 1, 1, 0.22);}
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<meta name="_token" content="{{csrf_token()}}" />
@if(session()->has('estado'))
    <div class="alert alert-info mt-3 text-center font-weight-bold p-1">
    {{session()->get('estado')}}
    </div>
@endif
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Gestion de material</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 pt-2">
            <form id="formInsert" action="{{url('fileSpecialist/insertShare')}}" method="post" enctype="multipart/form-data" class="m-0">
                <input type="hidden" id="idarchivospecialist" name="idarchivospecialist">
                <!-- justify-content-center align-items-center /conjuntamente con row-->
                <div class="row">
                    <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                    <div class="col-md-6">
                        <input type="hidden" id="typeNivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                        @if(session()->get('Person')->tEspecialista->nivel=='General')
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group mb-2">
                                    <label class="m-0">Ugel:</label>
                                    <select name="ugel" id="ugel" class="form-control form-control-sm">
                                        <option disabled selected>Elija la ugel:</option>
                                        <option value="30001">UGEL Abancay</option>
                                        <option value="30002">UGEL Andahuaylas</option>
                                        <option value="30003">UGEL Antabamba</option>
                                        <option value="30004">UGEL Aymaraes</option>
                                        <option value="30005">UGEL Cotabambas</option>
                                        <option value="30006">UGEL Chincheros</option>
                                        <option value="30007">UGEL Grau</option>
                                        <option value="30008">UGEL Huancarama</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group mb-2">
                                    <label class="m-0">Nivel:</label>
                                    <select name="nivel" id="nivel" class="form-control form-control-sm">
                                        <option disabled selected>Elija el nivel:</option>
                                        <option value="inicial">Inicial</option>
                                        <option value="primaria">Primaria</option>
                                        <option value="secundaria">Secundaria</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        @else
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <input type="text" id="nivel" name="nivel" value="{{session()->get('Person')->tEspecialista->nivel}}" class="form-control form-control-sm" disabled>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="m-0">I.E.:</label>
                            <select name="ies" id="ies" class="form-control form-control-sm select2">
                                <option disabled selected>Primero seleccione nivel:</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group mb-2">
                            <label class="m-0">Nombre:</label>
                            <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                        </div>
                        <div class="form-group">
                            <label class="m-0">Archivo:</label>
                            <input class='form-control form-control-sm' type="File" name="file" id="file">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label class="m-0">Comentario:</label>
                            <textarea name="comentario" id="comentario" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <label class="m-0">Video:</label>
                        <div class="input-group mb-3">
                            <div class="input-group-prepend">
                                <span class="input-group-text"><i class="fa fa-video"></i></span>
                            </div>
                            <input type="email" name="video" id="video" class="form-control form-control-sm" placeholder="video">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="m-0">Estado:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="publico" id="publico" value="publico"> 
                            <label for="publico">Público</label>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <label class="m-0">Compartir:</label>
                        <div class="form-control form-control-sm text-center">
                            <input type="checkbox" name="compartir" id="compartir" value="compartir"> 
                            <label for="compartir">Con especialistas</label>
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
             </form>
        </div>
        <div class="card-footer p-2">
            <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1">
            <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de materiales</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
            <div class="row">
                <div class="col-md-12">
                    <table id="example1" class="table table-sm table-hover m-0">
                        <thead class="text-center">
                            <tr>
                                <th>C.M.</th>
                                <th>Nombre I.E.</th>
                                <th>Nivel</th>
                                <th>nombre</th>
                                <th>Formato</th>
                                <th>Peso</th>
                                <th>Registrado</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($listTae as $item)
                            <tr class="text-center">
                                <td>{{explode('//',$item->cie)[0]}}</td>
                                <td>{{explode('//',$item->cie)[1]}}</td>
                                <td>{{explode('//',$item->cie)[2]}}</td>
                                <td>{{$item->nombre}}</td>
                                <td>
                                    @if($item->formato!='')
                                    <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                                    {{$item->formato=='docx'?'fa-file-word':''}}
                                    {{$item->formato=='xls'?'fa-file-excel':''}}"></span> {{$item->formato}}
                                    @else- -
                                    @endif
                                </td>
                                <td>@if($item->peso!=''){{$item->peso}}
                                @else- -
                                @endif
                                </td>
                                <td>{{$item->createddate}}</td>
                                <td>
                                    @if($item->status==1)
                                    <span class="btn btn-xs" data-toggle="tooltip" title="Publico"><i class="fa fa-users"></i></span>
                                    @else
                                    <span class="btn btn-xs" data-toggle="tooltip" title="Privado"><i class="fa fa-user"></i></span>
                                    @endif
                                    <a href="#" class="edit btn btn-primary btn-xs" data-id="{{$item->idarchivospecialist}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a>

                                    <span class="btn btn-info btn-xs fab fa-rocketchat" data-toggle="tooltip" title="Comentario/observacion: {{$item->comentario}}"></span>
                                    @if(strpbrk($item->nombrereal, '-')!='')
                                    <a href="{{asset('fileSpecialist/')}}/{{$item->nombrereal}}.{{$item->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                    @endif
                                    <a href="{{url('/fileSpecialist/delete').'/'.$item->idarchivospecialist}}" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Borrar registro"><i class="fa fa-trash"></i></a>
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    $("#formInsert").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        rules: {
            nivel: "required",
            ies: "required",
            nombre: "required",
            comentario: "required",
        },
        messages: {
            nivel: "Ingrese nivel de  la I.E.",
            ies: "Elija la I.E.",
            nombre: "Ingrese el nombre",
            comentario: "Ingrese el comentario",
        },
    });
</script>
<script>
    var ppp = $('#typeNivel').val();
    if(ppp!='general')
        cargaie(ppp);
    console.log(ppp);
    $('.limpiar').on('click',function(){
        $('#formInsert').attr('action','{{url('fileSpecialist/insert')}}');
        $('#nombre').val('');
        $('#comentario').val('');
        $('#video').val('');
        $('#nivel option:nth-child(1)').prop("selected",true);
        $('#ies').empty();
        $("#ies").append("<option disabled selected>Primero seleccione nivel:</option>");
        $('#file').val('');
        $("#publico").prop('checked',false);
        $("#compartir").prop('checked',false);
        $('.msjIe').css('display','none');
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
    });
    $('.edit').on('click',function(){
        // alert($(this).attr('data-id'));
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $(this).attr('data-id');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/edit') }}",
            data: {idarchivospecialist:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                var datosIe = result.data.cie.split('//');
                $('#formInsert').attr('action','{{url('fileSpecialist/edit')}}');
                $('#idarchivospecialist').val(result.data.idarchivospecialist);
                $('#nivel option').each(function(){
                    if(datosIe[2]==$(this).val())
                        $(this).attr("selected",true);
                });
                cargaie(datosIe[2]);
                $('.msjIe').html('Compartido con la I.E. '+datosIe[0]+' con nombre '+datosIe[1]);
                $('.msjIe').css('display','block');
                $('#nombre').val(result.data.nombre);
                $('#comentario').val(result.data.comentario);
                $('#video').val(result.data.video);
                $('#ies').addClass('ignore');
                if(result.data.status==1) 
                    $("#publico").attr('checked',true);
                else
                    $("#publico").attr('checked',false);
                if(result.data.otherspecialist==1) 
                    $("#compartir").attr('checked',true);
                else
                    $("#compartir").attr('checked',false);
            }
        });
        console.log('llego aqui');
        
    });

    $('#nivel,#ugel').on('change',function(ev){
        var nivel=$('#nivel').val();
        var ugel=$('#ugel').val();
        if(nivel!==null && ugel!==null)
        {
            console.log('cargaie(nivel);');
            cargaie(nivel);
        }
            
    });
    $( document ).ready(function() 
    { 
        $('#ies').select2({
            placeholder: "Seleccione un curso",
        });
    });
    
    function cargaie(nivel)
    {
        console.log('entro a la funcion para cargar las ie');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getIeGeneralUgel') }}",
            data: {nivel:nivel,idugel:$('#ugel').val()},
            method: 'get',
            success: function(result){
                // $('.select2-selection.select2-selection--single').addClass('pt-1');
                $("#ies").empty();
                $("#ies").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    var valueIe = value.codigomodular+"//"+value.ie_nombre+"//"+$('#nivel').val();
                    $("#ies").append("<option value='" + valueIe + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }
</script>