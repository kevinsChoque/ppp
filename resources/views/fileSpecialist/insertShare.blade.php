@extends('template/template')
@section('generalBody')
@if(session()->get('Person')->tEspecialista->ugelid=='30000')
    @include('fileSpecialist/formShareSegunUgel/ugelGeneral')
@else
    @include('fileSpecialist/formShareSegunUgel/ugelParticular')
@endif

@endsection