<li class="nav-item has-treeview">
    <a href="{{url('/')}}" class="nav-link">
        <i class="fas fa-home"></i>
        <p>INICIO</p>
    </a>
</li>
<li class="nav-item has-treeview">
    <a href="https://aulavirtual.dreapurimac.gob.pe/" class="nav-link">
        <i class="fas fa-vr-cardboard"></i>
        <p>AULA VIRTUAL</p>
    </a>
</li>
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>REPOSITORIO<i class="right fas fa-angle-left"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item has-treeview">
            <a href="{{url('fileDcte/insertPa')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Planificacion anual</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('fileUnidad/insertUnidad')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Agregar unidad didactica</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('fileSesion/insertSesion')}}" class="nav-link">
                <i class="far fa-file nav-icon"></i>
                <p>Agregar sesiones</p>
            </a>
        </li>
        <!-- <li class="nav-item has-treeview">
            <a href="{{url('fileDcte/listFileForCurso')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Archivos por curso</p>
            </a>
        </li> -->
        <!-- <li class="nav-item has-treeview">
            <a href="{{url('insert/educationalmaterial')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Material educativo</p>
            </a>
        </li>
        <li class="nav-item has-treeview">
            <a href="{{url('insert/file')}}" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>
                Ver todo
                </p>
            </a>
        </li> -->
    </ul>
</li>
