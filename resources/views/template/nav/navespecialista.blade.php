<li class="nav-item">
    <a href="{{url('/')}}" class="nav-link"><i class="fas fa-home"></i><p>&nbsp; Inicio </p></a>
</li>
<li class="nav-item">
    <a href="https://aulavirtual.dreapurimac.gob.pe/" class="nav-link"><i class="fas fa-vr-cardboard"></i><p>&nbsp; Aula Virtual </p></a>
</li>
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>Mantenimiento<i class="fas fa-angle-left right"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="{{url('ie/updateDcte')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Institucion educativa</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{url('dcte/option')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Docentes</p>
        </a>
        </li>
    </ul>
</li>
<li class="nav-item has-treeview menu-open">
    <a href="#" class="nav-link">
        <i class="fas fa-briefcase"></i>
        <p>Repositorio<i class="fas fa-angle-left right"></i></p>
    </a>
    <ul class="nav nav-treeview">
        <li class="nav-item">
        <a href="{{url('fileSpecialist/insertShare')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Compartir con I.E.</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{url('fileSpecialist/insertMaterial')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Agregar material</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{url('fileSpecialist2/searchDocGes')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Documentos de Gestion I.E</p>
        </a>
        </li>
        <li class="nav-item">
        <a href="{{url('fileSpecialist2/searchPcDcte')}}" class="nav-link">
            <i class="far fa-file nav-icon"></i>
            <p>Plan Curricular Docente</p>
        </a>
        </li>
        <!-- <li class="nav-item">
        <a href="{{url('fileIe/shareFile')}}" class="nav-link">
            <i class="far fa-circle nav-icon"></i>
            <p>Compartir Archivo </p>
        </a>
        </li> -->
    </ul>
</li>