<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="_token" content="{{csrf_token()}}" />
  <title>DREA-REPOSITORIO</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/fontawesome-free/css/all.min.css')}}">
  <!-- Ionicons -->
  <!-- <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css"> -->
  <!-- <link rel="stylesheet" href="{{asset('css/ionicons.min.css')}}"> -->
  <!-- Tempusdominus Bbootstrap 4 -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css')}}">
  <!-- iCheck -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/icheck-bootstrap/icheck-bootstrap.min.css')}}">
  <!-- JQVMap -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/jqvmap/jqvmap.min.css')}}">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/dist/css/adminlte.min.css')}}">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css')}}">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/daterangepicker/daterangepicker.css')}}">
  <!-- summernote -->
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/summernote/summernote-bs4.css')}}">

  <script src="{{asset('plugin/sweetalert/sweetalert.min.js')}}"></script>
  
  <!-- Google Font: Source Sans Pro -->
  <!-- <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet"> -->
  <link rel="stylesheet" href="{{asset('plugin/pnotify/pnotify.custom.min.css')}}">
  <!-- <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/select2/css/select2.min.css')}}"> -->
  <link href="{{asset('/css/select2.min.css')}}" rel="stylesheet" />
  <link rel="stylesheet" href="{{asset('plugin/adminlte/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/datatables.min.css')}}">

  <link rel="stylesheet" href="{{asset('css/styleGeneral.css')}}">
  <!-- jQuery -->
  <script src="{{asset('plugin/adminlte/plugins/jquery/jquery.min.js')}}"></script>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script> -->

    <script src="{{asset('plugin/pnotify/pnotify.custom.min.js')}}"></script>

    <!-- prueba diana  form validation -->
    <!--<link rel="stylesheet" href="{{asset('plugin/formvalidation//bootstrap.min.css')}}">
    <script src="{{asset('plugin/formvalidation/formValidation.min.js')}}"></script>
    <script src="{{asset('plugin/formvalidation/bootstrap.validation.min.js')}}"></script>
    <script src="{{asset('plugin/formvalidation/bootstrap.min.js')}}"></script>-->

  <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script> -->
  <script src="{{asset('js/jquery.min.js')}}"></script>
  <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script> -->
  <script src="{{asset('js/popper.min.js')}}"></script>
  <!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"></script> -->
  <script src="{{asset('js/bootstrap.min.js')}}"></script>

  <script src="{{asset('js/datatables.min.js')}}"></script>
  
  <script src="{{asset('js/helperdrea.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/jquery.validate.js')}}"></script>
  <script type="text/javascript" src="{{asset('js/additional-methods.min.js')}}"></script>
<!-- ------------------------------------------------------------------------------------------- -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script> -->
<!-- ------------------------------------------------------------------------------------------- -->
</head>
<body class="hold-transition sidebar-mini layout-fixed">
<!-- START NAVBAR -->
<nav id="navbar-example2" class="navbar navbar-light bg-light fixed-top" style="padding: 5px 50px;">

    <a class="navbar-brand p-0 col-lg-5" href="{{url('/')}}">
        <img src="{{asset('assets/img/logo-gore.png')}}" class="img-responsive center-block" alt="logo" style="width: 5%;margin: 0;">PLATAFORMA EDUCATIVA DE APURÍMAC
    </a>
    <ul class="nav nav-pills">
        <!-- <li class="nav-item">
            <a class="btn btn-sm btn-primary py-0 mr-1" href="{{url('materialEducativo/mostrar')}}"><i class="fa fa-file"></i> Material educativo(OCTUBRE)</a>
        </li>
        <li class="nav-item">
            <a class="btn btn-sm btn-primary py-0 mr-1" href="{{url('materialEducativo2/mostrar2')}}"><i class="fa fa-file"></i> Material educativo(NOVIEMBRE)</a>
        </li>
        <li class="nav-item">
            <a class="btn btn-sm btn-primary py-0 mr-1" href="{{url('materialEducativo3/mostrar3')}}"><i class="fa fa-file"></i> Material educativo(DICIEMBRE)</a>
        </li> -->
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Material educativo 2020</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{url('materialEducativo/mostrar')}}"><i class="fa fa-file"></i> Octubre</a>
                <div role="separator" class="dropdown-divider m-0"></div>
                <a class="dropdown-item" href="{{url('materialEducativo2/mostrar2')}}"><i class="fa fa-file"></i> Noviembre</a>
                <div role="separator" class="dropdown-divider m-0"></div>
                <a class="dropdown-item" href="{{url('materialEducativo3/mostrar3')}}"><i class="fa fa-file"></i> Diciembre</a>
            </div>
        </li>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Material educativo 2021</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="{{url('raPortal/listarInicial')}}"><i class="fa fa-file"></i> Inicial</a>
                <div role="separator" class="dropdown-divider m-0"></div>
                <a class="dropdown-item" href="{{url('raPortal/listarPrimaria')}}"><i class="fa fa-file"></i> Primaria</a>
                <div role="separator" class="dropdown-divider m-0"></div>
                <a class="dropdown-item" href="{{url('raPortal/listarSecundaria')}}"><i class="fa fa-file"></i> Secundaria</a>
            </div>
        </li>

        <!-- <li class="nav-item">
            <a class="nav-link" href="#mdo">@mdo</a>
        </li> -->
        <!-- <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">Dropdown</a>
            <div class="dropdown-menu">
                <a class="dropdown-item" href="#one">one</a>
                <a class="dropdown-item" href="#two">two</a>
                <div role="separator" class="dropdown-divider"></div>
                <a class="dropdown-item" href="#three">three</a>
            </div>
        </li> -->
    </ul>
</nav>
<!-- END NAVBAR -->
<!-- <img src="{{asset('assets/img/slider1.jpg')}}" width="100%"> -->
<div class="text-center w-100 mt-4 pt-4" style="background-image: url('{{asset('assets/img/slider1.jpg')}}');height: 150px;background-size: cover;">
    <!-- <p style="padding-top: 150px;">
        <img src="{{asset('imagen/drea-apurimac.png')}}" class="img-responsive center-block" alt="logo" style="width: 20%;margin: auto;">
    </p> -->
    <div class="container mt-4" style="color: #232c35;">
        <h1 class="titulo-banner">Materiales Educativos</h1>
        <p class="descripcion-banner"></p>
    </div>
    <!-- <div class="container text-center">
        <a href="{{$v=asset('manuales/manual_director.pdf')}}" class="btn btn-sm btn-success" target="blank"><i class="fa fa-file"></i> MANUAL DE DIRECTOR</a>
        <a href="{{$v=asset('manuales/manual_docente.pdf')}}" class="btn btn-sm btn-success" target="blank"><i class="fa fa-file"></i> MANUAL DE DOCENTE</a>
    </div> -->
    <!-- <div class="container text-center mt-4">
        <a href="{{url('user/login')}}" class="btn btn-sm btn-success"><i class="fa fa-arrow-right"></i> INICIAR SESION</a>
    </div> -->
</div>
@yield('generalBodyPortal')
 <!-- @include('sweet::alert') -->
 <!-- START FOOTER -->
 <footer class="footer mt-auto">
  <div class="container-fluid">
    <div class="row">
      <!-- <div class="col-lg-3 text-center"><img src="{{asset('imgSocio/minedu.png')}}" class="w-75"></div>
      <div class="col-lg-3 text-center"><img src="{{asset('imgSocio/logo-gore.png')}}" class="w-25"></div>
      <div class="col-lg-3 text-center"><img src="{{asset('imgSocio/logo-blanco-sombra.png')}}" class="w-50"></div>
      <div class="col-lg-3 text-center"><img src="{{asset('imgSocio/bicentenario.png')}}" class="w-100"></div> -->
      <div class="col-lg-12 p-0"><img src="{{asset('imgSocio/todo.png')}}" class="w-100"></div>
      <div class="col-lg-12 bg-danger"><img src="{{asset('imgSocio/cinta-andina.png')}}" class="w-100"></div>
    </div>
  </div>
</footer>
<!-- <nav class="hidden-xs hidden-sm navbar footer-nav" role="navigation" style="background: #2d2d2d;">
    <div class="container">
        <div class="navbar-header">
            <div class="navbar-brand">
                <span class="sr-only">©THEMEWAGON</span>
                <a href="#" style="color:#9399a0">©DREA</a>
            </div>
            </div>
            <div class="collapse navbar-collapse" id="main-nav-collapse">
                <ul class="nav navbar-nav navbar-right text-capitalize">
                    <li><a href="#"><span>--</span></a></li>
                    <li><a href="#services"><span>--</span></a></li>
                    <li><a href="#portfolio"><span>--</span></a></li>
                    <li><a href="#team"><span>--</span></a></li>
                    <li><a href="#pricing"><span>--</span></a></li>
                    <li><a href="#blog"><span>--</span></a></li>
                    <li><a href="#contact"><span>--</span></a></li>
                </ul>
            </div>
    </div>
</nav> -->

<!-- <footer class="footer mt-auto py-3 bg-danger">
  <div class="container">
    <span class="text-muted">Place sticky footer content here.</span>
  </div>
</footer> -->
 <!-- END FOOTER -->
  
<!-- jQuery UI 1.11.4 -->
<script src="{{asset('plugin/adminlte/plugins/jquery-ui/jquery-ui.min.js')}}"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{asset('plugin/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js')}}"></script>
<!-- ChartJS -->
<script src="{{asset('plugin/adminlte/plugins/chart.js/Chart.min.js')}}"></script>
<!-- Sparkline -->
<script src="{{asset('plugin/adminlte/plugins/sparklines/sparkline.js')}}"></script>
<!-- JQVMap -->

<!--<script src="{{asset('plugin/adminlte/plugins/jqvmap/jquery.vmap.min.js')}}"></script>
<script src="{{asset('plugin/adminlte/plugins/jqvmap/maps/jquery.vmap.usa.js')}}"></script>-->
<!-- Select2 -->
<!-- <script src="{{asset('plugin/adminlte/plugins/select2/js/select2.full.min.js')}}"></script> -->
<script src="{{asset('/js/select2.min.js')}}"></script>
<!-- jQuery Knob Chart -->
<script src="{{asset('plugin/adminlte/plugins/jquery-knob/jquery.knob.min.js')}}"></script>
<!-- daterangepicker -->
<script src="{{asset('plugin/adminlte/plugins/moment/moment.min.js')}}"></script>
<script src="{{asset('plugin/adminlte/plugins/daterangepicker/daterangepicker.js')}}"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{asset('plugin/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js')}}"></script>
<!-- Summernote -->
<script src="{{asset('plugin/adminlte/plugins/summernote/summernote-bs4.min.js')}}"></script>
<!-- overlayScrollbars -->
<script src="{{asset('plugin/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js')}}"></script>
<!-- AdminLTE App -->
<script src="{{asset('plugin/adminlte/dist/js/adminlte.js')}}"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{asset('plugin/adminlte/dist/js/pages/dashboard.js')}}"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{asset('plugin/adminlte/dist/js/demo.js')}}"></script>
<script>
 $(document).ready(function(){
  $('[data-toggle="tooltip"]').tooltip();   
});

    //Initialize Select2 Elements
    // $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })


/*$('.select').select2(
{
    language:
    {
        noResults: function()
        {
            return "No se encontraron resultados.";        
        },
        searching: function()
        {
            return "Buscando...";
        },
        inputTooShort: function()
        { 
            return 'Por favor ingrese 3 o más caracteres';
        }
    },
    placeholder: 'Buscar...'
});*/

</script>
<!-- --------------------------------- -->
<!-- <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.1/dist/umd/popper.min.js" integrity="sha384-9/reFTGAW83EW2RDu2S0VKaIzap3H66lZH81PoYlFhbGU+6BZp6G7niu735Sk7lN" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.min.js" integrity="sha384-+YQ4JLhjyBLPDQt//I+STsc9iw4uQqACwlvpslubQzn4u2UU2UFM80nGisd026JF" crossorigin="anonymous"></script> -->
<!-- --------------------------------- -->
</body>
</html>
