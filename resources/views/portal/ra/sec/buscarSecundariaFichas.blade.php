<div class="row buscarSecundariaFichas">
	<div class="col-lg-12">
		<div class="card">
			<div class="card-body">
				<div class="row">
					<div class="col-lg-12">
						<h2 class="text-center text-primary">Experiencia de aprendizaje "{{$tExperiencias->nombre}}"</h2>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-3">
				<div class="card card-danger card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-danger">Ciencias Sociales</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCs)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCs as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/{{$item->archivoPdf}} -->
								<!-- {{asset('filera/sec/doc').'/'.$item->archivoDoc}} -->
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-info card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-info">Comunicación</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaComu)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaComu as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-success card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-success">Matemática</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaMate)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaMate as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-warning card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-warning">Ciencia y Tecnología</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCyt)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCyt as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
