<div class="row buscarInicial">
	<div class="col-lg-12">
		<h3 class="text-center mb-4">5 AÑOS</h3>
	</div>
	<div class="col-lg-12">
		<h3>EXPERIENCIAS DE APRENDIZAJE</h3>
	</div>
	<div class="col-md-12">
		<div class="card card-primary card-outline">
			<div class="card-body py-2 px-0">
				<table id="example1" class="table table-sm table-hover m-0 w-100">
	                <thead class="text-center">
	                    <tr>
	                        <th>Nombre</th>
	                        <th>Descripcion</th>
	                        <th>Archivo pdf</th>
	                        <th>Archivo word</th>
	                        <th>Enlaces</th>
	                        <th>Opciones</th>
	                    </tr>
	                </thead>
	                <tbody>
	                    @foreach($listEa as $item)
	                    <tr class="text-center">
	                        <td>{{$item->nombre}}</td>
	                        <td>{{$item->descripcion}}</td>
	                        <td>{{$item->archivoPdf}}</td>
	                        <td>{{$item->archivoDoc}}</td>
	                        <td>
	                            @if($item->earchivo!='')
	                                <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
	                            @endif
	                            @if($item->evideo!='')
	                                <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
	                            @endif
	                            @if($item->eaudio!='')
	                                <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
	                            @endif
	                        </td>
	                        <td>
	                        	<!-- http://pcr.dreapurimac.gob.pe/laravel/public/fileea/pri/doc/{{$item->archivoDoc}} -->
	                            @if($item->archivoPdf!='')
	                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
	                            @endif
	                            @if($item->archivoDoc!='')
	                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
	                            @endif
	                        </td>
	                    </tr>
	                    @endforeach
	                </tbody>
	            </table>
			</div>
		</div>
	</div>
	<div class="col-lg-12">
		<h3>FICHAS DE AUTOAPRENDIZAJE</h3>
	</div>
	<div class="col-lg-12">
		<div class="row justify-content-center align-items-center">
			<div class="col-lg-3">
				<div class="card card-primary card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-danger">Fichas</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listAa)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCs as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/fileea/sec/pdf/{{$item->archivoPdf}} -->
								<!-- {{asset('filera/sec/doc').'/'.$item->archivoDoc}} -->
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/sec/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
        $('.pagination').addClass('m-0');
    } );
</script>