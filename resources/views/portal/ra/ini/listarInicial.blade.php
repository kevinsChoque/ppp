@extends('template/templatePortal')
@section('generalBodyPortal')
<div class="container-fluid mt-3" style="padding: 0px 50px;">
	<div class="row justify-content-center align-items-center">
		<div class="col-md-3">
            <div class="form-group mb-2">
                <label class="m-0">Periodo de vigencia:</label>
                <select name="mes" id="mes" class="form-control form-control-sm">
                    <option disabled="" selected="">Elija el periodo:</option>
                    @foreach($listMes as $item)
                    <option value="{{$item->mes}}">Del 24 de mayo al 18 de junio</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group mb-2">
                <label class="m-0" style="visibility: hidden;">Periodo de vigencia:</label>
                    <a href="#" class="form-control form-control-sm btn btn-success btn-sm buscar"><i class="fa fa-search"></i> Buscar</a>
            </div>
        </div>
        <div class="col-lg-12">
			<p class="text-center">Experiencias de aprendizaje articuladas por grado a desarrollarse entre el <strong>24 de mayo y el 18 de junio 2021</strong></p>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="card">
				<div class="card-body">
					<div class="row">
						<div class="col-lg-12">
							<h2 class="text-center mensaje">Elija el mes para poder acceder a las experiencias de aprendizaje y fichas de autoaprendizaje</h2>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="row contenedorBuscarIni">
		
	</div>
</div>

<script>

	$('.titulo-banner').html('Materiales Educativos 2021 - INICIAL');
	$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
	$('body').css('background-repeat','round');
    $('.buscar').on('click',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/raPortal/buscarInicial') }}",
            data: {mes:$('#mes').val()},
            method: 'get',
            success: function(result){
                console.log(result);
                if(result.data==false)
                {
                	alert('Ingrese los datos');
                }
                else
                {
                	$('.buscarInicial').remove();
                	$('.contenedorBuscarIni').append(result);
                	$('.mensaje').html('Material de aprendizaje del mes de '+$('#mes').val());
                	$('.mensaje').addClass('text-primary');
                }
            }
        });
    });
</script>
@endsection
