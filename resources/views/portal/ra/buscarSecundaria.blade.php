<div class="row buscarSecundaria">
	<div class="col-lg-12">
		@if(COUNT($listEa)=='1')
		<h3 class="text-center mb-4" style="text-decoration: underline;"><a href="{{asset('filera/sec/pdf').'/'.$listEa[0]->archivoPdf}}" target="_blank">{{$listEa[0]->grado}} GRADO : {{$listEa[0]->nombre}}</a></h3>
		@else
		<h3 class="text-center mb-4">{{$grado}} GRADO</h3>
		@endif
	</div>
	@if(COUNT($listEa)!='1')
		@if(COUNT($listEa)!='0')
		<div class="col-lg-12">
			<h3>EXPERIENCIAS DE APRENDIZAJE</h3>
		</div>
		<div class="col-md-12">
			<div class="card card-primary card-outline">
				<div class="card-body py-2 px-0">
					<table id="example1" class="table table-sm table-hover m-0 w-100">
		                <thead class="text-center">
		                    <tr>
		                        <th>Tipo de material</th>
		                        <th>Nombre</th>
		                        <th>Descripcion</th>
		                        <th>Archivo pdf</th>
		                        <th>Archivo word</th>
		                        <th>Enlaces</th>
		                        <th>Opciones</th>
		                    </tr>
		                </thead>
		                <tbody>
		                    @foreach($listEa as $item)
		                    <tr class="text-center">
		                        <td>{{$item->tipoMaterial=='expApr'?'Experiencias de aprendizaje':'Fichas de autoaprendizaje'}}</td>
		                        <td>{{$item->nombre}}</td>
		                        <td>{{$item->descripcion}}</td>
		                        <td>{{$item->archivoPdf}}</td>
		                        <td>{{$item->archivoDoc}}</td>
		                        <td>
		                            @if($item->earchivo!='')
		                                <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
		                            @endif
		                            @if($item->evideo!='')
		                                <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
		                            @endif
		                            @if($item->eaudio!='')
		                                <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
		                            @endif
		                        </td>
		                        <td>
		                            @if($item->archivoPdf!='')
		                                <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fa fa-download"></i><!--  Descargar --></a>
		                            @endif
		                        </td>
		                    </tr>
		                    @endforeach
		                </tbody>
		            </table>
				</div>
			</div>
		</div>
		@endif
	@endif
	<div class="col-lg-12">
		<h3>FICHAS DE AUTOAPRENDIZAJE</h3>
	</div>
	<div class="col-lg-12">
		<div class="row">
			<div class="col-lg-3">
				<div class="card card-danger card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-danger">Ciencias Sociales</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCs)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCs as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}} -->
								@if($item->archivoDoc!='')
								<a href="{{asset('filera/sec/doc').'/'.$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="{{asset('filera/sec/pdf').'/'.$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-info card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-info">Comunicación</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaComu)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaComu as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}} -->
								@if($item->archivoDoc!='')
								<a href="{{asset('filera/sec/doc').'/'.$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="{{asset('filera/sec/pdf').'/'.$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-success card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-success">Matemática</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaMate)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaMate as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}} -->
								@if($item->archivoDoc!='')
								<a href="{{asset('filera/sec/doc').'/'.$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="{{asset('filera/sec/pdf').'/'.$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
			<div class="col-lg-3">
				<div class="card card-warning card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-warning">Ciencia y Tecnología</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($listFaCyt)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($listFaCyt as $item)
							<p class="mb-1">{{$item->orden}} Autoaprendizaje |
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}} -->
								@if($item->archivoDoc!='')
								<a href="{{asset('filera/sec/doc').'/'.$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="{{asset('filera/sec/pdf').'/'.$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<script>
	$(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
        $('.pagination').addClass('m-0');
    } );
</script>