@extends('template/templatePortal')
@section('generalBodyPortal')
<div class="container-fluid mt-3" style="padding: 0px 50px;">
	<div class="row justify-content-center align-items-center">
		<div class="col-lg-12">
			<h4 class="text-center">CPAVA SECUNDARIA</h4>
		</div>
		<div class="col-lg-12">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>ÁREA</th>
                        <th>PARTICIPANTES</th>
                        <th>ELABORARON</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                		<td>CC.SS</td>
                		<td>80</td>
                		<td>22</td>
                	</tr>
                	<tr>
                		<td>MATEMATICA</td>
                		<td>45</td>
                		<td>17</td>
                	</tr>
                	<tr>
                		<td>COMUNICACION</td>
                		<td>80</td>
                		<td>27</td>
                	</tr>
                	<tr>
                		<td>CIENCIA Y TECNOLOGIA</td>
                		<td>100</td>
                		<td>28</td>
                	</tr>
                </tbody>
            </table>
		</div>
		<div class="col-lg-12">
			<h5 class="text-center">Docentes de CCSS participantes en la Comunidad Profesional (elaboraron las fichas de autoaprendizaje_EA 1)
			</h5>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>IIEE</th>
                        <th>Provincia</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                		<td>Basilia Gómez Ccahua</td>
                		<td>Antonio Ocampo</td>
                		<td>Curahuasi- Abancay</td>
                	</tr>
                	<tr>
                		<td>Jasmine Pérez Urrutia</td>
                		<td>José Abelardo Quiñones</td>
                		<td>Huancarama</td>
                	</tr>
                	<tr>
                		<td>Cirilo Pacco Peñalva.</td>
                		<td>Nueva Fuerabamba</td>
                		<td>Distrito Challhuahuacho, Provincia Cotabambas.</td>
                	</tr>
                	<tr>
                		<td>Jaime Isidoro Alférez Chahua</td>
                		<td>Virgen de Chapi</td>
                		<td>Haquira - Cotabambas</td>
                	</tr>
                	<tr>
                		<td>Rocsana Beatriz Corimanya Barriga</td>
                		<td>Giraldo Contreras Trujillo</td>
                		<td>Antabamba</td>
                	</tr>
                	<tr>
                		<td>Hermitaño Soria Galindo</td>
                		<td>Ccasacancha</td>
                		<td>Tambobamba-Cotabambas</td>
                	</tr>
                	<tr>
                		<td>Celestino Gonzales Concha</td>
                		<td>Daniel Estrada Pérez  de San Juan de juta</td>
                		<td>Lucre -Aymaraes.</td>
                	</tr>
                	<tr>
                		<td>Olga Colque Mollo</td>
                		<td>Erasmo Delgado Vivanco</td>
                		<td>Tambobamba- Cotabambas</td>
                	</tr>
                	<tr>
                		<td>Nora Álvarez Canal</td>
                		<td>Erasmo Delgado Vivanco</td>
                		<td>Tambobamba- Cotabambas</td>
                	</tr>
					<tr>
						<td>Mónica Quispitupa Cahuana</td>
						<td>Víctor Acosta Ríos de Concacha</td>
						<td>Abancay</td>
					</tr>
					<tr>
						<td>Andrés Salas Castillo</td>
						<td>--</td>
						<td>--</td>
					</tr>
                </tbody>
            </table>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>IIEE</th>
                        <th>Provincia</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td>Moisés Wilfredo Quispe Condori</td>
						<td>Santa Catalina</td>
						<td>Chapimarca -Aymaraes</td>
					</tr>
					<tr>
						<td>Mercedes Vargas Contreras</td>
						<td>Mosoq Illaryq</td>
						<td>Iscahuaca Aymaraes</td>
					</tr>
					<tr>
						<td>Nilo Hilarión Caballero Sequeiros</td>
						<td>Monseñor Renzo Micchelli O.S.A</td>
						<td>Grau</td>
					</tr>
					<tr>
						<td>Nelly Oblitas Barra	Monseñor</td>
						<td>Renzo Micchelli O.S.A</td>
						<td>Grau</td>
					</tr>
					<tr>
						<td>Laureano Espinoza Vásquez </td>
						<td>Santa Catalina</td>
						<td>Aymaraes</td>
					</tr>
					<tr>
						<td>Santiago  Ustua Ancco</td>
						<td>José María Arguedas	</td>
						<td>Huanipaca-Abancay</td>
					</tr>
					<tr>
						<td>Sabino Samata Ccansaya</td>
						<td>Occaccahua</td>
						<td>Tambobamba-Cotabambas</td>
					</tr>
					<tr>
						<td>Sadith Valenzuela Tapia</td>
						<td>José Antonio Encinas</td>
						<td>Oropesa- Antabamba</td>
					</tr>
					<tr>
						<td>María Armida Vera Ypenza</td>
						<td>Quisapata</td>
						<td>Abancay</td>
					</tr>
					<tr>
						<td>Jorge Huamani Huamani</td>
						<td>CRFA "Nuestra Señora DE Cocharcas</td>
						<td>Chincheros</td>
					</tr>
					<tr>
						<td>Jesús Rodríguez Fernández</td>
						<td>Jose Antonio Encinas</td>
						<td>Oropesa- Antabamba</td>
					</tr>

                </tbody>
            </table>
		</div>
		<div class="col-lg-12">
			<h5 class="text-center">Docentes de Ciencia y Tecnología participantes en la Comunidad Profesional Elaboraron las fichas de autoaprendizaje correspondientes a la EA 1
			</h5>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>DNI</th>
                    </tr>
                </thead>
                <tbody>
                	<tr>
                		<td>Verónica Aguilar Enriquez</td> 	
                		<td>29735087</td>
                	</tr>
					<tr>
						<td>Leonor Alejandrina Portugal Arias</td>
						<td>29473938</td>
					</tr>
					<tr>
						<td>Carmen Rosa Arone Lovón	</td>
						<td>31535797</td>
					</tr>
					<tr>
						<td>Luz Florencia Palomino Gómez</td>
						<td>08819685</td>
					</tr>
					<tr>
						<td>Edith Nancy Mamani Pariapaza</td>
						<td>41051114</td>
					</tr>
					<tr>
						<td>Elie Pumacayo Rodriguez</td>
						<td>31189252</td>
					</tr>
					<tr>
						<td>Paz Atilio Cornejo Pinto </td>
						<td>23882108</td>
					</tr>
					<tr>
						<td>Inés Nelly Huamani Cahuana</td>
						<td>29723670</td>
					</tr>
					<tr>
						<td>Indira Yomali Grovas Macedo</td>
						<td>72187270</td>
					</tr>
					<tr>
						<td>Maria Auccacusi Cuadros</td>
						<td>23832780</td>
					</tr>
					<tr>
						<td>Martha Mamani Choquehuanca</td>
						<td>42152634</td>
					</tr>
					<tr>
						<td>Marilin rancisca Salas Conde</td>
						<td>40467839</td>
					</tr>
					<tr>
						<td>Willy Gonzalo Mendoza Chumbes</td>
						<td>70475193</td>
					</tr>
					<tr>
						<td>Mary Doris Vivanco Contreras</td>
						<td>31340132</td>
					</tr>
					

                	
                </tbody>
            </table>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>DNI</th>
                    </tr>
                </thead>
                <tbody>
                	
					<tr>
						<td>Yvony Casaverde Moina</td>
						<td>31541301</td>
					</tr>
					<tr>
						<td>Elizabeth Luisa Evanan Huamani</td>
						<td>21552651</td>
					</tr>
					<tr>
						<td>Milagros Ortiz Zavala</td>
						<td>40467838</td>
					</tr>
					<tr>
						<td>Loyola Caman Vin</td>
						<td>22091263</td>
					</tr>
					<tr>
						<td>Sonia Kari Ferro</td>
						<td>40486197</td>
					</tr>
					<tr>
						<td>Raul Valderrama Condori</td>
						<td>31041010</td>
					</tr>
					<tr>
						<td>Miguel Angel Bustinza Choquehuanca</td>
						<td>40050070</td>
					</tr>
					<tr>
						<td>Eliana Elva Quispe Mendoza</td>
						<td>41087238</td>
					</tr>
					<tr>
						<td>Jorge Luis Limascca Bautista</td>
						<td>42106424</td>
					</tr>
					<tr>
						<td>Simión Torres Carbajal</td>
						<td>31541900</td>
					</tr>
					<tr>
						<td>Edith Durand Ticona</td>
						<td>40436265</td>
					</tr>
					<tr>
						<td>Magda Huahuasoncco Ramos</td>
						<td>31044081</td>
					</tr>
					<tr>
						<td>Maria Antonieta de la Torre Baca</td>
						<td>24388881</td>
					</tr>
					<tr>
						<td>Yurika Ascarza Sotomayor</td>
						<td>31031550</td>
					</tr>
                </tbody>
            </table>
		</div>
		<div class="col-lg-12">
			<h5 class="text-center">Docentes de Matemática participantes en la Comunidad Profesional (elaboraron las fichas de autoaprendizaje EA 1)
			</h5>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>IE</th>
                        <th>Provincia</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td>JORGE AMERICO ESPINOZA ROJAS</td>
						<td>FRANCISCO BOLOGNESI</td>
						<td>ABANCAY</td>
					</tr>
					<tr>
						<td>EMILIA CURI ASTOCAHUANA</td>
						<td>LA VICTORIA</td>
						<td>ABANCAY</td>
					</tr>
					<tr>
						<td>ERIKA MOLINA RAMIREZ</td>
						<td>JOSE CARLOS MARIATEGUI</td>
						<td>ANTABAMBA</td>
					</tr>
					<tr>
						<td>YOVANA TACURI HUALPA</td>
						<td>MICAELA BASTIDAS</td>
						<td>GRAU</td>
					</tr>
					<tr>
						<td>Nancy Damiana Elguera Vargas</td>
						<td>Micaela Bastidas</td>
						<td>Chuquibambilla-Grau</td>
					</tr>
					<tr>
						<td>Joao Morocco Ramos</td>
						<td>IE SAN LUIS</td>
						<td>Abancay</td>
					</tr>
					<tr>
						<td>Maryluz Pumacayo Rodríguez</td>
						<td>Tupac Amaru II</td>
						<td>San Antonio-Grau</td>
					</tr>
					<tr>
						<td>José Recalde Tuero</td>
						<td>San Agustín </td>
						<td>Curpahuasi-Grau</td>
					</tr>
					<tr>
						<td>Jaime Soto Pérez</td>
						<td>Renzo Michelli</td>
						<td>Chuquibambilla- Grau</td>
					</tr>
					<tr>
						<td>Jhon Arturo Valenzuela Barreto	</td>
						<td>José María Arguedas	</td>
						<td>Chuquibambilla -Grau</td>
					</tr>
					<tr>
						<td>Rogelio Quispe Paucar</td>
						<td>Erasmo Delgado Vivanco</td>
						<td>Tambobamba¬-Cotabambas</td>
					</tr>
					<tr>
						<td>Pedro John Ccahua Vivanco</td>
						<td>Erasmo Delgado Vivanco</td>
						<td>Tambobamba¬-Cotabambas</td>
					</tr>
					<tr>
						<td>Melesio Carlo Medina Salazar</td>
						<td>Tupac Amaru</td>
						<td>Chincheros</td>
					</tr>
					<tr>
						<td>Tomas Soncco Mamani</td>
						<td>Inca Garcilaso de la Vega</td>
						<td>Uranmarca-Chincheros</td>
					</tr>
					<tr>
						<td>Julio César Barrientos Quispe</td>
						<td>“José Antonio encinas”</td>
						<td>Aymaraes</td>
					</tr>
					
                </tbody>
            </table>
		</div>
		<div class="col-lg-6">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                        <th>Nombres</th>
                        <th>IE</th>
                        <th>Provincia</th>
                    </tr>
                </thead>
                <tbody>
					
					<tr>
						<td>Gabriel Baldomero Barrientos Barrientos</td>
						<td>“Daniel Estrada Pérez”</td>
						<td>Aymaraes</td>
					</tr>
					<tr>
						<td>Gaspar Huaylla Valverde </td>
						<td>“Antonio Raymondi”</td>
						<td>Antabamba</td>
					</tr>
					<tr>
						<td>Oscar Bravo Carbajal </td>
						<td>“Manuel Gonzáles Prada”</td>
						<td>Antabamba</td>
					</tr>
					<tr>
						<td>Edgar Castillo Huillca </td>
						<td>Labora en la UGEL</td>
						<td>Grau</td>
					</tr>
					<tr>
						<td>Aurora Inés Sánchez Valenzuela</td>
						<td>Micaela Bastidas</td>
						<td>Grau</td>
					</tr>
					<tr>
						<td>Nélida Quenta Paco</td>
						<td>José María Flores</td>
						<td>Chincheros</td>
					</tr>
					<tr>
						<td>Maribel Arias Moriano</td>
						<td>Divino Maestro</td>
						<td>Huancarama</td>
					</tr>
					<tr>
						<td>Elizabeth Oyardo Huayca</td>
						<td>José Carlos Mariátegui</td>
						<td>Antabamba</td>
					</tr>
					<tr>
						<td>Luz María Dávila Portocarrero</td>
						<td>José María Arguedas</td>
						<td>Antabamba</td>
					</tr>
					<tr>
						<td>Rocio Farfán Dávalos</td>
						<td>Libertadores de América</td>
						<td>Aymaraes</td>
					</tr>
					<tr>
						<td>Ada Yaneth Jalixto Challco</td>
						<td>Divino Maestro</td>
						<td>Huancarama</td>
					</tr>
					<tr>
						<td>José Recalde Tuero</td>
						<td>San Agustín	</td>
					</tr>
					<tr>
						<td>Climaco Eccoña Torres</td>
						<td>Fernando Belaunde Terry</td>
						<td>Chincheros</td>
					</tr>
					<tr>
						<td>Nancy Judith Mallma Pinares</td>
						<td>Andrés Avelino Cáceres</td>
						<td>Aymaraes</td>
					</tr>
                </tbody>
            </table>
		</div>
		<div class="col-lg-12">
			<h5 class="text-center">RELACIÓN DE DOCENTES QUE ELABORARON FICHAS DE COMUNICACIÓN
			</h5>
		</div>
		<div class="col-lg-12">
			<table id="example1" class="table table-sm table-hover m-0 w-100">
                <thead class="text-center">
                    <tr>
                    	<th>Ficha</th>
                    	<th>Ugel</th>
                    	<th>Grado</th>
                        <th>Nombres</th>
                        <th>DNI</th>
                        <th>Celular</th>
                        <th>Correo</th>
                    </tr>
                </thead>
                <tbody>
					<tr>
						<td>Ficha 01</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Edith Gaby Cancino Apaza</td>
						<td>40817672</td>
						<td>989520763</td>
						<td>celula12151@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 01</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Rimberti Eddy Suri Quispe </td>
						<td>45504505</td>
						<td>954404477</td>
						<td>eddysq7@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 02</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Norma Mercedes Chacnama Zea </td>
						<td>29732669</td>
						<td>999542230</td>
						<td>Normis-811@hotmail,com</td>
					</tr>
					<tr>
						<td>Ficha 02</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Dina Espinoza Yabar </td>
						<td>23946852</td>
						<td>974287751</td>
						<td>na.di.espinoza@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Mariluz Canaza Zamata</td>
						<td>29642861</td>
						<td>992529981</td>
						<td>malucaza01@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Yeni Ñaccha Gutierrez</td>
						<td>23958304</td>
						<td>984208836</td>
						<td>nacchagutierrez@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Flora Challco Mendoza.</td>
						<td>23815745</td>
						<td>958340858</td>
						<td>floraineschallco@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Cotabambas</td>
						<td>1º grado</td>
						<td>Josue Benites Valer </td>
						<td>43432425</td>
						<td>980264788</td>
						<td>jhosuep004@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 01</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>DUEÑAS VALENZUELA, Ruth Nancy</td>
						<td>31545798</td>
						<td>982119266</td>
						<td>ruthnancy_29@hotmail.com</td>
					</tr>
					<tr>
						<td>Ficha 02</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>HURTADO OCHUPE, Brisney</td>
						<td>71938599</td>
						<td>990914131</td>
						<td>brisneyhurtado2015@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 02</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>PUMA CANDIA, Bertha</td>
						<td>47881442</td>
						<td>955 894 721</td>
						<td>e.b.puma@gmendel.edu.pe</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>HUAMANÑAHUI SÁNCHEZ, Juan Carlos </td>
						<td>31020454</td>
						<td>974 508 337</td>
						<td>sanchezhuamannahui@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>CCASA QUISPE, Yaneth</td>
						<td>44606119</td>
						<td>983 844 623</td>
						<td>yanezoiv21@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 05</td>
						<td>Grau</td>
						<td>2º grado</td>
						<td>RABANAL HOLGUÍN, Jesús Milena</td>
						<td>18182225</td>
						<td>974 872 222</td>
						<td>rabanal.jmilena@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 01</td>
						<td>Aymaraes</td>
						<td>3º grado</td>
						<td>WEECKY SOFÍA WARTHON SALGADO</td>
						<td>31543028</td>
						<td>941036145</td>
						<td>sofiawarthons@hotmail.com</td>
					</tr>
					<tr>
						<td>Ficha 02</td>
						<td>Aymaraes</td>
						<td>3º grado</td>
						<td>ROEMER HUAMANI MENDOZA</td>
						<td>80126192</td>
						<td>979909016</td>
						<td>roemerhuamani28@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Huancarama</td>
						<td>3º grado</td>
						<td>Mirian Beatriz Rivera Alave</td>
						<td>47608497</td>
						<td>952701544</td>
						<td>miberial_20@hotmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Huancarama</td>
						<td>3º grado</td>
						<td>Marilu Chirinos Matencio</td>
						<td>31037843</td>
						<td>983712000</td>
						<td>yanet.chirinosm@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Huancarama</td>
						<td>3º grado</td>
						<td>Julio Rosmel Flores Huacho</td>
						<td>40363643</td>
						<td>983724249</td>
						<td>florese1@hotmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Huancarama</td>
						<td>3º grado</td>
						<td>Marilu Chirinos Matencio</td>
						<td>31037843</td>
						<td></td>
						<td>yanet.chirinosm@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 01 y 02</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Yanet BAUTISTA HUILLCA</td>
						<td>40782288</td>
						<td>994712930</td>
						<td>yanelybautista19@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 01 y 02</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Marcelina AYALA NAVEDOS</td>
						<td>31042331</td>
						<td>983790581</td>
						<td>Ayalanadedos09@gmail.com</td>
					</tr>
					<tr>
						<td></td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>SHARY SAHUARAURA SEGOVIA</td>
						<td>40280272</td>
						<td>984786352</td>
						<td>sharysahuaraurasegovia@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Erna IPENZA VALDIGLESIAS</td>
						<td>31032121</td>
						<td>983610225</td>
						<td>ernaipenzav1224@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 03</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Jorge Luis DURAN LÓPEZ</td>
						<td>44764870</td>
						<td>999781422</td>
						<td>JORGDURANLOPEZ@GMAIL.COM</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Aida Clorinda ÑAHUI CRUSINTA</td>
						<td>31011441</td>
						<td>992591044</td>
						<td>crusintaida@gmail.com</td>
					</tr>
					<tr>
						<td>Ficha 04</td>
						<td>Abancay</td>
						<td>5º grado</td>
						<td>Hipólito ÁLVIZ RAYÁN</td>
						<td>31032447</td>
						<td>983619380</td>
						<td>alvizrh@hotmail.com</td>
					</tr>
                </tbody>
            </table>
		</div>
	</div>
</div>

<script>
	$('.titulo-banner').html('Materiales Educativos 2021 - PRESENTACIÒN');
	$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
	$('body').css('background-repeat','round');
</script>
@endsection
