<div class="col-lg-12">
		<h3>FICHAS DE AUTOAPRENDIZAJE</h3>
	</div>
	<div class="col-lg-12">
		<div class="row justify-content-center align-items-center">
			<div class="col-lg-3">
				<div class="card card-primary card-outline">
					<div class="card-header py-1">
						<p class="m-0 font-weight-bold text-center text-warning">{{$grado}}ª GRADO</p>
					</div>
					<div class="card-body py-1">
						@if(COUNT($grado1)=='0')
						<p class="m-0">Sin materiales</p>
						@else
							@foreach($grado1 as $item)
							<p class="mb-1">{{$categoria=='cat5'?
								'Ficha #'.$item->orden.'-'.$item->curso:$item->orden.' Autoaprendizaje'}}|
								<!-- http://pcr.dreapurimac.gob.pe/laravel/public/filera/pri/pdf/{{$item->archivoPdf}} -->
								<!-- {{asset('filera/sec/doc').'/'.$item->archivoDoc}} -->
								@if($item->archivoDoc!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/pri/doc/{{$item->archivoDoc}}" target="_blank" class="btn text-light bg-primary btn-xs" title="Descargar"><i class="fas fa-file-word fa-2x"></i></a>
								@endif
								@if($item->archivoPdf!='')
								<a href="http://pcr.dreapurimac.gob.pe/laravel/public/filera/pri/pdf/{{$item->archivoPdf}}" target="_blank" class="btn text-light bg-danger btn-xs" title="Descargar"><i class="fas fa-file-pdf fa-2x"></i></a>
								@endif
								@if($item->earchivo!='')
								<a href="{{$item->earchivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de archivo"><i class="fas fa-file fa-2x"></i></a>
								@endif
								@if($item->evideo!='')
								<a href="{{$item->evideo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de video"><i class="fas fa-file-video fa-2x"></i></a>
								@endif
								@if($item->eaudio!='')
								<a href="{{$item->eaudio}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Enlace de audio"><i class="fas fa-file-audio fa-2x"></i></a>
								@endif
							</p>
							<hr class="my-0">
							@endforeach
						@endif
					</div>
				</div>
			</div>
		</div>
	</div>