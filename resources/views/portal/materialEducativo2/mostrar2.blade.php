@extends('template/templatePortal')
@section('generalBodyPortal')
<style>
	.card{
		border: 3px solid red;
    border-radius: 5px;
    padding: 5px;
    box-shadow: 5px 10px 9px teal;
	}
	body{background-image: url('{{asset('imagen/fondo.png')}}');}
</style>
<div class="container mt-4">
	<div class="row presentacion">
		<div class="col-lg-12">
			<h5 class="text-center titulo-re">RECURSOS EDUCATIVOS(NOVIEMBRE)</h5>
		</div>
		<div class="col-lg-12 pb-4">
			<div class="separator"></div>
		</div>
		<div class="col-lg-6">
			<p>Los recursos educativos abiertos <strong>(representados con  las siglas REA)</strong> son recursos digitales creados con una finalidad educativa que se encuentran de manera gratuita y abierta para docentes, estudiantes y el público general.</p>
			<p><strong>El contenido de los mismos es bastante variado:</strong> pueden incluir textos, imágenes, recursos de audio y vídeo, juegos educativos y herramientas de software, entre otros.</p>
			<p><strong>Recursos disponibles segun nivel.</strong></p>
		</div>

		<div class="col-lg-6">
			<img src="{{asset('imagen/repositorio.jpg')}}" width="100%">
		</div>
	</div>
	<div class="row">
		<div class="col-lg-4 col-md-4 col-sm-6">
			<div class="card cardNivel" style="width: 100%;cursor: pointer;" data-nivel="inicial">
				<img src="{{asset('imagen/ini.png')}}">
		  		<img class="card-img-top d-none d-sm-block" src="{{asset('imagen/inicial.jpg')}}" width="100%">
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6">
			<div class="card cardNivel" style="width: 100%;cursor: pointer;" data-nivel="primaria">
				<img src="{{asset('imagen/pri.png')}}">
		  		<img class="card-img-top d-none d-sm-block" src="{{asset('imagen/primaria.jpg')}}" width="100%">
			</div>
		</div>
		<div class="col-lg-4 col-md-4 col-sm-6">
			<div class="card cardNivel" style="width: 100%;cursor: pointer;" data-nivel="secundaria">
				<img src="{{asset('imagen/sec.png')}}">
		  		<img class="card-img-top d-none d-sm-block" src="{{asset('imagen/secundaria.jpg')}}" width="100%">
			</div>
		</div>
	</div>
	<div class="row iniCont" style="display: none;">
		<div class="col-lg-12 p-4">
			<div class="card" style="border: 3px solid #cddee0;box-shadow: 5px 3px 9px #008080ba;background-image: url('{{asset('imagen/fondo.png')}}')";>
				<img src="{{asset('imagen/ini.png')}}" class="m-auto w-25">
				<div class="container text-center mt-4">
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="ini-5"><strong>5 AÑOS</strong></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row priCont" style="display: none;">
		<div class="col-lg-12 p-4">
			<div class="card" style="border: 3px solid #cddee0;box-shadow: 5px 3px 9px #008080ba;background-image: url('{{asset('imagen/fondo.png')}}')";">
				<img src="{{asset('imagen/pri.png')}}" class="m-auto w-25">
				<div class="container text-center mt-4">
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-1"><strong>1° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-2"><strong>2° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-3"><strong>3° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-4"><strong>4° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-5"><strong>5° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-buscar="pri-6"><strong>6° GRADO</strong></span>
				</div>
			</div>
		</div>
	</div>
	<div class="row secCont" style="display: none;">
		<div class="col-lg-12 p-4">
			<div class="card" style="border: 3px solid #cddee0;box-shadow: 5px 3px 9px #008080ba;background-image: url('{{asset('imagen/fondo.png')}}')";">
				<img src="{{asset('imagen/sec.png')}}" class="m-auto w-25">
				<div class="container text-center mt-4">
					<span class="badgeGrado badge badge-primary py-3 px-4" data-name="1"><strong>1° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-name="2"><strong>2° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-name="3"><strong>3° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-name="4"><strong>4° GRADO</strong></span>
					<span class="badgeGrado badge badge-primary py-3 px-4" data-name="5"><strong>5° GRADO</strong></span>
				</div>
				<div class="container text-center mt-3 nameSec"><h2 class="m-0"></h2></div>
				<div class="container text-center mt-4 containerCurso" style="display: none;">
					<span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-comu"><strong>COMUNICACION</strong></span>
					<span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-mate"><strong>MATEMATICA</strong></span>
					<span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-csoc"><strong>CIENCIAS SOCIALES</strong></span>
					<span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-ctec"><strong>CIENCIA Y TECNOLOGIA</strong></span>

					<span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-dper"><strong>Desarrollo Personal, Ciudadanía y Cívica</strong></span>
					<!-- <span class="badgeCurso badge badge-primary py-3 px-4" data-buscar="sec-cciv"><strong>CIUDADANIA Y CÍVICA</strong></span> -->
				</div>
			</div>
		</div>
	</div>
</div>
<style>
	.ver{display: block !important;}
	.badgeGrado,.badgeCurso{font-size: 1.1rem;border: 5px solid #fdc500;border-radius: 10px;cursor: pointer;}
	.badge:hover{background:#ff00008f; }
	.activeOpcion,.activeCurso{background: red;}
</style>
<!-- <div class="container text-center mt-4 containerMaterial">aki ta</div> -->

<script>
	function buscar(data)
	{
		console.log(data);
		// $('.ver').find('.card').append('<div class="container text-center mt-4 containerMaterial">aki ta '+data+'</div>');
		jQuery.ajax(
        { 
            url: "{{ url('/materialEducativo2/buscar2') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	data:data
            },
            method: 'post',
            success: function(result){
            	$('.ver').find('.card').append(result);
            	// $('.contenedorSegunNivel>div').remove();
             //    $('.contenedorSegunNivel').append(result);
            }
        });
	}
	
	$('.cardNivel').on('click',function(){
		$('.nameSec>h2').html('');
		$('.presentacion').css('display','none');
		$('.containerCurso').css('display','none');
		$('.ver').removeClass('ver');
		$('.activeOpcion').removeClass('activeOpcion');
		$('.containerMaterial').remove();
		if($(this).attr('data-nivel')=='inicial') $('.iniCont').addClass('ver');
		if($(this).attr('data-nivel')=='primaria') $('.priCont').addClass('ver');
		if($(this).attr('data-nivel')=='secundaria') $('.secCont').addClass('ver');
	});
	$('.badgeGrado').on('click',function(){
		$('.activeOpcion').removeClass('activeOpcion');
		$('.activeCurso').removeClass('activeCurso');
		$('.containerMaterial').remove();
		$(this).addClass('activeOpcion');
		// console.log
		$('.bloque').remove();
		// --------------
		if($(this).attr('data-buscar')!==undefined)
		{
			// alert('puede envia|'+$(this).attr('data-buscar'));
			buscar($(this).attr('data-buscar'));
		}
		else
		{
			// alert('es secundaria');
			$('.nameSec>h2').html($(this).attr('data-name')+'° GRADO ¡COMENCEMOS!');
			$('.badgeCurso').attr('data-grado',$(this).attr('data-name'));
			$('.containerCurso').css('display','block');
		}
	});
	$('.badgeCurso').on('click',function(){
		$('.activeCurso').removeClass('activeCurso');
		$(this).addClass('activeCurso');
		$('.containerMaterial').remove();
		$('.bloque').remove();
		buscar($(this).attr('data-grado')+'-'+$(this).attr('data-buscar'));
	});
</script>
<script>
	function buscarSemanaNew(data)
	{
		// alert(data);
		jQuery.ajax(
        { 
            url: "{{ url('/materialEducativo2/buscarSemana2') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	data:data
            },
            method: 'post',
            success: function(result){
            	console.log('entro a ali');
            	$('.ver').find('.card').append(result);
            	// $('.containerMaterialBuscado>row').remove();
            	// $('.containerMaterialBuscado>row').append(result);
            }
        });
		$('.bloque').remove();
	}
	// $('.semana').on('click',function(){
	// 	// $('.actSemana').removeClass('actSemana');
	// 	// $(this).addClass('actSemana');
	// 	// $('.containerMaterial').remove();
	// 	alert($(this).attr('data-semana'));
	// 	// buscarSemana($(this).attr('data-semana'));
	// 	$('.bloque').remove();
		
	// });
</script>
@endsection