<div class="card card-default card-info card-outline" style="background: rgb(218 211 131 / 50%);">
    <div class="card-body p-0 mt-3">
        <div class="row">
            <div class="col-md-12">
                <table id="example1" class="table table-sm table-hover m-0 w-100">
                    <thead class="text-center">
                        <tr>
                            <th>Ugel</th>
                            <th>Tipo de material</th>
                            <th>Mes</th>
                            <th>Nivel</th>
                            <th>Grado</th>
                            <th>Curso</th>
                            <th>Nombre</th>
                            <th>Descripcion</th>
                            <th>Archivo</th>
                            <th>Enlaces</th>
                            <th>Opciones</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($list as $item)
                        <tr class="text-center">
                            <td>{{$item->ugel}}</td>
                            <td>{{$item->tipoMaterial=='expApr'?'Experiencias de aprendizaje':'Fichas de autoaprendizaje'}}</td>
                            <td>{{$item->mes}}</td>
                            <td>{{$item->nivel}}</td>
                            <td>{{$item->grado}}</td>
                            <td>{{$item->curso}}</td>
                            <td>{{$item->nombre}}</td>
                            <td>{{$item->descripcion}}</td>
                            <td>{{$item->archivo}}</td>
                            <td>
                                @if($item->earchivo!='')
                                    <a href="{{$item->earchivo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> archivo</a>
                                @endif
                                @if($item->evideo!='')
                                    <a href="{{$item->evideo}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> video</a>
                                @endif
                                @if($item->eaudio!='')
                                    <a href="{{$item->eaudio}}" target="_blank" class="btn btn-info btn-xs"><i class="fa fa-link"></i> audio</a>
                                @endif
                            </td>
                            <td>
                                @if($item->archivo!='')
                                    <a href="http://pcr.dreapurimac.gob.pe/laravel/public/filerecursosaprendizaje/{{$item->archivo}}" target="_blank" class="btn text-light bg-dark btn-xs" title="Descargar"><i class="fa fa-download"></i><!--  Descargar --></a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
<script>
	$(document).ready( function () {
        $('#example1').DataTable( {
            "destroy":true,
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay materiales disponibles",
                "sEmptyTable": "No se cuenta con materiales.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar materiales');
    } );
</script>