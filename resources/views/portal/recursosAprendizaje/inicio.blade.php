@extends('template/templatePortal')
@section('generalBodyPortal')
<style>
	.cursorMano{
		cursor: pointer;
	}
    .selectedRow{background: #4ec369;}
    .selectedGrado{background: #7fda82;}
</style>
<div class="container-fluid mt-3">
	<div class="row">
		<div class="col-lg-3 col-md-3 col-sm-12">
			<div class="row contenedorNivel">
				<div class="col-lg-12 col-md-4">
					<div class="card cursorMano nivel" data-nivel='Inicial'>
						<div class="card-body p-2">
							<h1 class="text-center">Inicial</h1>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-4">
					<div class="card cursorMano nivel" data-nivel='Primaria'>
						<div class="card-body p-2">
							<h1 class="text-center">Primaria</h1>
						</div>
					</div>
				</div>
				<div class="col-lg-12 col-md-4">
					<div class="card cursorMano nivel" data-nivel='Secundaria'>
						<div class="card-body p-2">
							<h1 class="text-center">Secundaria</h1>
						</div>
					</div>
				</div>
			</div>
		</div>
		<div class="col-lg-9">
			<div class="card card-default card-info card-outline mensajeBienvenida">
				<div class="card-body">
					<h2 class="m-0">En este apartado encontrada los materiales educativos, las cuales estan organizadas por:</h2>
					<ul class="m-0">
						<li><h5>Nivel</h5></li>
						<li><h5>Grado</h5></li>
						<li><h5>Tipo de material</h5></li>
						<li><h5>Mes</h5></li>
					</ul>
				</div>
			</div>
			<div class="row contenedorGrado" style="visibility: hidden;">
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='0'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado0">1º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='1'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado1">2º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='2'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado2">3º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='3'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado3">4º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='4'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado4">5º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='5'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado5">6º grado</p>
						</div>
					</div>
				</div>
				<div class="col-lg-2">
					<div class="card cursorMano grado" data-grado='6'>
						<div class="card-body p-2">
							<p class="text-center m-0 font-weight-bold grado6">6 años</p>
						</div>
					</div>
				</div>
				<div class="col-lg-12">
					<div class="alert alert-info mensaje">
						<p class="m-0 font-weight-bold text-center">Debe elegir un grado para poder mostrar los recursos.</p>
					</div>
				</div>
				<div class="col-lg-12 containerTabla1" style="display: none;">
					<div class="card card-default card-info card-outline" >
				        <div class="card-body p-0 ">
				            <div class="row">
				                <div class="col-md-12">
				                	<div class="table-responsive">
					                    <table id="tabla1" class="table table-sm table-hover m-0 w-100 table-striped table-bordered">
					                        <thead class="text-center">
					                            <tr>
					                                <th>Mayo</th>
					                                <th>Junio</th>
					                                <th>Julio</th>
					                                <th>Agosto</th>
					                                <th>Setiembre</th>
					                                <th>Octubre</th>
					                                <th>Noviembre</th>
					                                <th>Diciembre</th>
					                            </tr>
					                        </thead>
					                        <tbody>
					                            <tr class="text-center">
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Mayo'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Junio'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Julio'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Agosto'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Setiembre'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Octubre'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Noviembre'>Experiencia de aprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-link btn-sm mostrarMaterial" data-tipo='expApr' data-mes='Diciembre'>Experiencia de aprendizaje</a></td>
					                            </tr>
					                            <tr class="text-center">
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Mayo'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Junio'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Julio'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Agosto'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Setiembre'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Octubre'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Noviembre'>Fichas de autoaprendizaje</a></td>
					                            	<td><a href="#" class="btn btn-info btn-sm mostrarMaterial" data-tipo='ficAut' data-mes='Diciembre'>Fichas de autoaprendizaje</a></td>
					                            </tr>
					                        </tbody>
					                    </table>
				                	</div>
				                </div>
				            </div>
				        </div>
	    			</div>
				</div>
				
			</div>
		</div>
		<div class="col-lg-12 contenedorListMaterial">
			
		</div>
	</div>
</div>

<script>
	$('.titulo-banner').html('Materiales Educativos 2021');
	$('body').css('background-image','url({{asset('imgSocio/fondo.png')}})');
	$('body').css('background-repeat','round');
    
    $('.nivel').on('click',function(){
    	$('.mensajeBienvenida').css('display','none');
    	$('.contenedorListMaterial>div').remove();
    	$('.mensaje>p').html('Elija un grado/edad');
    	$('.containerTabla1').css('display','none');

    	$('.contenedorGrado').css('visibility','visible');
    	$('.contenedorGrado').find('.selectedGrado').removeClass('selectedGrado');
    	$('.contenedorNivel').find('.selectedRow').removeClass('selectedRow');
        $(this).addClass('selectedRow');
        if($(this).attr('data-nivel')=='i')
        {
        	$('.grado0').html('0 años');
			$('.grado1').html('1 años');
			$('.grado2').html('2 años');
			$('.grado3').html('3 años');
			$('.grado4').html('4 años');
			$('.grado5').html('5 años');
			$('.grado5').parent().parent().parent().css('display','block');
			$('.grado6').parent().parent().parent().css('display','block');
        }
        if($(this).attr('data-nivel')=='p')
        {
        	$('.grado0').html('1º grado');
			$('.grado1').html('2º grado');
			$('.grado2').html('3º grado');
			$('.grado3').html('4º grado');
			$('.grado4').html('5º grado');
			$('.grado5').html('6º grado');
			$('.grado5').parent().parent().parent().css('display','block');
			$('.grado6').parent().parent().parent().css('display','none');
        }
        if($(this).attr('data-nivel')=='s')
        {
        	$('.grado0').html('1º grado');
			$('.grado1').html('2º grado');
			$('.grado2').html('3º grado');
			$('.grado3').html('4º grado');
			$('.grado4').html('5º grado');
			$('.grado5').parent().parent().parent().css('display','none');
			$('.grado6').parent().parent().parent().css('display','none');
        }
    });
    $('.grado').on('click',function(){
    	$('.contenedorListMaterial>div').remove();
    	$('.containerTabla1').css('display','block');

    	$('.contenedorGrado').find('.selectedGrado').removeClass('selectedGrado');
        $(this).addClass('selectedGrado');

        var gra=$('.selectedGrado').find('p').html();
    	$('.mensaje>p').html('Material educativo de '+gra);
        
    });
    $('.mostrarMaterial').on('click',function(){
    	$('.contenedorListMaterial>div').remove();
    	var nivel=$('.selectedRow').attr('data-nivel');
    	var grado=$('.selectedGrado').attr('data-grado');
    	var tipo=$(this).attr('data-tipo');
    	var mes=$(this).attr('data-mes');
    	
    	// alert(nivel+grado+tipo);
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/recursosAprendizaje/listarPortal') }}",
            data: {nivel:nivel,grado:grado,tipo:tipo,mes:mes},
            method: 'get',
            success: function(result){
                $('.contenedorListMaterial').append(result);
            }
        });
        
    });
    
</script>
@endsection
