<script src="{{asset('js/export/jquery.dataTables.min.js')}}"></script>

<div class="mb-4">
	<div class="badge badge-info m-0 w-100 mb-2" style="font-size: 20px;">V CICLO</div>
	<table id="list" style="width: 100%;" class="table-bordered">
		<thead class="bg-info">
			<tr class="text-center">
				<th>N°</th>
				<th>CICLO</th>
				<th>TITUTLO</th>
				<th>UGEL</th>
				<th>T. ARCHIVO</th>
				<th>ARCHIVO</th>
			</tr>
		</thead>
		<tbody>
			<tr class="text-center">
				<td>1</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>experiencia de aprendizaje V ciclo setiembre</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/experiencia_de_aprendizaje_v_ciclo_setiembre.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>2</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>ficha ciencia y tecnologia V ciclo 1/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_cyt_v_ciclo_1_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>3</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>ficha de comunicacion V ciclo 4/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_comunicacion_v_ciclo_4_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>4</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>ficha de matematica V ciclo 2/9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_matematica_v_ciclo_2_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>5</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>personal social V ciclo 9/2020</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/personal_social_v_ciclo_9_2020.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

			<tr class="text-center">
				<td>6</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>5grado comunicaion primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5grado_comunicaion_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>7</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>5grado comunicaion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5grado_comunicaion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>8</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>5grado personal social primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

			<tr class="text-center">
				<td>9</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>5grado personal social segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>10</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>6grado comunicaion primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6grado_comunicaion_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>11</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>6grado comunicaion segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6grado_comunicaion_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>12</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>6grado personal social primera semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6grado_ps_primera_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>13</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>6grado personal social segunda semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6grado_ps_segunda_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>14</td>
				<td>V CICLO</td>
				<td>--</td>
				<td>experiencia de aprendizaje V ciclo 1ra 2da semana</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/experiencia_de_aprendizaje_v_ciclo_1ra_2da_semana.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

			<tr class="text-center">
				<td>15</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>1 ficha de personal social 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_ps_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>16</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>2 ficha de comunicacion 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2_ficha_comunicacion_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>17</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>3 ficha matematica 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/3_ficha_matematica_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>18</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>4 ficha educacion fisica 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/4_ficha_educacion_fisica_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>19</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>5 ficha ciencia y tecnologia 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5_ficha_cyt_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>

			<tr class="text-center">
				<td>20</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>6 ficha personal social 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6_ficha_ps_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>21</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>7 ficha arte y cultura 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/7_ficha_arte_y_cultura_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>22</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>8 ficha comunicacion 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/8_ficha_comunicacion_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>23</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>9 ficha arte y cultura 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/9_ficha_arte_y_cultura_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>24</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>10 ficha comunicacion 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/10_ficha_comunicacion_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>25</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>0 ficha personal social 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/0_ficha_ps_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>26</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>1 ficha matematica 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_matematica_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>27</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>2 ficha comunicacion 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2_ficha_comunicacion_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>28</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>3 ficha matematica 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/3_ficha_matematica_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>29</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>4 ficha educacion fisica 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/4_ficha_educacion_fisica_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>30</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>5 ficha ciencia y tecnologia 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/5_ficha_cyt_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>31</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>6 ficha personal social 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/6_ficha_ps_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>32</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>7 ficha arte y cultura 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/7_ficha_arte_cultura_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>33</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>8 ficha comunicacion 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/8_ficha_comunicacion_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>34</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>9 ficha arte y cultura 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/9_ficha_arte_cultura_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>35</td>
				<td>V CICLO</td>
				<td>antabamba</td>
				<td>10 ficha comunicacion 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/10_ficha_comunicacion_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>


			<tr class="text-center">
				<td>36</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>orientaciones a ppff 5grado final</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/orientaciones_a_ppff_5grado_final.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>37</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>orientaciones a ppff 6grado final</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/orientaciones_a_ppff_6grado_final.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>38</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 comunicacion 5grado semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_comunicacion_5grado_semana1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>39</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 ciencia y tecnologia 5grado semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_cyt_5grado_semana1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>40</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 matematica 5grado semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_matematica_5grado_semana1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>41</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 personal social 5grado semana1</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ps_5grado_semana1.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>42</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 5grado semana1 leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_5grado_semana1_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>43</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 5grado semana2 leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_5grado_semana2_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>44</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 5grado semana3 leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_5grado_semana3_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>45</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 5grado semana4 leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_5grado_semana4_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>46</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 5grado semana5 leemos juntos</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_5grado_semana5_leemos_juntos.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>47</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 6grado semana1 comunicacion</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_6grado_semana1_comunicacion.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>48</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 6grado semana1 cñiencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_6grado_semana1_cyt.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>49</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 6grado semana1 matematica</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_6grado_semana1_matematica.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>50</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>1 6grado semana1 personl social</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_6grado_semana1_ps.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>51</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>comunicacion 5grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/comunicacion_5grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>52</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>ciencia y tecnologia de 5grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/cyt_5grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>53</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>matematica 5grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/matematica_5grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>54</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>personal social 5grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ps_5grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>55</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>comunicacion 6grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/comunicacion_6grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>56</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>ciencia y tecnologia 6grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/cyt_6grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>57</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>matematicas 6grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/matematicas_6grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>58</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>personal social 6grado semana2 prim</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ps_6grado_semana2_prim.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>59</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>semana1 leemos juntos 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana1_leemos_juntos_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>60</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>semana2 leemos juntos 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_leemos_juntos_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>61</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>semana3 leemos juntos 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana3_leemos_juntos_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>62</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>semana4 leemos juntos 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana4_leemos_juntos_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>63</td>
				<td>V CICLO</td>
				<td>DREa</td>
				<td>semana5 leemos juntos 6grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana5_leemos_juntos_6grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>












			<tr class="text-center">
				<td>64</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>orientaciones a ppff 5grado final abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/orientaciones_a_ppff_5grado_final_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>65</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana1 leemos juntos 5grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana1_leemos_juntos_5grado_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>66</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 leemos juntos 5grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_leemos_juntos_5grado_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>67</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana3 leemos juntos 5grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana3_leemos_juntos_5grado_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>68</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana4 leemos juntos 5grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana4_leemos_juntos_5grado_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>69</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana5 leemos juntos 5grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana5_leemos_juntos_5grado_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



			<tr class="text-center">
				<td>70</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana comunicacion 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_comunicacion_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>71</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana ciencia y tecnologi 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_cyt_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>72</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana matematica 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_matematica_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>73</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana personal social 5grado</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_ps_5grado.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>






			<tr class="text-center">
				<td>74</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 comunicacion 5grado primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_comunicacion_5grado_primaria.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>75</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 ciencia y tecnologia 5grado primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_cyt_5grado_primaria.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>76</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 matematica 5grado primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_matematica_5grado_primaria.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>77</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 personal social 5grado primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_ps_5grado_primaria.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
















			<tr class="text-center">
				<td>78</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>orientaciones a ppff 6grado final abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/orientaciones_a_ppff_6grado_final_aba.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>







			<tr class="text-center">
				<td>79</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana1 leemos juntos 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana1_leemos_juntos_6grado_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>80</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana2 leemos juntos 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana2_leemos_juntos_6grado_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>81</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana3 leemos juntos 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana3_leemos_juntos_6grado_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>82</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana4 leemos juntos 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana4_leemos_juntos_6grado_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>83</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>semana5 leemos juntos 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/semana5_leemos_juntos_6grado_abancay.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>






			<tr class="text-center">
				<td>84</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana comunicacion 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_comunicacion_6grado_aban.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>85</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana ciencia y tecnologia 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_cyt_6grado_aban.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>86</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana matematica 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_matematica_6grado_aban.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>87</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>1semana personal social 6grado abancay</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1semana_ps_6grado_aban.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>





			<tr class="text-center">
				<td>88</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>2semana comunicacion 6grado abancay primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2semana_comunicacion_6grado_aban_pri.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>89</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>2semana ciencia y tecnologia 6grado ababncay primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2semana_cyt_6grado_aban_pri.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>90</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>2semana matematica 6grado abancay primaria</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2semana_matematica_6grado_aban_pri.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>91</td>
				<td>V CICLO</td>
				<td>abancay</td>
				<td>2semana personal social 6grado aban pri</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/2semana_ps_6grado_aban_pri.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
















			<tr class="text-center">
				<td>92</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de ccomunicacion castellano v ciclo 14 agosto semana19</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_ccomunicacion_castellano_v_ciclo_14_agosto_semana19.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>93</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de ciencia y tecnologia v ciclo 3 agosto semana18</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_cyt_v_ciclo_3_agosto_semana18.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>94</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de ciencia y tecnologia v ciclo 10 agosto semana19</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_cyt_v_ciclo_10_agosto_semana19.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>95</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de ciencia y tecnologia v ciclo 17 agosto semana20</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_cyt_v_ciclo_17_agosto_semana20.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>






			<tr class="text-center">
				<td>96</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de ciencia y tecnologia v comunicacion casrtellano 7 agosto semana18</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_cyt_v_comunicacion_casrtellano_7_agosto_semana18.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>97</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de matematica castellano v ciclo 5 agosto semana18</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_matematica_castellano_v_ciclo_5_agosto_semana18.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>98</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de matematica castellano v ciclo 12 agosto semana19</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_matematica_castellano_v_ciclo_12_agosto_semana19.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>99</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de matematica castellano v ciclo 19 agosto semana20</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_matematica_castellano_v_ciclo_19_agosto_semana20.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>




			<tr class="text-center">
				<td>100</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de personal social v ciclo 6 agosto semana18</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_ps_v_ciclo_6_agosto_semana18.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>101</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de personal social v ciclo 13 agosto semana19</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_ps_v_ciclo_13_agosto_semana19.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>102</td>
				<td>V CICLO</td>
				<td>chincheros</td>
				<td>ficha de personal social v ciclo 20 agosto semana20</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/ficha_de_ps_v_ciclo_20_agosto_semana20.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>












			<tr class="text-center">
				<td>103</td>
				<td>V CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha IV ciclo comunicacion</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_IV_ciclo_comunicacion_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>104</td>
				<td>V CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha IV ciclo matematica</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_IV_ciclo_matematica_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>105</td>
				<td>V CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha IV ciclo personal social</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_IV_ciclo_ps_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>
			<tr class="text-center">
				<td>106</td>
				<td>V CICLO</td>
				<td>cotabambas</td>
				<td>1 ficha V ciclo ciencia y tecnologia</td>
				<td><i class="fa fa-file-pdf"></i> PDF</td>
				<td><a href="{{$v=asset('files/primaria/5ciclo/1_ficha_V_ciclo_cyt_cota.pdf')}}" class="btn btn-sm btn-link" target="_blank"><i class="fa fa-file"></i> Ver</a></td>
			</tr>



		</tbody>
	</table>
</div>
<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>