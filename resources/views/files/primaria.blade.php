<style>
	.card{
		border: 3px solid red;
    	border-radius: 5px;
    	padding: 5px;
    	box-shadow: 5px 10px 9px teal;
	}
	h5{
		text-shadow: 2px 3px 0px #d0c3c3;
    	font-size: 20px;
	}
</style>
<div class="row">
	<div class="col-md-4 p-4">
		<div class="card segun" data-segun="primaria-3" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/primaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">III CICLO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-4 p-4">
		<div class="card segun" data-segun="primaria-4" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/primaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">IV CICLO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-4 p-4">
		<div class="card segun" data-segun="primaria-5" style="width: 100%;cursor: pointer;">
		  	<img class="card-img-top" src="{{asset('imagen/primaria.jpg')}}" width="100%">
		  	<div class="card-body">
		    	<h5 class="card-title text-center">V CICLO</h5>
		  	</div>
		</div>
	</div>
	<div class="col-md-12 contenedorSegunList my-4"></div>
</div>
<script>
	// _token:$('meta[name="_token"]').attr('content')
	$('.segun').on('click',function(){
        // alert('cascasc');
        jQuery.ajax(
        { 
            url: "{{ url('/files/viewList') }}",
            data: {
            	_token:$('meta[name="_token"]').attr('content'),
            	nivel:$(this).attr('data-segun')},
            method: 'post',
            success: function(result){
            	console.log(result);
            	$('.contenedorSegunList>div').remove();
                $('.contenedorSegunList').append(result);
            }
        });
	});
</script>