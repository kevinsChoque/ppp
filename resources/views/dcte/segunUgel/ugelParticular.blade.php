<style>
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<div class="modal fade" id="modalChangeIe" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header py-1">
                <h5 class="modal-title" id="exampleModalLabel">Cambiar de I.E.</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="alert alert-success text-center font-weight-bold py-1 colegio"></div>
                <div class="row">
                    <input type="hidden" name="dniChange" id="dniChange">
                    <div class="col-md-12">
                        <input type="hidden" id="typeNivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                        @if(session()->get('Person')->tEspecialista->nivel=='General')
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <select name="nivelChange" id="nivelChange" class="form-control form-control-sm">
                                <option disabled selected>Elija el nivel:</option>
                                <option value="inicial">Inicial</option>
                                <option value="primaria">Primaria</option>
                                <option value="secundaria">Secundaria</option>
                            </select>
                        </div>
                        @else
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <input type="text" id="nivelChange" name="nivelChange" value="{{session()->get('Person')->tEspecialista->nivel}}" class="form-control form-control-sm" disabled>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-12">
                        <div class="form-group mb-2">
                            <label class="m-0">I.E.:</label>
                            <select name="iesChange" id="iesChange" class="form-control form-control-sm select2">
                                <option disabled selected>Primero seleccione nivel:</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer py-1">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary btn-sm btnChangeIe">Guardar cambio</button>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Buscar institucion educativa</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body pb-0 py-2">
            <div class="row">
                <div class="col-md-12 alert alert-info msjIe text-center font-weight-bold p-1 mb-1" style="display: none;"></div>
                <div class="col-md-4">
                    <input type="hidden" id="typeNivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                    @if(session()->get('Person')->tEspecialista->nivel=='General')
                    <div class="form-group mb-2">
                        <label class="m-0">Nivel:</label>
                        <select name="nivel" id="nivel" class="form-control form-control-sm">
                            <option disabled selected>Elija el nivel:</option>
                            <option value="inicial">Inicial</option>
                            <option value="primaria">Primaria</option>
                            <option value="secundaria">Secundaria</option>
                        </select>
                    </div>
                    @else
                    <div class="form-group mb-2">
                        <label class="m-0">Nivel:</label>
                        <input type="text" id="nivel" name="nivel" value="{{session()->get('Person')->tEspecialista->nivel}}" class="form-control form-control-sm" disabled>
                    </div>
                    @endif
                </div>
                <div class="col-md-4">
                    <div class="form-group mb-2">
                        <label class="m-0">I.E.:</label>
                        <select name="ies" id="ies" class="form-control form-control-sm select2">
                            <option disabled selected>Primero seleccione nivel:</option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group mb-2">
                        <label class="m-0" style="visibility: hidden;"></label>
                        <input type="button" value="Agregar docente" class="btn btn-success btn-sm w-100 addDcte">
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group mb-2">
                        <label class="m-0" style="visibility: hidden;"></label>
                        <input type="button" value="Listar docentes" class="btn btn-success btn-sm w-100 listDcte">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container-fluid my-3 msj">
    <div class="row">
        <div class="alert alert-info w-100 text-center m-0 font-weight-bold">Elija una institucion educativa.</div>
    </div>
</div>
<div class="contenedor-ajax" style="display: none;"></div>
<script>
    var ppp = $('#typeNivel').val();
    if(ppp!='general')
    {
        cargaie(ppp);
        cargaieChange(ppp)
    }

    $('#nivel').on('change',function(ev){
        cargaie($('#nivel').val());
    });
    $('#nivelChange').on('change',function(ev){
        cargaieChange($('#nivelChange').val());
    });
    $( document ).ready(function() 
    { 
        $('#ies').select2({
            placeholder: "Seleccione un curso",
        });
        $('#iesChange').select2({
            placeholder: "Seleccione un curso",
        });
    });

    $('.addDcte').on('click',function(){
        if($('#ies').val()!==null)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax(
            { 
                url: "{{ url('/dcte/addDcte') }}",
                data: {cm:$('#ies').val()},
                method: 'get',
                success: function(result){
                    $('.contenedor-ajax>div').remove();
                    $('.msj').css('display','none');
                    $('.contenedor-ajax').css('display','block');
                    $('.contenedor-ajax').append(result);
                }
            });
        }
    });
    $('.listDcte').on('click',function(){
        if($('#ies').val()!==null)
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax(
            { 
                url: "{{ url('/dcte/listDcte') }}",
                data: {cm:$('#ies').val()},
                method: 'get',
                success: function(result){
                    $('.contenedor-ajax>div').remove();
                    $('.msj').css('display','none');
                    $('.contenedor-ajax').css('display','block');
                    $('.contenedor-ajax').append(result);
                }
            });
        }
    });
    
    function cargaie(nivel)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getie') }}",
            data: {nivel:nivel},
            method: 'get',
            success: function(result){
                $("#ies").empty();
                $("#ies").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    $("#ies").append("<option value='" + value.codigomodular + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }

    function cargaieChange(nivel)
    {
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getie') }}",
            data: {nivel:nivel},
            method: 'get',
            success: function(result){
                $("#iesChange").empty();
                $("#iesChange").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    $("#iesChange").append("<option value='" + value.codigomodular + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }

    $('.btnChangeIe').on('click',function(){
        if($("#iesChange").val()!==null && $("#iesChange").val()!=$("#ies").val())
        {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            jQuery.ajax(
            { 
                url: "{{ url('/dcte/changeIe') }}",
                data: {cm:$('#iesChange').val(),dni:$('#dniChange').val()},
                method: 'get',
                success: function(result){
                    if(result.success)
                    {
                        $('#modalChangeIe').modal('hide');
                        $('.selectedRow').click();
                        if (typeof window.stackBottomLeft === 'undefined') {
                            window.stackBottomLeft = {
                                'dir1': 'right',
                                'dir2': 'up',
                                'firstpos1': 25,
                                'firstpos2': 480,
                                'push': 'top'
                            };
                        }
                        if (typeof window.stackTopLeft === 'undefined') {
                            window.stackTopLeft = {
                              'dir1': 'down',
                              'dir2': 'right',
                              'firstpos1': 25,
                              'firstpos2': 25,
                              'push': 'top'
                            };
                          }
                          if (typeof window.stackTopCenter === 'undefined') {
                            window.stackTopCenter = {
                              'dir1': 'down',
                              'firstpos1': 25
                            };
                          }
                        new PNotify(
                        {
                            title : 'Cambio exitoso.',
                            text : 'El docente '+ result.nombreDcte+' fue asignado a la I.E. '+ $("#iesChange option:selected").text(),
                            type : 'success',
                            stack: window.stackTopLeft
                        });
                    }
                }
            });
        }
        
    });

</script>