<style>
    .selectedRow{
        background: rgba(1, 1, 1, 0.22);
    }
</style>
<div class="container-fluid p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Lista de Docentes</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0 mt-1">
            <div class="row">
                <div class="col-md-12">
                    <table id="listDcte" class="table table-sm table-hover m-0 w-100">
                        <thead class="text-center">
                            <tr>
                                <th>DNI</th>
                                <th>Nombre del docente</th>
                                <th>Situación</th>
                                <th>Archivos</th>
                                <th>Delegar como</th>
                                <th>Opciones</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($ie->tDocente as $item)
                            <tr class="text-center">
                                <td>{{$item->dni}}</td>
                                <td>{{$item->tPersona_->nombres}} {{$item->tPersona_->apaterno}} {{$item->tPersona_->amaterno}}</td>
								<td>{{$item->condicion}}</td>
                                <td>{{count($item->tCursoxdocente)}} plan anual | 

									@php $totalU = 0; @endphp
                                    @foreach($item->tCursoxdocente as $item2)
                                        @php $subtotal=0; @endphp
                                        @foreach($item2->tUnidad as $item3)
                                            @php
                                                $subtotal=$subtotal+1;
                                            @endphp
                                        @endforeach
                                        @php $totalU=$totalU+$subtotal; @endphp
                                    @endforeach
                                    {{$totalU}} unidad | 

									@php $totalS = 0; @endphp
                                    @foreach($item->tCursoxdocente as $item2)
                                        @foreach($item2->tUnidad as $item3)
                                            @php $subtotal=0; @endphp

                                            @foreach($item3->tSesion as $item4)
                                                @php
                                                    $subtotal=$subtotal+1;
                                                @endphp
                                            @endforeach
                                            @php $totalS=$totalS+$subtotal; @endphp
                                        @endforeach
                                    @endforeach
                                    {{$totalS}} sesiones
                                </td>
                                <td>
                                    @if($item->director==1 && $item->delegado!=1)
                                        <span class="btn right badge bg-warning" title="Director de la I.E.">Director</span>
                                    @else
                                        <span class="btn right badge {{$item->delegado==1 ?'bg-success':'bg-secondary'}} delegarDirector {{$item->dni}}delegar" data-dni="{{$item->dni}}" style="cursor: pointer;">{{$item->delegado==1 ?'Director':'Profesor'}}</span>
                                    @endif
                                    
                                </td>
                                <td><span class="btn right badge {{$item->estado==1 ?'bg-success':'bg-danger'}} editEstado {{$item->dni}}" data-dni="{{$item->dni}}" style="cursor: pointer;">{{$item->estado==1 ?'Activo':'Inactivo'}}</span> | 
								<!-- <a href="#" class="edit btn btn-primary btn-xs" data-id="2" data-toggle="tooltip" title="Editar docente"><i class="fa fa-edit"></i></a> -->
                                <a href="#" class="edit btn btn-danger btn-xs borrar" style="display: none;"><i class="fa fa-trash"></i></a>
                                
								<a href="#" class="btn btn-primary btn-xs changeIe" data-dni="{{$item->dni}}" title="Cambiar de I.E."><i class="fas fa-home"></i> <i class="fa fa-arrow-right"></i> <i class="fas fa-home"></i></a>
									
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
    // var table = $('#listDcte').DataTable();
    if (typeof window.stackTopLeft === 'undefined') {
        window.stackTopLeft = {
          'dir1': 'down',
          'dir2': 'right',
          'firstpos1': 25,
          'firstpos2': 25,
          'push': 'top'
        };
      }
    $('#listDcte tbody').on( 'click', '.borrar', function () {
        $('#listDcte').DataTable()
            .row( $(this).parents('tr') )
            .remove()
            .draw();
    } );
    $(document).ready( function () {
        $('#listDcte').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[4, 8, -1], [4, 8, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ docentes.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay docentes",
                "sEmptyTable": "La I.E no tiene docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar docente');
    } );

    $('.editEstado').on('click',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/dcte/editEstado') }}",
            data: {dni:$(this).attr('data-dni')},
            method: 'get',
            success: function(result){
				if(result.success)
				{
					$('.'+result.dni).html($('.'+result.dni).html()=='Activo'?'Inactivo':'Activo');
					$('.'+result.dni).toggleClass($('.'+result.dni).html()=='Activo'?'bg-success bg-danger':'bg-danger bg-success');
                    new PNotify(
                    {
                        title : 'Operacion exitosa',
                        type : 'success',
                        stack : window.stackTopLeft
                    });
				}
            }
        });
    });
    $('.delegarDirector').on('click',function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/dcte/delegarDirector') }}",
            data: {dni:$(this).attr('data-dni')},
            method: 'get',
            success: function(result){
                console.log('cambio exitoso');
                if(result.success)
                {
                    $('.'+result.dni+'delegar').html($('.'+result.dni+'delegar').html()=='Profesor'?'Director':'Profesor');
                    $('.'+result.dni+'delegar').toggleClass($('.'+result.dni+'delegar').html()=='Profesor'?'bg-success bg-secondary':'bg-secondary bg-success');
                    new PNotify(
                    {
                        title : $('.'+result.dni+'delegar').html()=='Profesor'?'Se delego como docente.':'Se delego como director.',
                        type : 'success',
                        stack : window.stackTopLeft
                    });
                }
            }
        });
    });
    $('.changeIe').on('click',function(){
        $(this).parent().find('.borrar').addClass('selectedRow');
        $('#modalChangeIe').modal('show');
        $('.colegio').html($("#nivel option:selected").text()+' | '+$("#ies option:selected").text());
        // console.log($(this).attr('data-dni'));
        $('#dniChange').val($(this).attr('data-dni'));
    });

    
</script>