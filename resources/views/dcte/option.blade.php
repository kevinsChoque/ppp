@extends('template/template')
@section('generalBody')
@if(session()->get('Person')->tEspecialista->ugelid=='30000')
    @include('dcte/segunUgel/ugelGeneral')
@else
    @include('dcte/segunUgel/ugelParticular')
@endif
@endsection