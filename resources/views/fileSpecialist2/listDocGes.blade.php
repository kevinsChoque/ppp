<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
	    <thead class="text-center">
	        <tr>
	            <th>I.E.</th>
	            <th>Nombre del archivo</th>
	            <th>Comentario</th>
	            <th>Compartido en la I.E</th>
	            <!-- <th>video</th> -->
	            <th>Fecha de registro</th>
	            <th>Opciones</th>
	        </tr>
	    </thead>
	    <tbody>
	        @foreach($list as $item)
	        <tr class="text-center">
	            <td>{{$item->tIIEE->ie_nombre}}</td>
	            <td>{{$item->nombre}}</td>
	            <td>{{$item->comentario}}</td>
	            <td>
	            	@if($item->compartido=='no')
	            	<span class="badge badge-warning">No</span>
	            	@else
					<span class="badge badge-info">Compartido</span>
	            	@endif
	            </td>
	            <!-- <td>video</td> -->
	            <td>{{$item->createddate}}</td>
	            <td>
                    @if($item->formato!='')
                        <span class="fa {{$item->formato=='pdf'?'fa-file-pdf':''}}
                    {{$item->formato=='docx'?'fa-file-word':''}}
                    {{$item->formato=='xls'?'fa-file-excel':''}}"></span> {{$item->formato}}
                    @else- -
                    @endif
                     | 
                    @if(strpbrk($item->nombrereal, '-')!='')
                        <a href="{{asset('fileIe/')}}/{{$item->nombrereal}}.{{$item->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                    @endif
                </td>
	        </tr>
	        @endforeach
	    </tbody>   
	</table>
</div>

<!-- <script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "La I.E. tiene _TOTAL_ plan curricular subidos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay plan curricular de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script> -->