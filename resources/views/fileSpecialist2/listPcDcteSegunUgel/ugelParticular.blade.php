<meta name="_token" content="{{csrf_token()}}" />
<style>
    .select2-selection.select2-selection--single{padding-top: .20rem!important;}
</style>
<div class="container-fluid mt-3 p-0">
    <div class="card card-default card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Plan curricular de docentes.</h3>
            <div class="card-tools">
                <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body p-0">
        	<div class="row justify-content-center align-items-center px-4 mt-2">
                <h6 class="font-weight-bold">Buscar: </h6>
                <div class="mx-2">
                    <input type="radio" name="radioarchivo" id="Cliclo" value="plana">
                    <label for="Cliclo" style="margin-right:20px" class="font-weight-normal">Plan anual.</label>
                    <input type="radio" name="radioarchivo" id="Seccion" value="unidad" checked=""> 
                    <label for="Seccion" style="margin-right:20px" class="font-weight-normal">Unidades.</label>
                    <input type="radio" name="radioarchivo" id="Curso" value="sesion">
                    <label for="Curso" style="margin-right:20px" class="font-weight-normal">Sessiones.</label>

                    <input type="radio" name="radioarchivo" id="Resumen" value="resumen">
                    <label for="Resumen" style="margin-right:20px" class="font-weight-normal">Todos los archivos.</label>
                </div>
            </div>
            <hr class="mt-0 mx-3">
            <form id="formSerch" action="{{url('fileIe/listPcDcte')}}" method="post">
                <div class="row mx-2">
                    <div class="col-md-4">
                    	<input type="hidden" id="typeNivel" value="{{session()->get('Person')->tEspecialista->nivel}}">
                    	@if(session()->get('Person')->tEspecialista->nivel=='General')
                        <div class="form-group">
                            <label class="m-0">Nivel:</label>
                            <select name="nivel" id="nivel" class="form-control form-control-sm">
                                <option disabled selected>Elija el nivel:</option>
                                <option value="inicial">Inicial</option>
                                <option value="primaria">Primaria</option>
                                <option value="secundaria">Secundaria</option>
                            </select>
                        </div>
                        @else
                        <div class="form-group mb-2">
                            <label class="m-0">Nivel:</label>
                            <input type="text" id="nivel" name="nivel" value="{{session()->get('Person')->tEspecialista->nivel}}" class="form-control form-control-sm" disabled>
                        </div>
                        @endif
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">I.E.:</label>
                            <select name="listIe" id="listIe" class="form-control form-control-sm">
                                <option disabled selected>Seleccione I.E.</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Mes:</label>
                            <select name="listMes" id="listMes" class="form-control form-control-sm">
                                <option disabled selected>Elija el mes:</option>
                                <option value="enero">Enero</option><option value="febrero">Febrero</option>
                                <option value="marzo">Marzo</option><option value="abril">Abril</option>
                                <option value="mayo">Mayo</option><option value="junio">Junio</option>
                                <option value="julio">Julio</option><option value="agosto">Agosto</option>
                                <option value="setiembre">Setiembre</option><option value="octubre">Octubre</option>
                                <option value="noviembre">Noviembre</option><option value="diciembre">Diciembre</option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Fecha inicio:</label>
                            <input type="date" name="fechaI" id="fechaI" class="form-control form-control-sm">
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form-group">
                            <label class="m-0">Fecha final:</label>
                            <input type="date" name="fechaF" id="fechaF" class="form-control form-control-sm">
                        </div>
                    </div>
                </div>
                {{csrf_field()}}
            </form>

            <div class="row mx-2 mb-3">
                <div class="col-md-4">
                    <input type="button" value="limpiar" class="btn btn-secondary btn-sm w-100 limpiar">
                </div>
                <div class="col-md-4">
                    <input type="button" value="Buscar" class="btn btn-success btn-sm w-100 btnBuscar">
                </div>
            </div>
            <div class="row contenedorAjax">
                <div class="col-md-12 contenedorTable">
                    <div class="alert alert-primary text-center m-0 font-weight-bold">Agregue los datos para comensar a buscar.</div>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
	var ppp = $('#typeNivel').val();
    if(ppp!='general')
        cargaie(ppp);

    $( document ).ready(function() 
    { 
        $("#listIe").select2({
            placeholder: "Seleccione un curso",
        });
    });
    $('.limpiar').on('click',function(){
        $('#nivel option:nth-child(1)').prop("selected",true);
        $('#listIe').val(null).trigger('change');
        $('#listMes option:nth-child(1)').prop("selected",true);
        $('#fechaI').val('');
        $('#fechaF').val('');
    });
    
    $('.btnBuscar').on('click',function()
    {
        // if($('#listDocente').val()===null)
        // {
        //     new PNotify(
        //     {
        //         title : 'No se pudo proceder',
        //         text : 'Debe de elejir un docente.',
        //         type : 'alert'
        //     });
        //     return 0;
        // }
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist2/listPcDcte') }}",
            data: {
                tipoList:$('input:radio[name=radioarchivo]:checked').val(),
                cm:$('#listIe').val(),
                mes:$('#listMes').val(),
                fi:$('#fechaI').val(),
                ff:$('#fechaF').val(),
            },
            method: 'get',
            success: function(result){
                $('.contenedorTable').remove();
                $('.contenedorAjax').append(result);
            }
        });
    });
    $('#nivel').on('change',function(ev){
        var nivel=$('#nivel').val();
        console.log(nivel);
        cargaie(nivel);
    });
    function cargaie(nivel)
    {
        console.log('entro a la funcion para cargar las ie');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileSpecialist/getie') }}",
            data: {nivel:nivel},
            method: 'get',
            success: function(result){
                $("#listIe").empty();
                $("#listIe").append("<option disabled selected>Seleccione I. E.</option>");
                $.each( result, function(key, value){
                    $("#listIe").append("<option value='" + value.codigomodular + "'>"+value.codigomodular+" - "+value.ie_nombre + "</option>");
                });
            }
        });
    }
    $("input[name=radioarchivo]").click(function () 
    {
        var aux=$('input:radio[name=radioarchivo]:checked').val();

        if(aux=='plana' || aux=='resumen')
        {
            $("#listMes").prop('disabled', true);
            $("#fechaI").prop('disabled', true);
            $("#fechaF").prop('disabled', true);
        }

        if(aux=='unidad')
        {
            $("#listMes").prop('disabled', false);
            $("#fechaI").prop('disabled', false);
            $("#fechaF").prop('disabled', false);
        }
        if(aux=='sesion')
        {
          $("#listMes").prop('disabled', true);
          $("#fechaI").prop('disabled', false);
            $("#fechaF").prop('disabled', false);
        }
    }); 
</script>
