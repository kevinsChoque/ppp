<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
        <thead class="text-center">
            <tr>
                <th>DNI</th>
                <th>Nonbre</th>
                <th>Plan anual</th>
                <th>Unidades</th>
                <th>Sessiones</th>
                
            </tr>
        </thead>
        <tbody>
            @foreach($list as $item)
            <tr class="text-center">
                <td>{{$item->dni}}</td>
                <td>{{$item->tPersona_->nombres}} {{$item->tPersona_->apaterno}} {{$item->tPersona_->amaterno}}</td>
                <td><p class="mb-0"><i class="fa fa-file"></i> {{count($item->tCursoxdocente)}} plan anual</p></td>
                <td>
                	<p class="mb-0"><i class="fa fa-file"></i> 
                        @php $totalS = 0; @endphp
                        @foreach($item->tCursoxdocente as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tUnidad as $item3)
                                @php
                                    $subtotal=$subtotal+1;
                                @endphp
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} unidades
                    </p>
                </td>
                <td>
                	<p class="mb-0"><i class="fa fa-file"></i> 
                        @php $totalS = 0; @endphp
                        @foreach($item->tCursoxdocente as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tUnidad as $item3)
                                @foreach($item3->tSesion as $item4)
                                	@php
	                                    $subtotal=$subtotal+1;
	                                @endphp
								@endforeach
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} sesiones
                    </p>
                </td>
            </tr>
            @endforeach
        </tbody>      
    </table>
</div>
<script>
   $(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "Se tiene _TOTAL_ docentes.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay registros de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            },
            dom: 'Bfrtip',
            buttons: [
                {
                    extend: 'excelHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-excel p-0 fa-lg"></i>',
                    className:'btn btn-success p-1 excel ml-4',
                },
                {
                    extend: 'pdfHtml5',
                    title: 'Registros',
                    text: '<i class="fa fa-file-pdf p-0 fa-lg"></i>',
                    className:'btn btn-danger p-1',
                },
                {
                    extend: 'print',
                    title: 'Registros',
                    text: '<i class="fa fa-print p-0 fa-lg"></i>',
                    className:'btn btn-info p-1',
                }
            ]
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script>
