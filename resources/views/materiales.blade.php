<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>Document</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
	<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
</head>
<body >
	<div class="container-fluid m-0 p-0" >
		<div class="col-lg-12 p-0">
			<img src="{{asset('imgSocio/cinta-andina.png')}}" class="w-100">
		</div>
	</div>
	<div class="p-1" style="background-image: url({{asset('imgSocio/fondo2.jpg')}}); background-repeat: round;">
		<div class="container-fluid m-0 p-0" >
			<div class="row justify-content-center align-items-center m-0">
				<!-- <div class="col-lg-5 text-center">
					<p class="text-center font-weight-bold" style="font-size: 2rem;">APOYO SOCIOEMOCIONAL</p>
					<img src="{{asset('imgSocio/ninos.png')}}" class="w-50">
			 	</div> -->
			 	<div class="col-lg-12 text-center font-weight-bold">
			 		<p class="text-center font-weight-bold" style="font-size: 2rem;">APOYO SOCIOEMOCIONAL</p>
					<p style="font-size: 1.2rem; text-shadow: 2px 2px 2px #e2c4c4;">Los equipos de convivencia de la DREA y UGEL de la region de apurimac desde el año 2020 ante la situacion de emergencia sanitaria mundial, ofrece recursos tecnologicos de contenido socioemocional, para atender a estudiantes, docentes y padres de familia integrantes de la comunidad educativa; los recursos contemplan el contexto culturaly linguistico de la dinamica educativa local considerando la siguiente categorizacion:</p>
			 	</div>
			</div>
		</div>
		<div class="container-fluid p-2" >
			<div class="row" style="    margin: 0 5rem;">
				<div class="col-lg-6 p-1">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
                        <div class="card-header py-1 text-uppercase font-weight-bold text-center bg-success text-white">1.- Materiales</div>
                        <div class="card-body">
                            <ul>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="col-lg-6 p-1">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
                        <div class="card-header py-1 text-uppercase font-weight-bold text-center bg-success text-white">2.- Materiales</div>
                        <div class="card-body">
                            <ul>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="col-lg-6 p-1">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
                        <div class="card-header py-1 text-uppercase font-weight-bold text-center bg-success text-white">3.- Materiales</div>
                        <div class="card-body">
                            <ul>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            </ul>
                        </div>
                    </div>
				</div>
				<div class="col-lg-6 p-1">
					<div class="card" style="box-shadow: 2px 2px 2px 1px rgb(0 0 0 / 20%);">
                        <div class="card-header py-1 text-uppercase font-weight-bold text-center bg-success text-white">4.- Materiales</div>
                        <div class="card-body">
                            <ul>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            	<li>archivo</li>
                            </ul>
                        </div>
                    </div>
				</div>
			</div>
	    </div>
	</div>
</body>
</html>