<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
        @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!= false)
        <thead class="text-center">
            <tr>
                <th>Edad</th>
                <th>Seccion</th>
                <th>Unidad</th>
                <th>Tipo de unidad</th>
                <th>Nombre de la unidad</th>
                <th>P. academico</th>
                <th>Fecha que se realizo</th>
                <th>Fecha de registro</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list as $item)
            <tr class="text-center">
                <td>{{$item->tCursoxdocente->tGrado->nombre}}</td>
                <td>Sección {{$item->tCursoxdocente->tSeccion->nombre}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tipoUnidad}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tCursoxdocente->periodoAcademico}}</td>
                <td>{{$item->fechaPertenece}}</td>
                <td>{{$item->createddate}}</td>
                <td>
                    @if($item->comentario!='')
                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                    @endif
                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                    <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tSesion)}} sesiones</span>
                </td>
            </tr>
            @endforeach
        </tbody>      
        @endif
        @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'secundaria')!= false || stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'primaria')!= false)
        <thead class="text-center">
            <tr>
                <th>Grado</th>
                <th>Seccion</th>
                <th>Curso</th>
                <th>Unidad</th>
                <th>Tipo de unidad</th>
                <th>Nombre de la unidad</th>
                <th>P. academico</th>
                <th>Fecha que se realizo</th>
                <th>Fecha de registro</th>
                <th>Opciones</th>
            </tr>
        </thead>
        <tbody>
            @foreach($list as $item)
            <tr class="text-center">
                <td>{{$item->tCursoxdocente->tGrado->nombre}}</td>
                <td>Sección {{$item->tCursoxdocente->tSeccion->nombre}}</td>
                <td>{{$item->tCursoxdocente->tCurso->nombre}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tipoUnidad}}</td>
                <td>{{$item->nombre}}</td>
                <td>{{$item->tCursoxdocente->periodoAcademico}}</td>
                <td>{{$item->fechaPertenece}}</td>
                <td>{{$item->createddate}}</td>
                <td>
                    @if($item->comentario!='')
                    <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                    @endif
                    <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
                    <span class="badge badge-secondary"><i class="fa fa-file"></i> {{count($item->tSesion)}} sesiones</span>
                </td>
            </tr>
            @endforeach
        </tbody>    
        @endif
        
        
    </table>
</div>

<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "La I.E. tiene _TOTAL_ plan curricular subidos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay plan curricular de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script>