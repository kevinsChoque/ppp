<div class="col-md-12 contenedorTable">
	<table id="list" class="table table-sm table-hover my-0" style="width:100%;">
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')!= false)
    <thead class="text-center">
        <tr>
            <th>Edad</th>
            <th>Seccion</th>
            <th>Unidd</th>
            <th>Tipo de unidd</th>
            <th>Nombre de l sesion</th>
            <th>P. academico</th>
            <th>Fecha de registro</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list as $item)
        <tr class="text-center">
            <td>{{$item->tUnidad->tCursoxdocente->tGrado->nombre}}</td>
            <td>Sección {{$item->tUnidad->tCursoxdocente->tSeccion->nombre}}</td>
            <td>{{$item->tUnidad->nombre}}</td>
            <td>{{$item->tUnidad->tipoUnidad}}</td>
            <td>{{$item->nombre}}</td>
            <td>{{$item->tUnidad->tCursoxdocente->periodoAcademico}}</td>
            <td>{{$item->createddate}}</td>
            <td>
                @if($item->comentario!='')
                <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                @endif
                <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>
    @endif
    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'secundaria')!= false || stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'primaria')!= false)
    <thead class="text-center">
        <tr>
            <th>Grado</th>
            <th>Seccion</th>
            <th>Curso</th>
            <th>Unidd</th>
            <th>Tipo de unidd</th>
            <th>Nombre de l sesion</th>
            <th>P. academico</th>
            <th>Fecha de registro</th>
            <th>Opciones</th>
        </tr>
    </thead>
    <tbody>
        @foreach($list as $item)
        <tr class="text-center">
            <td>{{$item->tUnidad->tCursoxdocente->tGrado->nombre}}</td>
            <td>Sección {{$item->tUnidad->tCursoxdocente->tSeccion->nombre}}</td>
            <td>{{$item->tUnidad->tCursoxdocente->tCurso->nombre}}</td>
            <td>{{$item->tUnidad->nombre}}</td>
            <td>{{$item->tUnidad->tipoUnidad}}</td>
            <td>{{$item->nombre}}</td>
            <td>{{$item->tUnidad->tCursoxdocente->periodoAcademico}}</td>
            <td>{{$item->createddate}}</td>
            <td>
                @if($item->comentario!='')
                <span class="btn btn-info btn-xs" data-toggle="tooltip" title="{{$item->comentario}}"><i class="fab fa-rocketchat"></i></span>
                @endif
                <a href="{{asset('filedcte/')}}/{{session()->get('Person')->dni}}-{{$item->idcd}}.{{$item->formato}}" target="_blank" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs"><i class="fa fa-download"></i></a>
            </td>
        </tr>
        @endforeach
    </tbody>    
    @endif
    
</table>
</div>

<script>
	$(document).ready( function () {
        $('#list').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[10, 20, -1], [10, 20, "Todos"]],        
            "language": {
                "info": "La I.E. tiene _TOTAL_ plan curricular subidos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No hay plan curricular de docentes.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar Docente');
    } );
</script>