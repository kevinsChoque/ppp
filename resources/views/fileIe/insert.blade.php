@extends('template/template')
@section('generalBody')
<style>
    .selectedRow{
        background: rgba(1, 1, 1, 0.22);
    }
</style>
<meta name="_token" content="{{csrf_token()}}" />
<div class="row">
    <div class="col-md-4">
        <div class="container-fluid mt-3 p-0">
            <div class="card card-info card-outline">
                <div class="card-header py-2 pl-2">
                    <h3 class="card-title font-weight-bold">Gestion de Documentos</h3>
                    <div class="card-tools">
                      <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body pb-0">
                    <form id="formInsert" action="{{url('fileIe/insert')}}" method="post" enctype="multipart/form-data" class="m-0">
                        <input type="hidden" id="idarchivoie" name="idarchivoie">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group mb-2">
                                    <label class="m-0">Nombre:</label>
                                    <input type="text" name="nombre" id="nombre" class="form-control form-control-sm">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="m-0">Archivo:</label>
                                    <input class='form-control form-control-sm' type="File" name="file" id="file">
                                    <!-- <label id="nombre-error-file" class="text-danger font-italic" for="nombre" style="">Debe de subir un archivo</label> -->
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="m-0">Comentario:</label>
                                    <textarea name="comentario" id="comentario" cols="30" rows="4" class="form-control form-control-sm"></textarea>
                                </div>
                            </div>
                        </div>
                        {{csrf_field()}}
                     </form>
                </div>
                <div class="card-footer p-2">
                    <input type="submit" value="Guardar" form="formInsert" class="btn btn-success btn-sm float-right ml-1 btnSubmit">
                    <input type="button" value="limpiar" class="btn btn-secondary btn-sm float-right ml-1 limpiar">
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="container-fluid mt-3 p-0">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">
                    <h3 class="card-title font-weight-bold">Documentos Institucionales</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body p-0">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="example1" class="table table-sm table-hover m-0">
                                <thead class="text-center">
                                    <tr>
                                        <th>Nombre</th>
                                        <th>Peso</th>
                                        <th>Formato</th>
                                        <th>Registrado</th>
                                        <th>Opciones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($listTarchivoie as $row)
                                    <tr class="text-center">
                                        <td>{{$row->nombre}}</td>
                                        <td>{{strpbrk($row->nombrereal, '-')!=''?$row->peso:'- -'}}</td>
                                        <td>
                                            @if($row->formato!='')
                                                <span class="fa {{$row->formato=='pdf'?'fa-file-pdf':''}}
                                            {{$row->formato=='docx'?'fa-file-word':''}}
                                            {{$row->formato=='xls'?'fa-file-excel':''}}"></span> {{$row->formato}}
                                            @else- -
                                            @endif
                                        </td>
                                        <td>{{$row->createddate}}</td>
                                        <td>
                                            <a href="#" class="edit btn btn-primary btn-xs" data-id="{{$row->idarchivoie}}" data-toggle="tooltip" title="Editar registro"><i class="fa fa-edit"></i></a>
                                            @if($row->comentario!='')
                                                <span class="btn btn-info btn-xs fab fa-rocketchat comentario" data-toggle="tooltip" title="Comentario/observacion: {{$row->comentario}}"></span>
                                            @endif
                                            @if(strpbrk($row->nombrereal, '-')!='')
                                                <a href="{{asset('fileIe/')}}/{{$row->nombrereal}}.{{$row->formato}}" data-toggle="tooltip" title="Descargar" class="btn btn-success btn-xs" target="_blank"><i class="fa fa-download"></i></a>
                                            @endif
                                            <a href="{{url('/fileIe/delete').'/'.$row->idarchivoie}}" class="btn btn-danger btn-xs" data-toggle="tooltip" title="Borrar registro"><i class="fa fa-trash"></i></a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<script>
    $("#formInsert").validate({
        errorClass: "text-danger font-italic font-weight-normal",
        ignore: ".ignore",
        // validClass: "text-success",
        rules: {
            
            file: {
                required: true,
                extension: "docx|xls|xlsx|doc|pdf"
            },
            nombre: "required",
        },
        messages: {
            nombre: "Ingrese el nombre",
            file: {
                required: 'Ingrese un documento',
                extension:"Solo se acepta word, pdf, excel"
            }
        },
    });
</script>
<script>
    $('.limpiar').on('click',function(){
        $('#nombre').val('');
        $('#comentario').val('');
        $('#file').val('');
    });
    $('.edit').on('click',function(){
        $('table>tbody').find('.selectedRow').removeClass('selectedRow');
        $(this).parent().parent().addClass('selectedRow');
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        });
        jQuery.ajax(
        { 
            url: "{{ url('/fileIe/edit') }}",
            data: {idarchivoie:$(this).attr('data-id')},
            method: 'get',
            success: function(result){
                $('#formInsert').attr('action','{{url('fileIe/edit')}}');
                $('.btnSubmit').val('Guardar cambios');
                $('#idarchivoie').val(result.data.idarchivoie);
                $('#nombre').val(result.data.nombre);
                $('#comentario').val(result.data.comentario);
                $('#file').addClass('ignore');
            }
        });
    });
</script>
@endsection