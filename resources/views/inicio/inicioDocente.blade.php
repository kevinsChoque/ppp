@extends('template/template')
@section('generalBody')
<script src="{{asset('js/Chart.min.js')}}"></script>
<meta name="_token" content="{{csrf_token()}}" />

<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Datos de la I.E.</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body py-1">
            <div class="row">
            	@include('shareView/infoIe')
            </div>
        </div>
    </div>
</div>
<div class="container-fluid mt-3">
    <div class="row">
        @foreach($tArchivo as $item)
        <div class="col-lg-3 col-6">
            <div class="small-box bg-info m-0">
                <div class="inner py-0">
                    @if(stristr(session()->get('Person')->tDocente->tIIEE->nivelm,'inicial')== false)
                        <h3 class="m-0 pt-2 text-lg" style="font-size: 1rem !important;">{{$item->tcurso->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">{{$item->tGrado->nombre}}</p>
                        <p class="mb-0" id="cantU{{$item->idcd}}" data-cantU="{{count($item->tUnidad)}}"><i class="fa fa-file"></i> Tiene {{count($item->tUnidad)}} unidad</p>
                        <p class="mb-0"><i class="fa fa-file"></i> Tiene 
                        @php $totalS = 0; @endphp
                        @foreach($item->tUnidad as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tSesion as $item3)
                                @php
                                    $subtotal=$subtotal+1;
                                @endphp
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} sesion</p>
                    @else
                        <h3 class="m-0 pt-2 text-lg">{{$item->tgrado->nombre}} | {{$item->periodoAcademico}}</h3>
                        <p class="mb-0">Seccion {{$item->tSeccion->nombre}}</p>
                        <p class="mb-0" id="cantU{{$item->idcd}}" data-cantU="{{count($item->tUnidad)}}"><i class="fa fa-file"></i> Tiene {{count($item->tUnidad)}} unidad</p>
                        <p class="mb-0"><i class="fa fa-file"></i> Tiene 
                        @php $totalS = 0; @endphp
                        @foreach($item->tUnidad as $item2)
                            @php $subtotal=0; @endphp
                            @foreach($item2->tSesion as $item3)
                                @php
                                    $subtotal=$subtotal+1;
                                @endphp
                            @endforeach
                            @php $totalS=$totalS+$subtotal; @endphp
                        @endforeach
                         {{$totalS}} sesion</p>
                    @endif
                </div>
                <div class="icon"><i class="fa fa-briefcase"></i></div>
                <a href="#" class="small-box-footer p-0 showFormUs" data-id="{{$item->idcd}}" data-toggle="collapse" data-target="#id{{$item->idcd}}" aria-expanded="false" aria-controls="id{{$item->idcd}}"> <i class="fa fa-arrow-circle-right"></i></a>
            </div>
        </div>
        @endforeach
    </div>
</div>
@endsection