@extends('template/template')
@section('generalBody')
<script src="{{asset('js/Chart.min.js')}}"></script>
<meta name="_token" content="{{csrf_token()}}" />
<div class="container-fluid mt-3 p-0">
    <div class="card card-info card-outline">
        <div class="card-header py-2 pl-2">
            <h3 class="card-title font-weight-bold">Datos de la I.E.</h3>
            <div class="card-tools">
              <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
            </div>
        </div>
        <div class="card-body py-1">
            <div class="row">
            	@include('shareView/infoIe')
            </div>
        </div>
    </div>
</div>
<!-- <div class="row mt-3">
    <div class="col-md-12">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Archivos registrados por mes</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="bar3" width="150" height="25" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
</div> -->
<div class="row">
    <div class="col-md-4">
        <div class="container-fluid">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">Cant. de registros.</div>
                <div class="card-body px-1 pt-2 pb-0">
                    <canvas id="cantFile" width="150" height="60" class="canvas-bar"></canvas>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-8">
        <div class="container-fluid p-0">
            <div class="card card-default card-info card-outline">
                <div class="card-header py-2 pl-2">
                    <h3 class="card-title font-weight-bold">Plan Curricular de Docentes</h3>
                    <div class="card-tools">
                        <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button>
                    </div>
                </div>
                <div class="card-body p-0 mt-1">
                    <div class="row">
                        <div class="col-md-12">
                            <table id="listDcte" class="table table-sm table-hover m-0 w-100">
                                <thead class="text-center">
                                    <tr>
                                        <!-- <th>DNI</th> -->
                                        <th>Nombre del docente</th>
                                        <th>Cant. plan anual</th>
                                        <th>Cant. unidades</th>
                                        <th>Cant. sesiones</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($ie->tDocente as $item)
                                    <tr class="text-center">
                                        <!-- <td>{{$item->dni}}</td> -->
                                        <td>{{$item->tPersona_->nombres}} {{$item->tPersona_->apaterno}} {{$item->tPersona_->amaterno}}</td>
                                        <td>{{count($item->tCursoxdocente)}} plan anual</td>
                                        <td>
                                            @php $totalU = 0; @endphp
                                            @foreach($item->tCursoxdocente as $item2)
                                                @php $subtotal=0; @endphp
                                                @foreach($item2->tUnidad as $item3)
                                                    @php
                                                        $subtotal=$subtotal+1;
                                                    @endphp
                                                @endforeach
                                                @php $totalU=$totalU+$subtotal; @endphp
                                            @endforeach
                                            {{$totalU}} unidad
                                        </td>
                                        <td>
                                            @php $totalS = 0; @endphp
                                            @foreach($item->tCursoxdocente as $item2)
                                                @foreach($item2->tUnidad as $item3)
                                                    @php $subtotal=0; @endphp
        
                                                    @foreach($item3->tSesion as $item4)
                                                        @php
                                                            $subtotal=$subtotal+1;
                                                        @endphp
                                                    @endforeach
                                                    @php $totalS=$totalS+$subtotal; @endphp
        
                                                @endforeach
                                            @endforeach
                                            {{$totalS}} sesiones
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    var cantFile={!! json_encode($cantFile) !!};
    var planSegun = document.getElementById('cantFile').getContext('2d');
    var planSegunD = {
        label: ["Cantidad de archivos"],
        data: [cantFile[0], cantFile[1], cantFile[2],0],
        backgroundColor: [
            'rgba(40, 167, 69,.5)',
            'rgba(255, 193, 7,.5)',
            'rgba(42, 170, 190,.5)',
            'rgba(255, 206, 86, 0.2)'
        ],
    };
     
    var objbar = new Chart(planSegun, {
        type: 'bar',
        data: {
            labels: ["Plan anual", "Unidades", "Sesiones"],
            datasets: [planSegunD]
        },
        options: {
            responsive: true,
            tooltips: {
                mode: 'index'
            },
            legend: {
                display: false
            },
        }
    });
    var bar = document.getElementById('bar3').getContext('2d');
    var objbar = new Chart(bar, {
        type: 'line',
        data: {
            labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio','Agosto','Setiembre','Octubre','Noviembre','Diciembre'],
            datasets: [{
                    label: 'Plan anual fecha de registro',
                    backgroundColor: '#93d3a2',
                    borderColor: '#93d3a2',
                    data: [10, 30, 39, 20, 25, 34, 1,30, 39, 20, 25, 34],
                    fill: false,
                },{
                    label: 'Unidades fecha de registro',
                    data: [10, 30, 39, 20, 25, 30, 1,30, 30, 20, 30, 34],
                    backgroundColor: '#ffe083',
                    borderColor: '#ffe083',
                    fill: false,
                },{
                    label: 'Sesiones fecha de registro',
                    data: [10, 10, 39, 20, 25, 10, 1,20, 20, 20, 30, 3],
                    backgroundColor: '#94d4de',
                    borderColor: '#94d4de',
                    fill: false,
                } ]
        },
        options: {
                responsive: true,
                title: {
                    display: false,
                    text: 'Grid Line Settings'
                },
                scales: {
                    yAxes: [{
                        gridLines: {
                            drawBorder: false,
                            color: ['green', 'green', 'green', 'green', 'green', 'green', 'green', 'green']
                        },
                        ticks: {
                            min: 0,
                            max: 50,
                            stepSize: 10
                        }
                    }]
                }
            }
    });
    
</script>
<script>
    $(document).ready( function () {
        $('#listDcte').DataTable( {
            "paging": true,
            "scrollX": true,
            "lengthMenu": [[4, 8, -1], [4, 8, "Todos"]],        
            "language": {
                "info": "Tiene _TOTAL_ archivos.",
                "search":"",
                "infoFiltered": "(filtrado de _MAX_ Docentes)",
                "infoEmpty": "No hay registros disponibles",
                "sEmptyTable": "No tiene archivos subidos.",
                "lengthMenu":"_MENU_",
                "paginate": {
                    "first": "Primero",
                    "last": "Ultimo",
                    "next": "Siguiente",
                    "previous": "Anterior"
                }
            }
        } );
        $('input[type=search]').parent().addClass('mr-2');
        $('input[type=search]').prop('placeholder','Buscar archivos');
    } );
</script>
@endsection